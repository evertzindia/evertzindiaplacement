-- --------------------------------------------------------
-- Host:                         127.0.0.1
-- Server version:               10.3.10-MariaDB - mariadb.org binary distribution
-- Server OS:                    Win64
-- HeidiSQL Version:             9.4.0.5125
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;


-- Dumping database structure for evertz_interview_app
DROP DATABASE IF EXISTS `evertz_interview_app`;
CREATE DATABASE IF NOT EXISTS `evertz_interview_app` /*!40100 DEFAULT CHARACTER SET latin1 */;
USE `evertz_interview_app`;

-- Dumping structure for table evertz_interview_app.admin_user
DROP TABLE IF EXISTS `admin_user`;
CREATE TABLE IF NOT EXISTS `admin_user` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `USERNAME` varchar(50) NOT NULL,
  `PASSWORD` varchar(500) NOT NULL,
  PRIMARY KEY (`ID`),
  UNIQUE KEY `USERNAME_UNIQUE` (`USERNAME`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=latin1;

-- Dumping data for table evertz_interview_app.admin_user: ~3 rows (approximately)
DELETE FROM `admin_user`;
/*!40000 ALTER TABLE `admin_user` DISABLE KEYS */;
INSERT INTO `admin_user` (`ID`, `USERNAME`, `PASSWORD`) VALUES
	(1, 'evertz', 'ZXZlcnR6MQ=='),
	(6, 'joshua', 'ZXZlcnR6MQ=='),
	(11, 'test', 'ZXZlcnR6MQ==');
/*!40000 ALTER TABLE `admin_user` ENABLE KEYS */;

-- Dumping structure for table evertz_interview_app.branch
DROP TABLE IF EXISTS `branch`;
CREATE TABLE IF NOT EXISTS `branch` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `NAME` varchar(50) NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8;

-- Dumping data for table evertz_interview_app.branch: ~8 rows (approximately)
DELETE FROM `branch`;
/*!40000 ALTER TABLE `branch` DISABLE KEYS */;
INSERT INTO `branch` (`ID`, `NAME`) VALUES
	(1, 'ISE'),
	(2, 'CSE'),
	(3, 'ECE'),
	(4, 'IT'),
	(5, 'CIVIL'),
	(6, 'MECH'),
	(7, 'MCA'),
	(8, 'BCA');
/*!40000 ALTER TABLE `branch` ENABLE KEYS */;

-- Dumping structure for table evertz_interview_app.candidate_coding_response
DROP TABLE IF EXISTS `candidate_coding_response`;
CREATE TABLE IF NOT EXISTS `candidate_coding_response` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `CANDIDATE_DETAILS_ID` int(11) NOT NULL,
  `CODING_QUESTION_ID` int(11) NOT NULL,
  `CANDIDATE_PROGRAM` longtext DEFAULT NULL,
  `COMPILATION_STATUS` bit(1) NOT NULL DEFAULT b'0',
  `RUN_STATUS` tinyint(4) NOT NULL DEFAULT 0,
  `ATTEMPTED_STATUS` bit(1) NOT NULL DEFAULT b'0',
  `CODING_MARKS` int(11) NOT NULL DEFAULT 0,
  PRIMARY KEY (`ID`),
  KEY `CANDIDATE_DETAILS_ID` (`CANDIDATE_DETAILS_ID`),
  KEY `CODING_QUESTION_ID` (`CODING_QUESTION_ID`),
  CONSTRAINT `CODING_QUESTION_ID` FOREIGN KEY (`CODING_QUESTION_ID`) REFERENCES `coding_questions` (`ID`),
  CONSTRAINT `candidate_coding_response_ibfk_1` FOREIGN KEY (`CANDIDATE_DETAILS_ID`) REFERENCES `candidate_details` (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=332 DEFAULT CHARSET=latin1;

-- Dumping data for table evertz_interview_app.candidate_coding_response: ~0 rows (approximately)
DELETE FROM `candidate_coding_response`;
/*!40000 ALTER TABLE `candidate_coding_response` DISABLE KEYS */;
/*!40000 ALTER TABLE `candidate_coding_response` ENABLE KEYS */;

-- Dumping structure for table evertz_interview_app.candidate_details
DROP TABLE IF EXISTS `candidate_details`;
CREATE TABLE IF NOT EXISTS `candidate_details` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `REG_NO` varchar(20) NOT NULL,
  `NAME` varchar(200) NOT NULL,
  `DOB` date NOT NULL,
  `GENDER` bit(1) NOT NULL,
  `EMAIL` varchar(100) NOT NULL,
  `MOB_NO` varchar(10) NOT NULL,
  `COLLEGE_DETAILS_ID` int(11) NOT NULL,
  `DEGREE_ID` int(11) NOT NULL,
  `BRANCH_ID` int(11) NOT NULL,
  `GRAD_YEAR` year(4) NOT NULL,
  `CGPA` decimal(4,2) NOT NULL,
  `ATTEMPTED_DATETIME` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `ATTEMPTED_STATUS` int(3) NOT NULL DEFAULT 0,
  `MODULE_ID` int(11) NOT NULL,
  PRIMARY KEY (`ID`),
  UNIQUE KEY `REG_NO` (`REG_NO`),
  KEY `COLLEGE_DETAILS_ID_idx` (`COLLEGE_DETAILS_ID`),
  KEY `DEGREE_ID_idx` (`DEGREE_ID`),
  KEY `BRANCH_ID_idx` (`BRANCH_ID`),
  KEY `MODULE_ID` (`MODULE_ID`),
  CONSTRAINT `BRANCH_ID` FOREIGN KEY (`BRANCH_ID`) REFERENCES `branch` (`ID`),
  CONSTRAINT `COLLEGE_DETAILS_ID` FOREIGN KEY (`COLLEGE_DETAILS_ID`) REFERENCES `college_details` (`id`),
  CONSTRAINT `DEGREE_ID` FOREIGN KEY (`DEGREE_ID`) REFERENCES `degree` (`id`),
  CONSTRAINT `candidate_details_ibfk_1` FOREIGN KEY (`MODULE_ID`) REFERENCES `modules` (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=367 DEFAULT CHARSET=utf8;

-- Dumping data for table evertz_interview_app.candidate_details: ~0 rows (approximately)
DELETE FROM `candidate_details`;
/*!40000 ALTER TABLE `candidate_details` DISABLE KEYS */;
/*!40000 ALTER TABLE `candidate_details` ENABLE KEYS */;

-- Dumping structure for table evertz_interview_app.candidate_response
DROP TABLE IF EXISTS `candidate_response`;
CREATE TABLE IF NOT EXISTS `candidate_response` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `CANDIDATE_DETAILS_ID` int(11) NOT NULL,
  `QUESTION_ID` int(11) NOT NULL,
  `CANDIDATE_ANSWER` int(1) DEFAULT 0,
  `ATTEMPTED_STATUS` bit(1) NOT NULL,
  PRIMARY KEY (`ID`),
  KEY `QUESTIONS_ID_idx` (`QUESTION_ID`),
  KEY `CANDIDATE_DETAILS_ID_idx` (`CANDIDATE_DETAILS_ID`),
  CONSTRAINT `CANDIDATE_DETAILS_ID` FOREIGN KEY (`CANDIDATE_DETAILS_ID`) REFERENCES `candidate_details` (`ID`),
  CONSTRAINT `QUESTIONS_ID` FOREIGN KEY (`QUESTION_ID`) REFERENCES `questions` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=17469 DEFAULT CHARSET=utf8;

-- Dumping data for table evertz_interview_app.candidate_response: ~0 rows (approximately)
DELETE FROM `candidate_response`;
/*!40000 ALTER TABLE `candidate_response` DISABLE KEYS */;
/*!40000 ALTER TABLE `candidate_response` ENABLE KEYS */;

-- Dumping structure for table evertz_interview_app.coding_questions
DROP TABLE IF EXISTS `coding_questions`;
CREATE TABLE IF NOT EXISTS `coding_questions` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `QUESTION` text NOT NULL,
  `MODULE_ID` int(1) NOT NULL,
  `BASE_CLASS_NAME` varchar(50) NOT NULL,
  `BASE_CODE` text NOT NULL,
  PRIMARY KEY (`ID`),
  KEY `MODULE_ID1` (`MODULE_ID`),
  CONSTRAINT `MODULE_ID1` FOREIGN KEY (`MODULE_ID`) REFERENCES `modules` (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=120 DEFAULT CHARSET=utf8;

-- Dumping data for table evertz_interview_app.coding_questions: ~14 rows (approximately)
DELETE FROM `coding_questions`;
/*!40000 ALTER TABLE `coding_questions` DISABLE KEYS */;
INSERT INTO `coding_questions` (`ID`, `QUESTION`, `MODULE_ID`, `BASE_CLASS_NAME`, `BASE_CODE`) VALUES
	(60, '<b>Count Trees</b><br><br>Consider there are n number of trees with tree numbers (t) 0, 1, 2, 3, 4, 5 .... in a line with different heights in meteres (h) as 15, 1, 7, 4, 3, 6 ....<br><br>The farmer (f) wants to find the count of trees that are shorter and taller to his left and right from the point where he is standing (f)<br><br>Your job is to write a code to help the farmer find the number of trees that are shorter and taller to his left and right.<br><br>In the below image as you can see, the farmer is standing at a point f=3 and there are a total of t=10 trees. There are  3 trees to his left and 6 trees to his right.<br><br>Out of the 3 trees to his left, there is 1 tree that is shorter (height=1) and 2 trees that is taller (height=(15 & 7)). Similarly on his right, there are 3 trees <br>that are shorter (height=(3, 1 & 3)) and 3 trees that are taller (height=(6, 12 & 10)).<br><br><img class=coding-images" src="assets/question_1.jpg" height="550" width="1400"><br><br>Your program should print the output like below<br><br>1 2,3 3<br><br>Inputs:<br><br>The height of trees separated by commas t=(15,1,7,4,3,6,12,1,10,3) and the position of the farmer f=3<br>Both inputs are Integers<br><br>t is greater 0 and less than 1000 - 	Array of numbers between 0 and 999 (Maximum size of array is 1000)			<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Each value in the array is also between 0 and 999<br><br>f is greater than 0 and less than 1000 - 	Integer value between 0 and 999<br><br>Outputs:<br><br>Integer values between 0 and 999. Printed using a space and comma<br><br>Sample Testcase 1:<br><br>Input:<br>15,1,7,4,3,6,12,1,10,3 3<br><br>Output:<br>1 2,3 3<br><br>Sample Testcase 2:<br><br>Input:<br>1,5,99,52,8,25,68,74,14,5,23,7,92,32 7<br><br>Output:<br>6 1,5 1<br><br>', 11, 'CountTrees', 'public class CountTrees {<br><br>&nbsp;&nbsp;&nbsp;&nbsp;public static void main(String[] argsv) {<br><br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;//Getting the data from command line arguments<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;String treeHeightsDataString = argsv[0].split(" ")[0];<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;String farmerPositionString = argsv[0].split(" ")[1];<br><br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;String[] treeHeightsStringArray = treeHeightsDataString.split(",");<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;int numberOfTrees = treeHeightsStringArray.length;<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;int h[] = new int[numberOfTrees];<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;int f = Integer.parseInt(farmerPositionString);<br><br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;//Converting the String data to Integer<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;for (int i = 0; i < numberOfTrees; i++) {<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;h[i] = Integer.parseInt(treeHeightsStringArray[i]);<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;}<br><br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;String calculatedCountOfTrees = calculateCountOfTrees(h, f);<br><br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;System.out.println(calculatedCountOfTrees);<br><br>&nbsp;&nbsp;&nbsp;&nbsp;}<br><br>&nbsp;&nbsp;&nbsp;&nbsp;//This function to be populated by the candidate<br>&nbsp;&nbsp;&nbsp;&nbsp;public static String calculateCountOfTrees(int[] h, int f) {<br><br><br><br>&nbsp;&nbsp;&nbsp;&nbsp;}<br><br>}'),
	(64, '<b>Count Trees</b><br><br>Consider there are n number of trees with tree numbers (t) 0, 1, 2, 3, 4, 5 .... in a line with different heights in meteres (h) as 15, 1, 7, 4, 3, 6 ....<br><br>The farmer (f) wants to find the count of trees that are shorter and taller to his left and right from the point where he is standing (f)<br><br>Your job is to write a code to help the farmer find the number of trees that are shorter and taller to his left and right.<br><br>In the below image as you can see, the farmer is standing at a point f=3 and there are a total of t=10 trees. There are  3 trees to his left and 6 trees to his right.<br><br>Out of the 3 trees to his left, there is 1 tree that is shorter (height=1) and 2 trees that is taller (height=(15 & 7)). Similarly on his right, there are 3 trees <br>that are shorter (height=(3, 1 & 3)) and 3 trees that are taller (height=(6, 12 & 10)).<br><br><img class=coding-images" src="assets/question_1.jpg" height="550" width="1400"><br><br>Your program should print the output like below<br><br>1 2,3 3<br><br>Inputs:<br><br>The height of trees separated by commas t=(15,1,7,4,3,6,12,1,10,3) and the position of the farmer f=3<br>Both inputs are Integers<br><br>t is greater 0 and less than 1000 - 	Array of numbers between 0 and 999 (Maximum size of array is 1000)			<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Each value in the array is also between 0 and 999<br><br>f is greater than 0 and less than 1000 - 	Integer value between 0 and 999<br><br>Outputs:<br><br>Integer values between 0 and 999. Printed using a space and comma<br><br>Sample Testcase 1:<br><br>Input:<br>15,1,7,4,3,6,12,1,10,3 3<br><br>Output:<br>1 2,3 3<br><br>Sample Testcase 2:<br><br>Input:<br>1,5,99,52,8,25,68,74,14,5,23,7,92,32 7<br><br>Output:<br>6 1,5 1<br><br>', 22, 'CountTrees', 'import sys;<br><br># This function needs to be populated by the candidate<br>def calculateCountOfTrees(h, f):<br><br>&nbsp;&nbsp;&nbsp;&nbsp;return "Null";<br><br># Get data from command line argument<br>treeHeightsDataString = sys.argv[1].split(" ")[0];<br>farmerPositionString = sys.argv[1].split(" ")[1];<br>treeHeightsStringArray = treeHeightsDataString.split(",");<br>numberOfTrees = len(treeHeightsStringArray);<br><br># Initialise empty array<br>h = [];<br>f = int(farmerPositionString);<br><br># Populate array from command line argument string<br>for i in range(numberOfTrees):<br>&nbsp;&nbsp;&nbsp;&nbsp;h.append(int(treeHeightsStringArray[i]));<br><br># Call the function to do the work<br>calculatedCountOfTrees = calculateCountOfTrees(h, f);<br><br>print(calculatedCountOfTrees);'),
	(65, '<b>Bangalore Traffic :</b><br><br>Due to increase of traffic in Bangalore, Government is planning to design an application which will determines the duration taken by a bus to reach the destination.<br>The government is using Google map API to get the traffic increased percentage in a particular route.<br><br>You need to write a program that calculates the duration taken by the bus to reach its destination based on the average time and increased traffic percentages.<br><br>Sample Testcase 1 - Silk Board to Hebbal<br><br>Bus Route - 500D that starts from Silk Board and reaches Hebbal.<br><br><img class="coding-images" src="assets/question_3_tc1.jpg" height="550" width="1650"><br><br>Inputs:<br><br>a = (30,20,15)<br>t = (15,-5,10)<br><br>30,20,15 -> Array of time taken by bus between two stops.<br>+15,-5,+10 -> Array of traffic increased percentages between each stops.<br><br>a is greater than 1and less than 1000 - 	Array of numbers between 0 and 999 (Maximum size of array is 1000)<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Each value in the array is also between 0 and 999<br><br>t is greater than -1000 and less than 1000 - 	Integer value between 0 and 999<br><br>Length of t always equal to length of a<br><br>Output:<br><br>70.0<br><br>70.0 -> Time taken by the bus to reach the destination<br><br>Sample Testcase 2 - KBS to Electronics City<br><br>Bus Route - 335 that starts from Majestic and reaches Whitefield.<br><br><img class="coding-images" src="assets/question_3_tc2.jpg" height="550" width="1650"><br><br>Inputs:<br><br>a = (10,20,15,15)<br>t = (10,-10,25,30)<br><br>10,20,15,15 -> Array of time taken by bus between two stops.<br>10,-10,25,30 -> Array of traffic increased percentages between each stops.<br><br>a is greater than 1and less than 1000 - 	Array of numbers between 0 and 999 (Maximum size of array is 1000)<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Each value in the array is also between 0 and 999<br><br>t is greater than -1000 and less than 1000 - 	Integer value between 0 and 999<br><br>Length of t always equal to length of a<br><br>Output:<br>67.25<br><br>67.25 -> Time taken by the bus to reach the destination<br><br>', 11, 'BangaloreTraffic', 'public class BangaloreTraffic {<br><br>&nbsp;&nbsp;&nbsp;&nbsp;public static void main(String[] argsv) {<br><br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;// Getting the data from command line arguments<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;String[] averageTimeStringArray = argsv[0].split(" ")[0].split(",");<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;String[] averageTrafficStringArray = argsv[0].split(" ")[1].split(",");<br><br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;int averageTimeStringArrayLength = averageTimeStringArray.length;<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;int averageTrafficStringArrayLength = averageTrafficStringArray.length;<br><br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;if (averageTimeStringArrayLength != averageTrafficStringArrayLength) {<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;System.out.println("Time and Traffic Array Length doesnt match");<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;System.exit(1);<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;}<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;int[] a = new int[averageTimeStringArrayLength];<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;int[] t = new int[averageTrafficStringArrayLength];<br><br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;// Converting the String data to Integer<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;for (int i = 0; i < averageTimeStringArrayLength; i++) {<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;a[i] = Integer.parseInt(averageTimeStringArray[i]);<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;t[i] = Integer.parseInt(averageTrafficStringArray[i]);<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;}<br><br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;float calculatedReachingDuration = calculateReachingDuration(a, t, averageTimeStringArrayLength);<br><br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;System.out.println(calculatedReachingDuration);<br><br>&nbsp;&nbsp;&nbsp;&nbsp;}<br><br>&nbsp;&nbsp;&nbsp;&nbsp;// This function to be populated by the candidate<br>&nbsp;&nbsp;&nbsp;&nbsp;public static float calculateReachingDuration(int[] a, int[] t, int numberOfStops) {<br><br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;return 0;<br><br>&nbsp;&nbsp;&nbsp;&nbsp;}<br><br>}'),
	(69, '<b>Bangalore Traffic :</b><br><br>Due to increase of traffic in Bangalore, Government is planning to design an application which will determines the duration taken by a bus to reach the destination.<br>The government is using Google map API to get the traffic increased percentage in a particular route.<br><br>You need to write a program that calculates the duration taken by the bus to reach its destination based on the average time and increased traffic percentages.<br><br>Sample Testcase 1 - Silk Board to Hebbal<br><br>Bus Route - 500D that starts from Silk Board and reaches Hebbal.<br><br><img class="coding-images" src="assets/question_3_tc1.jpg" height="550" width="1650"><br><br>Inputs:<br><br>a = (30,20,15)<br>t = (15,-5,10)<br><br>30,20,15 -> Array of time taken by bus between two stops.<br>+15,-5,+10 -> Array of traffic increased percentages between each stops.<br><br>a is greater than 1and less than 1000 - 	Array of numbers between 0 and 999 (Maximum size of array is 1000)<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Each value in the array is also between 0 and 999<br><br>t is greater than -1000 and less than 1000 - 	Integer value between 0 and 999<br><br>Length of t always equal to length of a<br><br>Output:<br><br>70.0<br><br>70.0 -> Time taken by the bus to reach the destination<br><br>Sample Testcase 2 - KBS to Electronics City<br><br>Bus Route - 335 that starts from Majestic and reaches Whitefield.<br><br><img class="coding-images" src="assets/question_3_tc2.jpg" height="550" width="1650"><br><br>Inputs:<br><br>a = (10,20,15,15)<br>t = (10,-10,25,30)<br><br>10,20,15,15 -> Array of time taken by bus between two stops.<br>10,-10,25,30 -> Array of traffic increased percentages between each stops.<br><br>a is greater than 1and less than 1000 - 	Array of numbers between 0 and 999 (Maximum size of array is 1000)<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Each value in the array is also between 0 and 999<br><br>t is greater than -1000 and less than 1000 - 	Integer value between 0 and 999<br><br>Length of t always equal to length of a<br><br>Output:<br>67.25<br><br>67.25 -> Time taken by the bus to reach the destination<br><br>', 22, 'BangaloreTraffic', 'import sys;<br>import numpy as np;<br><br># To be populated by the candidate<br>def calculateReachingDuration(a, t, numberOfStops) :<br><br>&nbsp;&nbsp;&nbsp;&nbsp;return 0;<br><br># Reading command line arguments and handling the array split<br>averageTimeStringArray  = sys.argv[1].split(" ")[0].split(",");<br>averageTrafficStringArray  = sys.argv[1].split(" ")[1].split(",");<br>averageTimeStringArrayLength = len(averageTimeStringArray);<br>averageTrafficStringArrayLength  = len(averageTrafficStringArray);<br>if (averageTimeStringArrayLength != averageTrafficStringArrayLength):<br>&nbsp;&nbsp;&nbsp;&nbsp;print("Time and Traffic Array Length doesnt match");<br>&nbsp;&nbsp;&nbsp;&nbsp;sys.exit(1);<br><br>a =[]<br>t =[]<br><br>list(np.float_(a));<br>list(np.float_(t));<br><br>for i in range(averageTimeStringArrayLength) :<br>&nbsp;&nbsp;&nbsp;&nbsp;a.append(averageTimeStringArray[i]);<br>&nbsp;&nbsp;&nbsp;&nbsp;t.append(averageTrafficStringArray[i]);<br><br><br>calculatedReachingDuration = float(calculateReachingDuration(a, t, averageTimeStringArrayLength));<br>print(calculatedReachingDuration);'),
	(101, '<b>Find Movie</b><br><br>Adam has 1000+ movies in his 5TB Harddisk under Tamil, Telugu, English and Hindi folders. He has exported the list of movies from his 5TB Harddisk into a text file (Adam_Movie_Database.txt).<br><br>The text file now has 1000+ movie names separated by new line with the folder names as shown in the image below.<br><br><img class="coding-images" src="assets/question_2.jpg" height="550" width="550"><br><br>Now Adam wants to build an application that allows him to take a movie name as input and shows "Exists" if the movie exists in the text file or "Does not exist" if the movie does not exist in the text file.<br><br>You need to write a piece of code that takes the movie name as input and prints "Exists" or "Does not exist" based on its availability.<br><br>He also wants to find the count if the movie exists in multiple folders.<br><br>Inputs:<br><br>Movie name as String<br><br>Length of String is greater than 0 and less than 100<br><br>Outputs:<br><br>"Exists" or "Does not exist"<br>2 -> Number of times the movie exists<br><br>Sample Testcase 1:<br><br>Input:<br>Avengers<br><br>Output:<br>Exists,2<br><br>Sample Testcase 2:<br><br>Input:<br>Kuch Kuch Hota Hai<br><br>Output:<br>Does not exists,0<br><br>', 11, 'FindMovie', 'import java.io.BufferedReader;<br>import java.io.File;<br>import java.io.FileNotFoundException;<br>import java.io.FileReader;<br>import java.util.ArrayList;<br>import java.util.stream.Collectors;<br><br>public class FindMovie {<br><br>&nbsp;&nbsp;&nbsp;&nbsp;public static void main(String[] argsv) {<br><br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;// Getting the data from command line arguments<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;String movieToFind = argsv[0];<br><br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;String adamMovieList = null;<br><br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;// Reading the file and storing the contents in a String variable<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;// The UtilityFunction class has all the required functions<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;// If you see the text as "Please contact Supervisor" in the Run output. Please<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;// do contact Supervisor<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;try {<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;adamMovieList = readAdamFileContentsAndReturnString();<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;} catch (Exception e) {<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;System.out.println("Error: " + e.getMessage() + " - Please contact Supervisor");<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;System.exit(1);<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;}<br><br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;String resultFromFunction = calculateCountOfTrees(adamMovieList, movieToFind);<br><br>&nbsp;&nbsp;&nbsp;&nbsp;System.out.println(resultFromFunction);<br><br>&nbsp;&nbsp;&nbsp;&nbsp;}<br><br>&nbsp;&nbsp;&nbsp;&nbsp;// This function to be populated by the candidate<br>&nbsp;&nbsp;&nbsp;&nbsp;public static String calculateCountOfTrees(String adamMovieList, String movieToFind) {<br><br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;return null;<br>&nbsp;&nbsp;&nbsp;&nbsp;}<br>&nbsp;&nbsp;&nbsp;&nbsp;@SuppressWarnings("resource")<br>&nbsp;&nbsp;&nbsp;&nbsp;public static String readAdamFileContentsAndReturnString() throws Exception {<br><br>&nbsp;&nbsp;&nbsp;&nbsp;String adamMovieList = null;<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;String adamMovieTextFileLocation = "C:\\\\\\\\EvertzInterviewApplication\\\\\\\\CandidatePrograms\\\\\\\\Adam_Movie_Database.txt";<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;File file = new File(adamMovieTextFileLocation);<br><br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;BufferedReader br;<br><br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;// Protect file read using try/catch<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;try {<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;br = new BufferedReader(new FileReader(file));<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;adamMovieList = br.lines().collect(Collectors.joining());<br><br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;} catch (FileNotFoundException e) {<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;e.printStackTrace();<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;throw new Exception(e.getMessage());<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;}<br><br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;return adamMovieList;<br>&nbsp;&nbsp;&nbsp;&nbsp;}<br><br>}'),
	(105, '<b>Find Movie</b><br><br>Adam has 1000+ movies in his 5TB Harddisk under Tamil, Telugu, English and Hindi folders. He has exported the list of movies from his 5TB Harddisk into a text file (Adam_Movie_Database.txt).<br><br>The text file now has 1000+ movie names separated by new line with the folder names as shown in the image below.<br><br><img class="coding-images" src="assets/question_2.jpg" height="550" width="550"><br><br>Now Adam wants to build an application that allows him to take a movie name as input and shows "Exists" if the movie exists in the text file or "Does not exist" if the movie does not exist in the text file.<br><br>You need to write a piece of code that takes the movie name as input and prints "Exists" or "Does not exist" based on its availability.<br><br>He also wants to find the count if the movie exists in multiple folders.<br><br>Inputs:<br><br>Movie name as String<br><br>Length of String is greater than 0 and less than 100<br><br>Outputs:<br><br>"Exists" or "Does not exist"<br>2 -> Number of times the movie exists<br><br>Sample Testcase 1:<br><br>Input:<br>Avengers<br><br>Output:<br>Exists,2<br><br>Sample Testcase 2:<br><br>Input:<br>Kuch Kuch Hota Hai<br><br>Output:<br>Does not exists,0<br><br>', 22, 'FindMovie', 'import sys;<br>import numpy as np;<br><br># The candidate has to populate this function<br>def calculateCountOfTrees(adamMovieList, movieToFind) :<br><br>&nbsp;&nbsp;&nbsp;&nbsp;return 0;<br><br># This function to read the contents and return as a string<br>def readAdamFileContentsAndReturnString():<br><br>&nbsp;&nbsp;&nbsp;&nbsp;adamMovieList = "";<br>&nbsp;&nbsp;&nbsp;&nbsp;adamMovieTextFileLocation = "C:\\\\EvertzInterviewApplication\\\\CandidatePrograms\\\\Adam_Movie_Database.txt";<br><br>&nbsp;&nbsp;&nbsp;&nbsp;file = open(adamMovieTextFileLocation, "r");<br><br>&nbsp;&nbsp;&nbsp;&nbsp;try :<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;data = file.read();<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;for each in data :<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;adamMovieList = adamMovieList + ",".join(each);<br>&nbsp;&nbsp;&nbsp;&nbsp;except Exception as e :<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;e.printStackTrace();<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;raise Exception(e);<br><br>&nbsp;&nbsp;&nbsp;&nbsp;return adamMovieList;<br><br># Getting command line arguments<br>movieToFind = sys.argv[1];<br><br># Return from function<br>resultFromFunction = calculateCountOfTrees(readAdamFileContentsAndReturnString(), movieToFind);<br><br>print(resultFromFunction);'),
	(106, '<b>New Traffic Rules Penalties</b><br><br>According to new traffic rules, the charges for different violations is changed.<br> New charges are listed below. <br><br>There will be two cases when vehicle will be seized.<br>1. If the total amount of fine is more than 2000(For Bike)/4000(For Car).<br>2. If there are more than three violations (Either for Bike or Car).<br><br>You need to write a code to calculate total amount fined for a person and determine if licence will be seized or not.<br><br>No Helmet - 1000<br>No Driving Licence - 500<br>No Registration Certificate - 300<br>Triple Riding - 1000<br>No Seat Belt - 1500<br>Drink and Drive - 3000<br>Minor Driving - 1000<br>Using cellphone while driving - 500<br>Signal jumping - 300<br>Wrong way driving - 1000<br><br>The above data is already stored in two arrays in the code snippet.<br><br>Sample Testcase 1:<br><br>Inputs:<br><br>Bike:No Helmet,No Driving Licence,No Registration Certificate<br><br>Outputs:<br><br>1800,Vehicle will not be seized<br><br><br>Sample Testcase 2:<br><br>Inputs:<br><br>Car:No Seat Belt,Drink and Drive<br><br>Outputs:<br><br>4500,Vehicle will be seized<br><br>', 11, 'TrafficPenalties', 'import java.util.Arrays;<br><br>public class TrafficPenalties {<br>&nbsp;&nbsp;&nbsp;&nbsp;// Array that stores the Traffic Violations and their fines<br>&nbsp;&nbsp;&nbsp;&nbsp;// Use these two arrays to calculate the total fines<br>&nbsp;&nbsp;&nbsp;&nbsp;// Do not change any values in the below 5 variables<br>&nbsp;&nbsp;&nbsp;&nbsp;static String[] violationConditions = {"No Helmet", "No Driving Licence", "No Registration Certificate", "Triple Riding", "No Seat Belt", "Drink and Drive", "Minor Driving", "Using cellphone while driving", "Signal jumping", "Wrong way driving"};<br>&nbsp;&nbsp;&nbsp;&nbsp;static int[] violationPenalties = {1000, 500, 300, 1000, 1500, 3000, 1000, 500, 300, 1000};<br>&nbsp;&nbsp;&nbsp;&nbsp;static int numberOfViolationConditionsAndFines = 10;<br>&nbsp;&nbsp;&nbsp;&nbsp;static int bikeLicenseSiezeThreshold = 2000;<br>&nbsp;&nbsp;&nbsp;&nbsp;static int carLicenseSiezeThreshold = 4000;<br><br>&nbsp;&nbsp;&nbsp;&nbsp;public static void main(String[] args) {<br><br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;String[] argumentListArray = args[0].split(":");<br><br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;// Getting the data from command line arguments<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;String typeOfVehicle = argumentListArray[0];<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;String[] trafficViolationsArrayByVehicle = argumentListArray[1].split(",");<br><br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;String calculatedPenaltyAndDecision = calculateTotalPenalty(typeOfVehicle, trafficViolationsArrayByVehicle);<br><br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;System.out.println(calculatedPenaltyAndDecision);<br><br>&nbsp;&nbsp;&nbsp;&nbsp;}<br><br>&nbsp;&nbsp;&nbsp;&nbsp;// This function to be populated by the candidate<br>&nbsp;&nbsp;&nbsp;&nbsp;public static String calculateTotalPenalty(String typeOfVehicle, String[] trafficViolationsArrayByVehicle) {<br><br><br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;return null;<br><br>&nbsp;&nbsp;&nbsp;&nbsp;}<br>}'),
	(110, '<b>New Traffic Rules Penalties</b><br><br>According to new traffic rules, the charges for different violations is changed.<br> New charges are listed below. <br><br>There will be two cases when vehicle will be seized.<br>1. If the total amount of fine is more than 2000(For Bike)/4000(For Car).<br>2. If there are more than three violations (Either for Bike or Car).<br><br>You need to write a code to calculate total amount fined for a person and determine if licence will be seized or not.<br><br>No Helmet - 1000<br>No Driving Licence - 500<br>No Registration Certificate - 300<br>Triple Riding - 1000<br>No Seat Belt - 1500<br>Drink and Drive - 3000<br>Minor Driving - 1000<br>Using cellphone while driving - 500<br>Signal jumping - 300<br>Wrong way driving - 1000<br><br>The above data is already stored in two arrays in the code snippet.<br><br>Sample Testcase 1:<br><br>Inputs:<br><br>Bike:No Helmet,No Driving Licence,No Registration Certificate<br><br>Outputs:<br><br>1800,Vehicle will not be seized<br><br><br>Sample Testcase 2:<br><br>Inputs:<br><br>Car:No Seat Belt,Drink and Drive<br><br>Outputs:<br><br>4500,Vehicle will be seized<br><br>', 22, 'TrafficPenalties', 'import sys;<br><br>violationConditions = ["No Helmet", "No Driving Licence", "No Registration Certificate", "Triple Riding", "No Seat Belt", "Drink and Drive", "Minor Driving", "Using cellphone while driving", "Signal jumping", "Wrong way driving"]<br>violationPenalties = [1000, 500, 300, 1000, 1500, 3000, 1000, 500, 300, 1000]<br>numberOfViolationConditionsAndFines = 10<br>bikeLicenseSiezeThreshold = 2000<br>carLicenseSiezeThreshold = 4000<br><br># This function to be populated by the candidate<br>def calculateTotalPenalty(typeOfVehicle, trafficViolationsArrayByVehicle):<br><br>&nbsp;&nbsp;&nbsp;&nbsp;return "None";<br><br># Getting the data from command line arguments<br>argumentListArray = sys.argv[1].split(":");<br>typeOfVehicle = argumentListArray[0];<br>trafficViolationsArrayByVehicle = argumentListArray[1].split(",");<br>calculatedPenaltyAndDecision = calculateTotalPenalty(typeOfVehicle, trafficViolationsArrayByVehicle);<br><br>print(calculatedPenaltyAndDecision);'),
	(111, '<b>1D - 2D Printing</b><br><br>You will be given a 1D array as input and you need to split that and print as 2D array in such a way that data in alternate rows are in reverse order.<br><br>Inputs:<br><br>Input 1: Array of integers separated by comma<br><br>Eg: 1,2,3,4,5,6,7,8,9,10,11,12,13,14,15<br><br>Input 2: Print structure<br>Eg: 3x5<br><br>Outputs:<br><br>Since the print structure is 3x5, the output should be printed in such a way that it has 3 rows and 5 columns.<br>If you notice the second row, the numbers are printed as 10 9 8 7 6 instead of 6 7 8 9 10<br><br>Sample Testcase 1 -<br><br>Input:<br><br>1,2,3,4,5,6,7,8,9,10,11,12,13,14,15 3x5<br><br>Output:<br><br>1	2	3	4	5<br>10	9	8	7	6<br>11	12	13	14	15<br><br>Sample Testcase 2 -<br><br>Input:<br><br>1,2,3,4,5,6,7,8,9 3x3<br><br>Output:<br><br>1	2	3<br>6	5	4<br>7	8	9<br><br>', 11, 'TwoDimensionArrayPrinting', 'public class TwoDimensionArrayPrinting {<br><br>&nbsp;&nbsp;&nbsp;&nbsp;public static void main(String[] argsv) {<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;String[] arrayOfIntegersAsString = argsv[0].split(" ")[0].split(",");<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;String matrixStructure = argsv[0].split(" ")[1];<br><br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;int matrixNumberOfRows = Integer.parseInt(matrixStructure.split("x")[0]);<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;int matrixNumberOfColumns = Integer.parseInt(matrixStructure.split("x")[1]);<br><br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;int lengthOfArray = arrayOfIntegersAsString.length;<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;int[] arrayOfIntegers = new int[lengthOfArray];<br><br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;// Converting the String data to Integer<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;for (int i = 0; i < lengthOfArray; i++) {<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;arrayOfIntegers[i] = Integer.parseInt(arrayOfIntegersAsString[i]);<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;}<br><br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;printMatrix(arrayOfIntegers, matrixNumberOfRows, matrixNumberOfColumns);<br><br>&nbsp;&nbsp;&nbsp;&nbsp;}<br><br>&nbsp;&nbsp;&nbsp;&nbsp;// This function to be populated by the candidate<br>&nbsp;&nbsp;&nbsp;&nbsp;public static void printMatrix(int[] arrayOfIntegers, int matrixNumberOfRows, int matrixNumberOfColumns) {<br><br><br>&nbsp;&nbsp;&nbsp;&nbsp;}<br>}'),
	(115, '<b>1D - 2D Printing</b><br><br>You will be given a 1D array as input and you need to split that and print as 2D array in such a way that data in alternate rows are in reverse order.<br><br>Inputs:<br><br>Input 1: Array of integers separated by comma<br><br>Eg: 1,2,3,4,5,6,7,8,9,10,11,12,13,14,15<br><br>Input 2: Print structure<br>Eg: 3x5<br><br>Outputs:<br><br>Since the print structure is 3x5, the output should be printed in such a way that it has 3 rows and 5 columns.<br>If you notice the second row, the numbers are printed as 10 9 8 7 6 instead of 6 7 8 9 10<br><br>Sample Testcase 1 -<br><br>Input:<br><br>1,2,3,4,5,6,7,8,9,10,11,12,13,14,15 3x5<br><br>Output:<br><br>1	2	3	4	5<br>10	9	8	7	6<br>11	12	13	14	15<br><br>Sample Testcase 2 -<br><br>Input:<br><br>1,2,3,4,5,6,7,8,9 3x3<br><br>Output:<br><br>1	2	3<br>6	5	4<br>7	8	9<br><br>', 22, 'TwoDimensionArrayPrinting', '<br>import sys;<br><br><br># This function to be populated by the candidate<br>def  printMatrix(arrayOfIntegers, matrixNumberOfRows, matrixNumberOfColumns) :<br><br>&nbsp;&nbsp;&nbsp;&nbsp;print("None");<br><br># Getting data from Command line arguments<br>arrayOfIntegersAsString = sys.argv[1].split(" ")[0].split(",");<br>matrixStructure = sys.argv[1].split(" ")[1];<br>matrixNumberOfRows = int(matrixStructure.split("x")[0]);<br>matrixNumberOfColumns = int(matrixStructure.split("x")[1]);<br><br>lengthOfArray = len(arrayOfIntegersAsString);<br>arrayOfIntegers=[];<br><br>for i in range(lengthOfArray) :<br>&nbsp;&nbsp;&nbsp;&nbsp;arrayOfIntegers.append(arrayOfIntegersAsString[i]);<br><br>printMatrix(arrayOfIntegers, matrixNumberOfRows, matrixNumberOfColumns);'),
	(116, '<b>Find Movie</b><br><br>Adam has 1000+ movies in his 5TB Harddisk under Tamil, Telugu, English and Hindi folders. He has exported the list of movies from his 5TB Harddisk into a text file (Adam_Movie_Database.txt).<br><br>The text file now has 1000+ movie names separated by new line with the folder names as shown in the image below.<br><br><img class="coding-images" src="assets/question_2.jpg" height="550" width="550"><br><br>Now Adam wants to build an application that allows him to take a movie name as input and shows "Exists" if the movie exist in the text file or "Does not exist" if the movie does not exist in the text file.<br><br>You need to write a piece of code that takes the movie name as input and prints "Exists" or "Does not exist" based on its availability.<br><br>Inputs:<br><br>Movie name as String<br><br>Length of String is greater than 0 and less than 100<br><br>Outputs:<br><br>"Exists" or "Does not exist"<br><br>Sample Testcase 1:<br><br>Input:<br>Avengers<br><br>Output:<br>Exists<br><br>Sample Testcase 2:<br><br>Input:<br>Kuch Kuch Hota Hai<br><br>Output:<br>Does not exist<br><br>', 18, 'FindMovie', '// Function initialisations<br>void readAdamFileContentsAndReturnString();<br><br>char *findMovie(char *);<br><br>char *buffer;<br><br>void main(int argc, char **argv) {&nbsp;<br><br>&nbsp;int movieToFindLength = strlen(argv[1]);<br>&nbsp;<br>&nbsp;readAdamFileContentsAndReturnString();<br>&nbsp;<br>&nbsp;char *result = findMovie(argv[1]);<br>&nbsp;<br>&nbsp;printf("%s", result);<br>&nbsp;<br>&nbsp;free(buffer);<br>}<br><br>// The candidate to complete the code in the below function<br>char *findMovie(char *movieToFind) {<br>&nbsp;<br>&nbsp;return "Null"<br>&nbsp;<br>}<br><br>// Read file and store in global variable<br>void readAdamFileContentsAndReturnString(char *dataFromTextFile) {<br>&nbsp;<br>&nbsp;char *filename = "C:\\\\\\\\EvertzInterviewApplication\\\\\\\\CandidatePrograms\\\\\\\\Adam_Movie_Database.txt";<br>&nbsp;<br>&nbsp;FILE *fp;<br>&nbsp;long lSize;<br>&nbsp;char *data;<br><br>&nbsp;fp = fopen ( filename , "rb" );<br>&nbsp;if( !fp ) perror(filename),exit(1);<br><br>&nbsp;fseek( fp , 0L , SEEK_END);<br>&nbsp;lSize = ftell( fp );<br>&nbsp;rewind( fp );<br><br>&nbsp;/* allocate memory for entire content */<br>&nbsp;buffer = calloc( 1, lSize+1 );<br>&nbsp;if( !buffer ) fclose(fp),fputs("memory alloc fails",stderr),exit(1);<br><br>&nbsp;/* copy the file into the buffer */<br>&nbsp;if( 1!=fread( buffer , lSize, 1 , fp) )<br>&nbsp;  fclose(fp),free(buffer),fputs("entire read fails",stderr),exit(1);<br>&nbsp;<br>&nbsp;fclose(fp);<br>}'),
	(117, '<b>Find Movie</b><br><br>Adam has 1000+ movies in his 5TB Harddisk under Tamil, Telugu, English and Hindi folders. He has exported the list of movies from his 5TB Harddisk into a text file (Adam_Movie_Database.txt).<br><br>The text file now has 1000+ movie names separated by new line with the folder names as shown in the image below.<br><br><img class="coding-images" src="assets/question_2.jpg" height="550" width="550"><br><br>Now Adam wants to build an application that allows him to take a movie name as input and shows "Exists" if the movie exist in the text file or "Does not exist" if the movie does not exist in the text file.<br><br>You need to write a piece of code that takes the movie name as input and prints "Exists" or "Does not exist" based on its availability.<br><br>Inputs:<br><br>Movie name as String<br><br>Length of String is greater than 0 and less than 100<br><br>Outputs:<br><br>"Exists" or "Does not exist"<br><br>Sample Testcase 1:<br><br>Input:<br>Avengers<br><br>Output:<br>Exists<br><br>Sample Testcase 2:<br><br>Input:<br>Kuch Kuch Hota Hai<br><br>Output:<br>Does not exist<br><br>', 17, 'FindMovie', 'class FindMovie {<br>&nbsp;public:<br>&nbsp;<br>&nbsp;char *buffer;<br>&nbsp;<br>&nbsp;void runProgram(char **argsv) {<br>&nbsp;&nbsp;<br>&nbsp;&nbsp;int movieToFindLength = strlen(argsv[1]);<br>&nbsp;<br>&nbsp;&nbsp;readAdamFileContentsAndReturnString();<br>&nbsp;&nbsp;<br>&nbsp;&nbsp;char *result = findMovie(argsv[1]);<br>&nbsp;&nbsp;<br>&nbsp;&nbsp;cout<<result;<br>&nbsp;&nbsp;<br>&nbsp;&nbsp;free(buffer);<br>&nbsp;}<br>&nbsp;<br>&nbsp;// The candidate to complete the code in the below function<br>&nbsp;public: char *findMovie(char *movieToFind) {<br>&nbsp;&nbsp;<br>&nbsp;&nbsp;<br>&nbsp;}<br><br>&nbsp;// Read file and store in global variable<br>&nbsp;public: void readAdamFileContentsAndReturnString() {<br>&nbsp;&nbsp;<br>&nbsp;&nbsp;const char *filename = "C:\\\\\\\\EvertzInterviewApplication\\\\\\\\CandidatePrograms\\\\\\\\Adam_Movie_Database.txt";<br>&nbsp;&nbsp;<br>&nbsp;&nbsp;FILE *fp;<br>&nbsp;&nbsp;long lSize;<br>&nbsp;&nbsp;char *data;<br><br>&nbsp;&nbsp;fp = fopen ( filename , "rb" );<br>&nbsp;&nbsp;if( !fp ) perror(filename),exit(1);<br><br>&nbsp;&nbsp;fseek( fp , 0L , SEEK_END);<br>&nbsp;&nbsp;lSize = ftell( fp );<br>&nbsp;&nbsp;rewind( fp );<br><br>&nbsp;&nbsp;/* allocate memory for entire content */<br>&nbsp;&nbsp;buffer = (char*)calloc( 1, lSize+1 );<br>&nbsp;&nbsp;if( !buffer ) fclose(fp),fputs("memory alloc fails",stderr),exit(1);<br><br>&nbsp;&nbsp;/* copy the file into the buffer */<br>&nbsp;&nbsp;if( 1!=fread( buffer , lSize, 1 , fp) )<br>&nbsp;&nbsp;  fclose(fp),free(buffer),fputs("entire read fails",stderr),exit(1);<br>&nbsp;&nbsp;<br>&nbsp;&nbsp;fclose(fp);<br>&nbsp;}<br>};<br><br>int main(int argc, char **argv) {&nbsp;<br><br>&nbsp;class FindMovie fm;<br>&nbsp;<br>&nbsp;fm.runProgram(argv);<br>&nbsp;<br>&nbsp;return 0;<br>}'),
	(118, '<b>New Traffic Rules Penalties</b><br><br>According to new traffic rules, the charges for different violations is changed. <br>New charges are listed below. <br><br>There will be two cases when vehicle will be seized.<br>1. If the total amount of fine is more than 2000<br>2. If there are more than three violations<br><br>You need to write a code to calculate total amount fined for a person and determine if licence will be seized or not.<br><br>No Helmet - 1000<br>No Driving Licence - 500<br>No Registration Certificate - 300<br>Triple Riding - 1000<br>No Seat Belt - 1500<br>Drink and Drive - 4000<br>Minor Driving - 1000<br>Using cellphone while driving - 500<br>Signal jumping - 300<br>Wrong way driving - 1000<br><br>The above data is already stored in two arrays in the code snippet.<br><br>Sample Testcase 1:<br><br>Inputs:<br><br>No Helmet,No Driving Licence,No Registration Certificate<br><br>Outputs:<br><br>1800,Vehicle will not be seized<br><br><br>Sample Testcase 2:<br><br>Inputs:<br><br>Minor Driving,Drink and Drive<br><br>Outputs:<br><br>3000,Vehicle will be seized<br><br>', 18, 'TrafficPenalties', '// Function initialisations<br>char *calculatePenalties(const char **, const int [], char **, int, int);<br><br>void main(int argc, char **argv) {&nbsp;<br>&nbsp;<br>&nbsp;const char *violationConditions[] = {"No Helmet", "No Driving Licence", "No Registration Certificate", "Triple Riding", "No Seat Belt", "Drink and Drive", "Minor Driving", "Using cellphone while driving", "Signal jumping", "Wrong way driving"};<br>&nbsp;const int violationPenalties[] = {1000, 500, 300, 1000, 1500, 3000, 1000, 500, 300, 1000};<br>&nbsp;const int totalTracking = 10;<br>&nbsp;<br>&nbsp;char delim[] = ",";<br>&nbsp;<br>&nbsp;char *violationsArray = strtok(argv[1], delim);<br>&nbsp;<br>&nbsp;char **violationsByIndividual = (char **)malloc(sizeof(char *) * strlen(argv[1]));<br>&nbsp;<br>&nbsp;int numberOfViolations = 0;<br>&nbsp;int length = 0;<br>&nbsp;<br>&nbsp;while(violationsArray != NULL)<br>&nbsp;{<br>&nbsp;&nbsp;length = strlen(violationsArray);<br>&nbsp;&nbsp;<br>&nbsp;&nbsp;violationsByIndividual[numberOfViolations] = (char *)malloc(length * sizeof(char));<br>&nbsp;&nbsp;strcpy(violationsByIndividual[numberOfViolations++], violationsArray);<br>&nbsp;&nbsp;&nbsp;<br>&nbsp;&nbsp;violationsArray = strtok(NULL, delim);<br>&nbsp;}<br>&nbsp;<br>&nbsp;char *finalAmount = calculatePenalties(violationConditions, violationPenalties, violationsByIndividual, numberOfViolations, totalTracking);<br>&nbsp;<br>&nbsp;printf("%s", finalAmount);<br>}<br><br>// The candidate to complete the code in the below function<br>char *calculatePenalties(const char **violationConditions, const int violationPenalties[], char **violationsByIndividual, int numberOfViolations, int totalTracking) {<br>&nbsp;<br>&nbsp;return "Null";<br>}'),
	(119, '<b>New Traffic Rules Penalties</b><br><br>According to new traffic rules, the charges for different violations is changed. <br>New charges are listed below. <br><br>There will be two cases when vehicle will be seized.<br>1. If the total amount of fine is more than 2000<br>2. If there are more than three violations<br><br>You need to write a code to calculate total amount fined for a person and determine if licence will be seized or not.<br><br>No Helmet - 1000<br>No Driving Licence - 500<br>No Registration Certificate - 300<br>Triple Riding - 1000<br>No Seat Belt - 1500<br>Drink and Drive - 4000<br>Minor Driving - 1000<br>Using cellphone while driving - 500<br>Signal jumping - 300<br>Wrong way driving - 1000<br><br>The above data is already stored in two arrays in the code snippet.<br><br>Sample Testcase 1:<br><br>Inputs:<br><br>No Helmet,No Driving Licence,No Registration Certificate<br><br>Outputs:<br><br>1800,Vehicle will not be seized<br><br><br>Sample Testcase 2:<br><br>Inputs:<br><br>Minor Driving,Drink and Drive<br><br>Outputs:<br><br>3000,Vehicle will be seized<br><br>', 17, 'TrafficPenalties', 'class TrafficPenalties {<br>&nbsp;<br>&nbsp;public:<br>&nbsp;<br>&nbsp;// This function to be populated by the candidate<br>&nbsp;char *calculatePenalties(const char **violationConditions, const int violationPenalties[], char **violationsByIndividual, int numberOfViolations, int totalTracking) {<br>&nbsp;&nbsp;<br>&nbsp;&nbsp;return "Null";<br>&nbsp;}<br>};<br><br>int main(int argc, char **argv) {&nbsp;<br><br>&nbsp;const char *violationConditions[] = {"No Helmet", "No Driving Licence", "No Registration Certificate", "Triple Riding", "No Seat Belt", "Drink and Drive", "Minor Driving", "Using cellphone while driving", "Signal jumping", "Wrong way driving"};<br>&nbsp;const int violationPenalties[] = {1000, 500, 300, 1000, 1500, 3000, 1000, 500, 300, 1000};<br>&nbsp;const int totalTracking = 10;<br><br>&nbsp;char delim[] = ",";<br>&nbsp;<br>&nbsp;char *violationsArray = strtok(argv[1], delim);<br>&nbsp;<br>&nbsp;char **violationsByIndividual = (char **)malloc(sizeof(char *) * strlen(argv[1]));<br>&nbsp;&nbsp;int numberOfViolations = 0;<br>&nbsp;int length = 0;<br>&nbsp;<br>&nbsp;while(violationsArray != NULL)<br>&nbsp;{<br>&nbsp;&nbsp;length = strlen(violationsArray);<br>&nbsp;&nbsp;<br>&nbsp;&nbsp;violationsByIndividual[numberOfViolations] = (char *)malloc(length * sizeof(char));<br>&nbsp;&nbsp;strcpy(violationsByIndividual[numberOfViolations++], violationsArray);<br>&nbsp;&nbsp;&nbsp;<br>&nbsp;&nbsp;violationsArray = strtok(NULL, delim);<br>&nbsp;}<br>&nbsp;<br>&nbsp;<br>&nbsp;class TrafficPenalties tp;<br>&nbsp;<br>&nbsp;char *finalAmount = tp.calculatePenalties(violationConditions, violationPenalties, violationsByIndividual, numberOfViolations, totalTracking);<br>&nbsp;<br>&nbsp;cout<<finalAmount;<br>&nbsp;<br>&nbsp;return 0;<br>}');
/*!40000 ALTER TABLE `coding_questions` ENABLE KEYS */;

-- Dumping structure for table evertz_interview_app.coding_test_cases
DROP TABLE IF EXISTS `coding_test_cases`;
CREATE TABLE IF NOT EXISTS `coding_test_cases` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `CODING_QUESTION_ID` int(11) NOT NULL,
  `TEST_CASE_DATA` varchar(512) NOT NULL,
  `TEST_CASE_ANSWER` varchar(512) NOT NULL,
  `TEST_CASE_IS_HIDDEN` bit(1) NOT NULL DEFAULT b'1' COMMENT '0 - Shown testcase\n1 - Hidden testcase',
  PRIMARY KEY (`ID`),
  KEY `CODING_QUESTION_ID1` (`CODING_QUESTION_ID`),
  CONSTRAINT `CODING_QUESTION_ID1` FOREIGN KEY (`CODING_QUESTION_ID`) REFERENCES `coding_questions` (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=826 DEFAULT CHARSET=latin1;

-- Dumping data for table evertz_interview_app.coding_test_cases: ~140 rows (approximately)
DELETE FROM `coding_test_cases`;
/*!40000 ALTER TABLE `coding_test_cases` DISABLE KEYS */;
INSERT INTO `coding_test_cases` (`ID`, `CODING_QUESTION_ID`, `TEST_CASE_DATA`, `TEST_CASE_ANSWER`, `TEST_CASE_IS_HIDDEN`) VALUES
	(452, 60, '15,1,7,4,3,6,12,1,10,3 3', '1 2,3 3<br>', b'0'),
	(453, 60, '1,5,99,52,8,25,68,74,14,5,23,7,92,32 7', '6 1,5 1<br>', b'0'),
	(454, 60, '45,1,7,4,3,6,12,1,10,3,43,27,19,20,44,16,18,19,33,36,37,38,39 0', '0 0,22 0<br>', b'0'),
	(455, 60, '15,1,5,4,2,6,12,1,11,3 6', '5 1,3 0<br>', b'1'),
	(456, 60, '1,5,99,52,8,23,68,74,420,5,23,7,333,32,198,134,156,92,145,120,450,14 2', '2 0,11 8<br>', b'1'),
	(457, 60, '45,1,7,4,3,6,12,1,10,3,43,27,19,45,56,777,347,1 5', '3 2,3 9<br>', b'1'),
	(458, 60, '2,2,3,2,2 2', '2 0,2 0<br>', b'1'),
	(459, 60, '123,245,991,512,18,215,638,754,14,1,23,3,929,324 6', '5 1,5 2<br>', b'1'),
	(460, 60, '15,441,445,4,26,644,142,166,161,35 3', '0 3,0 6<br>', b'1'),
	(461, 60, '4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4 3', '0 0,0 0<br>', b'1'),
	(493, 65, '30,20,15 15,-5,10', '70.0<br>', b'0'),
	(494, 65, '10,20,15,15 10,-10,25,30', '67.25<br>', b'0'),
	(500, 65, '20,30,25,25 10,20,35,40', '126.75<br>', b'0'),
	(501, 65, '40,15,10,15 20,30,15,10', '95.5<br>', b'1'),
	(502, 65, '50,60,70,80 10,15,20,25', '308.0<br>', b'1'),
	(503, 65, '10,20,30,40 5,10,15,20\r ', '115.0<br>', b'1'),
	(504, 65, '60,70,80,10 15,5,10,2', '240.7<br>', b'1'),
	(505, 65, '100,90,80,70 20,30,5,6', '395.2<br>', b'1'),
	(506, 65, '110,120,160,175 36,8,17,3', '646.65<br>', b'1'),
	(507, 65, '210,320,160,75 40,32,5,9', '966.15<br>', b'1'),
	(538, 64, '15,1,7,4,3,6,12,1,10,3 3', '1 2,3 3<br>', b'0'),
	(539, 64, '1,5,99,52,8,25,68,74,14,5,23,7,92,32 7', '6 1,5 1<br>', b'0'),
	(540, 64, '45,1,7,4,3,6,12,1,10,3,43,27,19,20,44,16,18,19,33,36,37,38,39 0', '0 0,22 0<br>', b'0'),
	(541, 64, '15,1,5,4,2,6,12,1,11,3 6', '5 1,3 0<br>', b'1'),
	(542, 64, '1,5,99,52,8,23,68,74,420,5,23,7,333,32,198,134,156,92,145,120,450,14 2', '2 0,11 8<br>', b'1'),
	(543, 64, '45,1,7,4,3,6,12,1,10,3,43,27,19,45,56,777,347,1 5', '3 2,3 9<br>', b'1'),
	(544, 64, '2,2,3,2,2 2', '2 0,2 0<br>', b'1'),
	(545, 64, '123,245,991,512,18,215,638,754,14,1,23,3,929,324 6', '5 1,5 2<br>', b'1'),
	(546, 64, '15,441,445,4,26,644,142,166,161,35 3', '0 3,0 6<br>', b'1'),
	(547, 64, '4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4 3', '0 0,0 0<br>', b'1'),
	(588, 111, '1,2,3,4,5,6,7,8,9,10,11,12,13,14,15 3x5', '1 2 3 4 5<br>10 9 8 7 6<br>11 12 13 14 15<br>', b'0'),
	(589, 111, '1,2,3,4,5,6,7,8,9 3x3', '1 2 3<br>6 5 4<br>7 8 9<br>', b'0'),
	(590, 111, '1,1,1,1,1,1,1,1,1,1,11,1 3x4', '1 1 1 1<br>1 1 1 1<br>1 1 11 1<br>', b'0'),
	(591, 111, '3,4,5,7,9,11 2x3', '3 4 5<br>11 9 7<br>', b'1'),
	(592, 111, '3,4,5,7,9,11 2x3', '3 4 5<br>11 9 7<br>', b'1'),
	(593, 111, '0,9,45,688,690,55,-2,34,89,90,65,78,90,-22,-34,45,-87,3,67,19 4x5', '0 9 45 688 690<br>90 89 34 -2 55<br>65 78 90 -22 -34<br>19 67 3 -87 45<br>', b'1'),
	(594, 111, '3,4,5,75,-3,-5 2x3', '3 4 5<br>-5 -3 75<br>', b'1'),
	(595, 111, '7,8,0,4,5,6,2,3,2,32,12,67,24,-1,-2,3 4x4', '7 8 0 4<br>3 2 6 5<br>2 32 12 67<br>3 -2 -1 24<br>', b'1'),
	(596, 111, '65,3,3,3,3,3,3,3,3,3 2x5', '65 3 3 3 3<br>3 3 3 3 3<br>', b'1'),
	(597, 111, '2,2,2,2,2,2,2,2,2,2,2,2,2,2 2x7', '2 2 2 2 2 2 2<br>2 2 2 2 2 2 2<br>', b'1'),
	(628, 106, 'Car:No Seat Belt,Drink and Drive', '4500,Vehicle will be seized<br>', b'0'),
	(629, 106, 'Bike:No Helmet', '1000,Vehicle will not be seized<br>', b'0'),
	(630, 106, 'Bike:Triple Riding,Signal jumping,Wrong way driving', '2300,Vehicle will be seized<br>', b'0'),
	(631, 106, 'Bike:Using cellphone while driving,Drink and Drive,Minor Driving', '4500,Vehicle will be seized<br>', b'1'),
	(632, 106, 'Bike:Signal jumping,No Registration Certificate,Wrong way driving,No Driving Licence', '2100,Vehicle will be seized<br>', b'1'),
	(633, 106, 'Car:Drink and Drive,Using cellphone while driving,No Driving Licence', '4000,Vehicle will be seized<br>', b'1'),
	(634, 106, 'Car:Wrong way driving,Signal jumping,No Driving Licence,Minor Driving', '2800,Vehicle will be seized<br>', b'1'),
	(635, 106, 'Bike:No Helmet,No Driving Licence,No Registration Certificate', '1800,Vehicle will not be seized<br>', b'1'),
	(636, 106, 'Car:Drink and Drive', '3000,Vehicle will not be seized<br>', b'1'),
	(637, 106, 'Car:No Seat Belt,No Driving Licence,No Registration Certificate,Using cellphone while driving', '2800,Vehicle will be seized<br>', b'1'),
	(668, 110, 'Car:No Seat Belt,Drink and Drive', '4500,Vehicle will be seized<br>', b'0'),
	(669, 110, 'Bike:No Helmet,Triple Riding,Minor Driving', '3000,Vehicle will be seized<br>', b'0'),
	(670, 110, 'Bike:Triple Riding,Signal jumping,Wrong way driving', '2300,Vehicle will be seized<br>', b'0'),
	(671, 110, 'Bike:Using cellphone while driving,Drink and Drive,Minor Driving', '4500,Vehicle will be seized<br>', b'0'),
	(672, 110, 'Bike:Signal jumping,No Registration Certificate,Wrong way driving,No Driving Licence', '2100,Vehicle will be seized<br>', b'0'),
	(673, 110, 'Car:Drink and Drive,Using cellphone while driving,No Driving Licence', '4000,Vehicle will be seized<br>', b'0'),
	(674, 110, 'Car:Wrong way driving,Signal jumping,No Driving Licence,Minor Driving', '2800,Vehicle will be seized<br>', b'0'),
	(675, 110, 'Bike:No Helmet,No Driving Licence,No Registration Certificate', '1800,Vehicle will not be seized<br>', b'0'),
	(676, 110, 'Car:Drink and Drive', '3000,Vehicle will not be seized<br>', b'0'),
	(677, 110, 'Car:No Seat Belt,No Driving Licence,No Registration Certificate,Using cellphone while driving', '2800,Vehicle will be seized<br>', b'0'),
	(678, 101, 'Avengers', 'Exists,2<br>', b'0'),
	(679, 101, 'Kuch Kuch Hota Hai', 'Does not exist,0<br>', b'0'),
	(680, 101, 'True Lies', 'Exists,1<br>', b'0'),
	(681, 101, 'The Family Man', 'Does not exist,0<br>', b'1'),
	(682, 101, 'Day of the Jackal', 'Exists,1<br>', b'1'),
	(683, 101, 'Alex Pandian', 'Exists,1<br>', b'1'),
	(684, 101, 'Exorcist', 'Exists,1<br>', b'1'),
	(685, 101, 'Article 1', 'Exists,1<br>', b'1'),
	(686, 101, 'Hits for Kids', 'Exists,1<br>', b'1'),
	(687, 101, 'Adanga Maru', 'Exists,1<br>', b'1'),
	(718, 105, 'Avengers', 'Exists,2<br>', b'0'),
	(719, 105, 'Kuch Kuch Hota Hai', 'Does not exist,0<br>', b'0'),
	(720, 105, 'True Lies', 'Exists,1<br>', b'0'),
	(721, 105, 'The Family Man', 'Does not exist,0<br>', b'1'),
	(722, 105, 'Day of the Jackal', 'Exists,1<br>', b'1'),
	(723, 105, 'Alex Pandian', 'Exists,1<br>', b'1'),
	(724, 105, 'Exorcist', 'Exists,1<br>', b'1'),
	(725, 105, 'Article 1', 'Exists,1<br>', b'1'),
	(726, 105, 'Hits for Kids', 'Exists,1<br>', b'1'),
	(727, 105, 'Adanga Maru', 'Exists,1<br>', b'1'),
	(728, 69, '30,20,15 15,-5,10', '70.0', b'0'),
	(729, 69, '10,20,15,15 10,-10,25,30', '67.25', b'0'),
	(730, 69, '20,30,25,25 10,20,35,40', '126.75', b'0'),
	(731, 69, '40,15,10,15 20,30,15,10', '95.5', b'1'),
	(732, 69, '50,60,70,80 10,15,20,25', '308.0', b'1'),
	(733, 69, '10,20,30,40 5,10,15,20', '115.0', b'1'),
	(734, 69, '60,70,80,10 15,5,10,2', '240.7', b'1'),
	(735, 69, '100,90,80,70 20,30,5,6', '395.2', b'1'),
	(736, 69, '110,120,160,175 36,8,17,3', '646.65', b'1'),
	(737, 69, '210,320,160,75 40,32,5,9', '966.15', b'1'),
	(748, 115, '1,2,3,4,5,6,7,8,9,10,11,12,13,14,15 3x5', '1 2 3 4 5<br>10 9 8 7 6<br>11 12 13 14 15<br>', b'0'),
	(749, 115, '1,2,3,4,5,6,7,8,9 3x3', '1 2 3<br>6 5 4<br>7 8 9<br>', b'0'),
	(750, 115, '1,1,1,1,1,1,1,1,1,1,11,1 3x4', '1 1 1 1<br>1 1 1 1<br>1 1 11 1<br>', b'0'),
	(751, 115, '1,1,1,1,1,1,1,1,1,1,11,1 3x4', '1 1 1 1<br>1 1 1 1<br>1 1 11 1<br>', b'1'),
	(752, 115, '-1,-2,-3,-4,5,65,78,-1,0,0,0,45 3x4', '-1 -2 -3 -4<br>-1 78 65 5<br>0 0 0 45<br>', b'1'),
	(753, 115, '0,9,45,688,690,55,-2,34,89,90,65,78,90,-22,-34,45,-87,3,67,19 4x5', '0 9 45 688 690<br>90 89 34 -2 55<br>65 78 90 -22 -34<br>19 67 3 -87 45<br>', b'1'),
	(754, 115, '3,4,5,75,-3,-5 2x3', '3 4 5<br>-5 -3 75<br>', b'1'),
	(755, 115, '7,8,0,4,5,6,2,3,2,32,12,67,24,-1,-2,3 4x4', '7 8 0 4<br>3 2 6 5<br>2 32 12 67<br>3 -2 -1 24<br>', b'1'),
	(756, 115, '65,3,3,3,3,3,3,3,3,3 2x5', '65 3 3 3 3<br>3 3 3 3 3<br>', b'1'),
	(757, 115, '2,2,2,2,2,2,2,2,2,2,2,2,2,2 2x7', '2 2 2 2 2 2 2<br>2 2 2 2 2 2 2<br>', b'1'),
	(786, 116, 'Avengers', 'Exists', b'0'),
	(787, 116, 'Kuch Kuch Hota Hai', 'Does not exist', b'0'),
	(788, 116, 'True Lies', 'Exists', b'0'),
	(789, 116, 'The Family Man', 'Does not exist', b'1'),
	(790, 116, 'Day of the Jackal', 'Exists', b'1'),
	(791, 116, 'Alex Pandian', 'Exists', b'1'),
	(792, 116, 'Exorcist', 'Exists', b'1'),
	(793, 116, 'Article 1', 'Exists', b'1'),
	(794, 116, 'Hits for Kids', 'Exists', b'1'),
	(795, 116, 'Adanga Maru', 'Exists', b'1'),
	(796, 117, 'Avengers', 'Exists', b'0'),
	(797, 117, 'Kuch Kuch Hota Hai', 'Does not exist', b'0'),
	(798, 117, 'True Lies', 'Exists', b'0'),
	(799, 117, 'The Family Man', 'Does not exist', b'1'),
	(800, 117, 'Day of the Jackal', 'Exists', b'1'),
	(801, 117, 'Alex Pandian', 'Exists', b'1'),
	(802, 117, 'Exorcist', 'Exists', b'1'),
	(803, 117, 'Article 1', 'Exists', b'1'),
	(804, 117, 'Hits for Kids', 'Exists', b'1'),
	(805, 117, 'Adanga Maru', 'Exists', b'1'),
	(806, 118, 'No Seat Belt,No Driving Licence,No Registration Certificate,Using cellphone while driving', '2800,Vehicle will be seized', b'0'),
	(807, 118, 'Drink and Drive', '3000,Vehicle will be seized', b'0'),
	(808, 118, 'No Helmet,No Driving Licence,No Registration Certificate', '1800,Vehicle will not be seized', b'0'),
	(809, 118, 'Wrong way driving,Signal jumping,No Driving Licence,Minor Driving', '2800,Vehicle will be seized', b'1'),
	(810, 118, 'Drink and Drive,Using cellphone while driving,No Driving Licence', '4000,Vehicle will be seized', b'1'),
	(811, 118, 'Signal jumping,No Registration Certificate,Wrong way driving,No Driving Licence', '2100,Vehicle will be seized', b'1'),
	(812, 118, 'Using cellphone while driving,Drink and Drive,Minor Driving', '4500,Vehicle will be seized', b'1'),
	(813, 118, 'Triple Riding,Signal jumping,Wrong way driving', '2300,Vehicle will be seized', b'1'),
	(814, 118, 'No Helmet', '1000,Vehicle will not be seized', b'1'),
	(815, 118, 'No Seat Belt,Drink and Drive', '4500,Vehicle will be seized', b'1'),
	(816, 119, 'No Seat Belt,No Driving Licence,No Registration Certificate,Using cellphone while driving', '2800,Vehicle will be seized', b'0'),
	(817, 119, 'Drink and Drive', '3000,Vehicle will be seized', b'0'),
	(818, 119, 'No Helmet,No Driving Licence,No Registration Certificate', '1800,Vehicle will not be seized', b'0'),
	(819, 119, 'Wrong way driving,Signal jumping,No Driving Licence,Minor Driving', '2800,Vehicle will be seized', b'1'),
	(820, 119, 'Drink and Drive,Using cellphone while driving,No Driving Licence', '4000,Vehicle will be seized', b'1'),
	(821, 119, 'Signal jumping,No Registration Certificate,Wrong way driving,No Driving Licence', '2100,Vehicle will be seized', b'1'),
	(822, 119, 'Using cellphone while driving,Drink and Drive,Minor Driving', '4500,Vehicle will be seized', b'1'),
	(823, 119, 'Triple Riding,Signal jumping,Wrong way driving', '2300,Vehicle will be seized', b'1'),
	(824, 119, 'No Helmet', '1000,Vehicle will not be seized', b'1'),
	(825, 119, 'No Seat Belt,Drink and Drive', '4500,Vehicle will be seized', b'1');
/*!40000 ALTER TABLE `coding_test_cases` ENABLE KEYS */;

-- Dumping structure for table evertz_interview_app.college_details
DROP TABLE IF EXISTS `college_details`;
CREATE TABLE IF NOT EXISTS `college_details` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `NAME` varchar(200) NOT NULL,
  `CURRENT_STATUS` int(11) DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=18 DEFAULT CHARSET=utf8;

-- Dumping data for table evertz_interview_app.college_details: ~2 rows (approximately)
DELETE FROM `college_details`;
/*!40000 ALTER TABLE `college_details` DISABLE KEYS */;
INSERT INTO `college_details` (`ID`, `NAME`, `CURRENT_STATUS`) VALUES
	(12, 'NMIT', 0),
	(14, 'BMSIT', 1);
/*!40000 ALTER TABLE `college_details` ENABLE KEYS */;

-- Dumping structure for table evertz_interview_app.degree
DROP TABLE IF EXISTS `degree`;
CREATE TABLE IF NOT EXISTS `degree` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `NAME` varchar(50) NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=20 DEFAULT CHARSET=utf8;

-- Dumping data for table evertz_interview_app.degree: ~6 rows (approximately)
DELETE FROM `degree`;
/*!40000 ALTER TABLE `degree` DISABLE KEYS */;
INSERT INTO `degree` (`ID`, `NAME`) VALUES
	(1, 'B.E.'),
	(11, 'MCA'),
	(12, 'B.Tech'),
	(17, 'BCA'),
	(18, 'M.E'),
	(19, 'M.Tech');
/*!40000 ALTER TABLE `degree` ENABLE KEYS */;

-- Dumping structure for table evertz_interview_app.modules
DROP TABLE IF EXISTS `modules`;
CREATE TABLE IF NOT EXISTS `modules` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `NAME` varchar(50) NOT NULL,
  `MANDATORY_MODULE` bit(1) NOT NULL,
  `FILE_EXTENSION` varchar(10) DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=23 DEFAULT CHARSET=utf8;

-- Dumping data for table evertz_interview_app.modules: ~10 rows (approximately)
DELETE FROM `modules`;
/*!40000 ALTER TABLE `modules` DISABLE KEYS */;
INSERT INTO `modules` (`ID`, `NAME`, `MANDATORY_MODULE`, `FILE_EXTENSION`) VALUES
	(11, 'Java', b'0', 'java'),
	(14, 'Networking', b'1', NULL),
	(15, 'Linux', b'1', NULL),
	(17, 'C++', b'0', 'cpp'),
	(18, 'C', b'0', 'c'),
	(19, 'Broadcast', b'1', NULL),
	(20, 'Analytical', b'1', NULL),
	(21, 'SQL', b'1', 'sql'),
	(22, 'Python', b'0', 'py');
/*!40000 ALTER TABLE `modules` ENABLE KEYS */;

-- Dumping structure for table evertz_interview_app.questions
DROP TABLE IF EXISTS `questions`;
CREATE TABLE IF NOT EXISTS `questions` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `QUESTION` text NOT NULL,
  `OPTION1` varchar(500) NOT NULL,
  `OPTION2` varchar(500) NOT NULL,
  `OPTION3` varchar(500) NOT NULL,
  `OPTION4` varchar(500) NOT NULL,
  `ANSWER` int(1) NOT NULL,
  `MODULE_ID` int(1) NOT NULL,
  PRIMARY KEY (`ID`),
  KEY `MODULE_ID` (`MODULE_ID`),
  CONSTRAINT `MODULE_ID` FOREIGN KEY (`MODULE_ID`) REFERENCES `modules` (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=1192 DEFAULT CHARSET=utf8;

-- Dumping data for table evertz_interview_app.questions: ~91 rows (approximately)
DELETE FROM `questions`;
/*!40000 ALTER TABLE `questions` DISABLE KEYS */;
INSERT INTO `questions` (`ID`, `QUESTION`, `OPTION1`, `OPTION2`, `OPTION3`, `OPTION4`, `ANSWER`, `MODULE_ID`) VALUES
	(246, 'What is the output of the JAVA program?<br><br>final class Complex {<br>&nbsp;private final double re;<br>&nbsp;private final double im;<br><br>&nbsp;public Complex(double re, double im) {<br>&nbsp;&nbsp;this.re = re;<br>&nbsp;&nbsp;this.im = im;<br>&nbsp;}<br><br>&nbsp;public String toString() {<br>&nbsp;&nbsp;return "(" + re + " + " + im + "i)";<br>&nbsp}<br>}<br><br>class Main {<br>\r   &nbsp;public static void main(String args[])<br>&nbsp;{<br>&nbsp;&nbsp;Complex c = new Complex(10, 15);<br>&nbsp;&nbsp;System.out.println("Complex number is " + c);<br>&nbsp;}<br>}<br>', 'Complex number is (10.0 + 15.0i)', 'Compiler Error', 'Complex number is SOME_GARBAGE', 'Complex number is Complex@8e2fb5 (Here 8e2fb5 is hash code of c)', 1, 11),
	(247, 'Predict the output of following program. Note that fun() is public in base and private in derived.<br><br>class Base {<br>&nbsp;public void foo() { System.out.println("Base"); }<br>}<br><br>class Derived extends Base {<br>&nbsp;private void foo() { System.out.println("Derived"); }<br> ', 'Based', 'Derived', 'Compiler Error ', 'Runtime Error', 3, 11),
	(248, 'What is the output of the program?<br><br>public class Test<br>{<br>&nbsp;&nbsp;private int data = 5;<br><br>&nbsp;&nbsp;public int getData()<br>&nbsp;&nbsp;{<br>&nbsp;&nbsp;&nbsp;&nbsp;return this.data;<br>&nbsp;&nbsp;}<br><br>&nbsp;&nbsp;public int getData(int value)<br>&nbsp;&nbsp;{<br>&nbsp;&nbsp;&nbsp;&nbsp;return (data+1);<br>&nbsp;&nbsp;}<br><br>&nbsp;&nbsp;public int getData(int... value)<br>&nbsp;&nbsp;{<br>&nbsp;&nbsp;&nbsp;&nbsp;return  (data+2);<br>&nbsp;&nbsp;}<br><br>&nbsp;&nbsp;public static void main(String[] args)<br>&nbsp;&nbsp;{<br>&nbsp;&nbsp;&nbsp;&nbsp;Test temp = new Test();<br>&nbsp;&nbsp;&nbsp;&nbsp;System.out.println(temp.getData(7, 8, 12));<br>&nbsp;&nbsp;}<br>} ', '7', '8', '10', 'Either Compile time or Runtime error', 1, 11),
	(249, 'Predict the output of following Java code<br><br>import java.io.IOException;<br>import java.util.EmptyStackException;<br><br>public class newclass<br>{<br>&nbsp;&nbsp;public static void main(String[] args)<br>&nbsp;&nbsp;{<br>&nbsp;&nbsp;&nbsp;&nbsp;try<br>&nbsp;&nbsp;&nbsp;&nbsp;{<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;System.out.printf("%d", 1);<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;throw(new Exception());<br>&nbsp;&nbsp;&nbsp;&nbsp;}<br>&nbsp;&nbsp;&nbsp;&nbsp;catch(IOException e)<br>&nbsp;&nbsp;&nbsp;&nbsp;{<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;System.out.printf("%d", 2);<br>&nbsp;&nbsp;&nbsp;&nbsp;}<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;catch(EmptyStackException e)<br>&nbsp;&nbsp;&nbsp;&nbsp;{<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;System.out.printf("%d", 3);<br>&nbsp;&nbsp;&nbsp;&nbsp;}<br>&nbsp;&nbsp;&nbsp;&nbsp;catch(Exception e)<br>&nbsp;&nbsp;&nbsp;&nbsp;{<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;System.out.printf("%d", 4);<br>&nbsp;&nbsp;&nbsp;&nbsp;}<br>&nbsp;&nbsp;&nbsp;&nbsp;finally<br>&nbsp;&nbsp;&nbsp;&nbsp;{<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;System.out.printf("%d", 5);<br>&nbsp;&nbsp;&nbsp;&nbsp;}<br>&nbsp;&nbsp;}<br>} ', '135', '12345', '145', '15', 3, 11),
	(250, 'If class B is subclassed from class A then which is the correct syntax', 'class B implements A{}', 'class B extends A{}', 'class B:A{}', 'class B extends class A{}', 2, 11),
	(251, 'Which of the following is/are true statements?<br><br>&nbsp;&nbsp;(i)A class can extend only one class but can implement many interfaces<br>&nbsp;&nbsp;(ii)An interface can extend many interfaces<br>&nbsp;&nbsp;(iii)An interface can implement a class<br>&nbsp;&nbsp;(iv)An interface can implement another interface', '(i)', '(i) & (ii)', '(i) & (iii)', 'All of the above.', 2, 11),
	(252, 'Incorrect statement(s) about finally block in java exception ', 'There can be multiple finally blocks followed by try catch block.', 'Finally block always executes whether exception is handled or not.', 'Finally block always follow try catch block.', 'All are correct.', 1, 11),
	(253, 'Transient keyword in java is used in', 'Synchronization', 'Serialization', 'Polymorphism', 'Inheritance', 2, 11),
	(254, 'Which keyword is used to refer current object of a class in Java?', 'This', 'Current', 'Final', 'Static', 1, 11),
	(255, 'The fields in an interface are implicitly specified as,', 'Static only.', 'Final only.', 'Both static and final', 'None of the above.', 3, 11),
	(446, 'Which one is NOT a feature of the Swap space?', 'It is the amount of physical memory that is allocated for use by Linux to hold some concurrent running programs temporarily.', 'It relies on textual request and response transaction process where user types declarative commands to instruct the computer to allocate the space to run the programs.', 'It is used when Ram does not have enough memory to support all concurrent running programs.', 'This memory management involves the swapping of memory to and from physical storage.', 2, 15),
	(447, 'Which command is used to display the amount of available disk space for file systems on which the invoking user has appropriate read access?', 'mount', 'ls', 'df', 'du', 3, 15),
	(448, 'Which of the following is NOT a troubleshooting command?', 'ipconfig', 'hostname', 'nslookup', 'ping', 1, 15),
	(449, 'Which command is used to add a loadable kernel module to the Linux kernel or to remove a loadable kernel module from the kernel?', 'modprobe', 'getopt', 'dpkg', 'xargs', 1, 15),
	(450, 'Which commend is used to query the Domain Name System to obtain domain name or IP address mapping, or other DNS records?', 'netstat', 'netcomm', 'traceroute', 'nslookup', 4, 15),
	(451, 'Which command is used to change the owner of file system files, directories?', 'chgrp', 'chown', 'chmod', 'chusr', 2, 15),
	(452, 'Which command is used to display network connections for Transmission Control Protocol, routing tables, and a number of network interface and network protocol statistics?', 'route', 'ifconfig', 'ps -ef', 'netstat', 4, 15),
	(453, 'What is NOT a feature of Cron?', 'Cron allows the user to schedule tasks to be executed every minute.', 'Tasks can be scheduled by any normal user and are basically used when tasks have to be completed/executed at a particular hour or minute.', 'Cron does not expects the system to be running 24x7', 'It is ideal for servers and not desktops or laptops.', 3, 15),
	(454, 'Which command will look for files with an extension "c", and has the occurrence of the string "apple" in it?', 'find ./ -name "apple" | xargs grep –i "*.c"', 'find ./ -name "*.c" | xargs grep –i "apple"', 'find -name "*.c" | xargs grep "apple"', 'find ./ -name "*.c" | grep –i "apple"', 2, 15),
	(455, 'What is the maximum length for any file name under LINUX?', '264 characters', '1264 characters', '1255 characters', '255 characters', 4, 15),
	(648, 'int main()<br>{<br>&nbsp;&nbsp;char *str="includehelp";<br>&nbsp;&nbsp;printf("%s",str+7);<br>&nbsp;&nbsp;return 0;<br>}', 'help	', 'includehelp	', 'ehelp	', 'None of these	', 1, 18),
	(649, 'void main(){<br>&nbsp;&nbsp;int a=10,b=2,x=0;<br>&nbsp;&nbsp;x=a+b*a+10/2*a;<br>&nbsp;&nbsp;printf("value is =%d",x);<br>}', 'value is =1250	', 'value is =80	', 'value is =125	', 'ERROR	', 2, 18),
	(650, 'int main()<br>{<br>&nbsp;&nbsp;int a=10;<br>&nbsp;&nbsp;int b=2;<br>&nbsp;&nbsp;int c;<br>&nbsp;&nbsp;c=(a & b);<br>&nbsp;&nbsp;printf("c= %d",c);<br>&nbsp;&nbsp;return 0;<br>}	', 'c= 12	', 'c= 10	', 'c= 2	', 'c= 0	', 3, 18),
	(651, 'int main(){<br>&nbsp;&nbsp;int x=65;<br>&nbsp;&nbsp;const unsigned char c=(int)x;<br>&nbsp;&nbsp;printf("%c\\n",c);<br>&nbsp;&nbsp;return 0;<br>}	', 'Error	', '65	', 'A	', 'Null	', 1, 18),
	(652, 'What will be the output of following program ?<br>void main(){<br>&nbsp;&nbsp;if(!printf(""))<br>&nbsp;&nbsp;&nbsp;&nbsp;printf("Okkk");<br>&nbsp;&nbsp;else<br>&nbsp;&nbsp;&nbsp;&nbsp;printf("Hiii");<br>}	', 'Okkk	', 'Hiii	', 'Error	', 'None	', 1, 18),
	(654, 'What will be the output of following program ?<br>void main()<br>{<br>&nbsp;&nbsp;int a=10;<br>&nbsp;&nbsp;switch(a){<br>&nbsp;&nbsp;&nbsp;&nbsp;case 5+5:<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;printf("Hello\\n");<br>&nbsp;&nbsp;&nbsp;&nbsp;default:<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;printf("OK\\n");<br>&nbsp;&nbsp;}<br>}	', '0 1 2 ... infinite times	', '0 1 2 ... 127	', '0', '1	', 1, 18),
	(655, 'What will be the output of the following C code?\r      #include <stdio.h><br>\r      int main()<br>\r      {<br>\r          &nbsp;int c = 2 ^ 3;\r          <br>&nbsp;printf(""%d\\n"", c);\r      }<br>\r \r ', '1', '8', '9', '0', 1, 18),
	(656, 'What will be the output of the following C code?\r      #include <stdio.h><br>\r      int main()<br>\r      {<br>\r         &nbsp; unsigned int a = 10;\r          <br>&nbsp;a = ~a;\r          <br>&nbsp;printf(""%d\\n"", a);\r      <br>}\r ', '-9', '-10', '-11', '10', 3, 18),
	(657, 'What will be the output of the following C code?\r      #include <stdio.h><br>\r      int main()<br>\r      {<br>\r          &nbsp;if (7 & 8)\r          <br>&nbsp;printf(""Honesty"");\r              <br>&nbsp;if ((~7 & 0x000f) == 8)\r                  <br>&nbsp;printf(""is the best policy\\n"");\r      }\r ', 'Honesty is the best policy', 'Honesty', ' is the best policy', ' No output', 3, 18),
	(946, 'The SQL clause \'UNION\' can be used with', 'SELECT clause only', 'UPDATE clause only', 'Both DELETE and UPDATE clauses', 'SELECT, DELETE and UPDATE clause', 1, 21),
	(947, 'Choose the correct option based on the order of the execution of the following keywords:<br><br>&nbsp;&nbsp;(i)SELECT , <br>&nbsp;&nbsp;(ii)FROM , <br>&nbsp;&nbsp;(iii)WHERE ,<br>&nbsp;&nbsp;(iv) HAVING , <br>&nbsp;&nbsp;(v)GROUP BY ,<br>&nbsp;&nbsp;(vi)ORDER BY', 'i,ii,iii,iv,v,vi', 'i,ii,iii,v,iv,vi', 'ii,iii,v,i,iv,vi', 'ii,iii,v,iv,i,vi', 4, 21),
	(948, 'Which of the following option is correct for the query:<br>&nbsp;&nbsp; To retrive all the employees who joined in the year 1981 ', 'SELECT ENAME FROM EMP WHERE SUBSTR( \'HIREDATE\', -4 , 4) =1981;', 'SELECT * FROM EMP WHERE INSTR( \'HIREDATE\', 1981, 1, 1) > 0;', 'Both OPTION1 and OPTION2', 'None of the above', 3, 21),
	(949, 'Find the name of those cities with temperature and condition whose condition is either sunny or cloudy but temperature must be greater than 70.', 'SELECT city, temperature,condition FROM weather WHERE condition = ‘sunny’ AND condition = ‘cloudy’ OR temperature >70', 'SELECT city, temperature,condition FROM weather WHERE condition= ‘sunny’ OR condition = ‘cloudy’ OR temperature >70', 'SELECT city, temperature, condition FROM weather WHERE condition = ‘sunny’ OR condition = ‘cloudy’ AND temperature > 70', 'SELECT city, temperature, condition FROM weather WHERE condition = ‘sunny’ AND condition = ‘cloudy’ AND temperature > 70', 3, 21),
	(950, 'Aggregate functions can be used in the select list or the_______clause of a select statement or subquery. They cannot be used in a ______ clause.', 'Where, having', 'Having, where', 'Group by, having', 'Group by, where', 2, 21),
	(951, 'The command to remove rows from a table ‘CUSTOMER’ is', ' DROP FROM CUSTOMER', 'UPDATE FROM CUSTOMER', 'REMOVE FROM CUSTOMER', 'DELETE FROM CUSTOMER WHERE', 4, 21),
	(952, 'What type of join is needed when you wish to return rows that do have matching values?', 'Equi-join', 'Natural join', 'Outer join', 'All of the mentioned', 4, 21),
	(953, 'The following SQL is which type of join:<br>&nbsp;&nbsp; SELECT CUSTOMER_T. CUSTOMER_ID, ORDER_T. CUSTOMER_ID, NAME, ORDER_ID FROM CUSTOMER_T,ORDER_T WHERE CUSTOMER_T. CUSTOMER_ID = ORDER_T. CUSTOMER_ID', 'Natural join', 'Equi-join', 'Outer join', 'Cartesian join', 2, 21),
	(954, 'Which of the following is not a foreign key constraint?', 'SET NULL', 'NO ACTION', 'CASCADE', 'All of the mentioned.', 3, 21),
	(955, 'What is the meaning of LIKE ‘%0%0%’ ?', 'Feature begins with two 0’s', 'Feature ends with two 0’s', 'Feature has more than two 0’s', 'Feature has two 0’s in it, at any position', 4, 21),
	(1046, 'What is the output of the bellow code ?<br><br>&nbsp;&nbsp;&nbsp;&nbsp;evertz1 = [1, 2, 3, 4]<br>&nbsp;&nbsp;&nbsp;&nbsp;evertz1.append([5,6,7,8])<br>&nbsp;&nbsp;&nbsp;&nbsp;print len(evertz1)', '5', '8', 'Error', '7', 1, 22),
	(1047, 'Which is true about Python when it comes to memory management?', 'Python memory is managed by Python private heap space.', 'The allocation of Python heap space for Python objects is done by Python memory manager.', 'Python also have an inbuilt garbage collector, which recycle all the unused memory and frees the memory and makes it available to the heap space.', 'All of the above', 4, 22),
	(1048, 'What value we need to give in place of "?" to get the output like<br><br>1<br>2 2<br>3 3 3<br>4 4 4 4<br><br>from __future__ import print_function:<br>for i in range(1, ?): <br>&nbsp;&nbsp;for j in range(i):<br>&nbsp;&nbsp;&nbsp;&nbsp;print(i, end=" ")<br>&nbsp;&nbsp;print()<br>', '5', '4', '6', '7', 1, 22),
	(1049, 'Which is NOT a mutable built-in type?', 'List', 'Tuples', 'Sets', 'Dictionaries', 2, 22),
	(1050, 'What is the output of the following segment :<br> \r      chr(ord("A"))<br>', 'B', 'a', 'A', 'Error', 3, 22),
	(1051, 'What will be the output of the following code :<br> print type(type(int))', 'type \'int\'', 'type \'type\'', 'Error', '0', 2, 22),
	(1052, 'Which of these is true for Flask-WTF?', 'Integration with wtforms', 'Secure form with csrf token', 'Internationalization integration', 'All of the above', 4, 22),
	(1053, 'Suppose list1 is [3, 4, 5, 20, 5, 25, 1, 3]; <br> What is list1 after list1.pop(1) ?<br>', '[3, 4, 5, 20, 5, 25, 1, 3]', '[1, 3, 3, 4, 5, 5, 20, 25]', '[3, 5, 20, 5, 25, 1, 3]  ', '[1, 3, 4, 5, 20, 5, 25]', 3, 22),
	(1054, 'What is the output of the flowing code ?<br><br>def f(x,l=[]):<br>&nbsp;&nbsp;for i in range(x): <br>&nbsp;&nbsp;&nbsp;&nbsp;l.append(i*i)<br>&nbsp;&nbsp;&nbsp;&nbsp;print(l)<br><br>f(2)<br>f(3,[3,2,1])<br>f(3)<br>', '[0, 1][3, 2, 1, 0, 1, 4][0, 1, 0, 1, 4] ', '[0, 1][3, 2, 1, 0, 1, 4][0, 1, 2, 4]', '[0, 1][3, 2, 1, 0, 1, 4][0, 1, 6, 1, 4]', '[0, 1][3, 2, 1, 3, 1, 4][0, 1, 0, 3, 4]', 1, 22),
	(1055, 'Which of the following functions can be used to check if a file "Logo" exists ?', 'os.path.isFile(logo)', 'os.path.isfile(logo) ', 'os.isFile(logo)', 'os.path.exists(logo)', 3, 22),
	(1056, 'What will be the output of the following program ?<br>\n         tuple = (1, 2, 3, 4)<br>\n         tuple.append( (5, 6, 7) )<br>\n         print(len(my_tuple))<br>', '1', '2', '5', 'Error', 4, 22),
	(1149, 'Sohan started a business with a capital of Rs. 80000.After 6 months Mohan joined as a partner by investing Rs. 65000. After one year they earned total profit Rs. 20000. What is share of Sohan in the profit?\n', 'Rs. 5222.2', ' Rs. 5777.7', 'Rs. 6222.2', 'Rs. 6777.7', 2, 20),
	(1150, 'If January 1, 1996, was Monday, what day of the\n week was January 1, 1997?', 'Thursday', 'Wednesday', ' Friday', 'Sunday', 2, 20),
	(1152, 'Two ships are sailing in the sea on the two sides of a lighthouse.The angles of elevation of the top of the lighthouse observed from the ships are 30° and 45° respectively. If the lighthouse is 100m high, find the distance between the two ships.\n', ' 155.80 m', '157.80 m', '  159.80 m', '161.80 m', 2, 20),
	(1154, 'Find the logarithm of 1/256 to the base 2√2.', '16', '(13/5)', '(-16/3)', '12', 3, 20),
	(1155, 'If x<sup>2</sup>+1/x<sup>2</sup>= 34, x+1/x is equal to', '3', '4', '5', 'None of these', 4, 20),
	(1156, 'Complete the series 2, 5, 9, 19, 37.......', '76', '74', '75', 'None of these', 3, 20),
	(1157, 'If 15 men can reap the crops of a field in 28 days, in how many days will 5 men reap it?\n', '50 days', '60 days', '84 days', '9.333 days', 3, 20),
	(1158, 'What is the market price of a 9% share when a person gets 180 by investing Rs. 4000?\n', '150', '200', '250', '300', 2, 20),
	(1159, 'A: B: C is in the ratio of 3: 2: 5. How much money will C get out of Rs 1260?\n', '252', '125', '503', 'None of these', 4, 20),
	(1160, 'A 60 liter mixture of milk and water contains 10% water. How much water must be added to make water 20% in the mixture?\n', '8 liters', '7.5 liters', '3. 7 liters', '4.65 liters', 2, 20),
	(1161, 'Why is IP Protocol is considered unreliable ?', 'A packet may be lost', 'Duplicate packets may be generated\n', 'Packets may arrive out of order', 'All of the above', 4, 14),
	(1162, 'What is the use of subnetting ?', 'It divides one large network into smaller ones', 'It speeds up the speed of the network', 'It divides network into network classes', 'None of the above', 1, 14),
	(1163, 'Which of the following is true regarding access lists applied to an interface ?', 'You can place as many access lists as you want on any interface until you run out of memory.', 'You can apply only one access list on any interface.', 'One access list may be configured, per direction, for each layer 3 protocol configured on an interface.', 'You can apply two access lists to any interface.', 3, 14),
	(1164, 'Which of the following statements are true regarding the command IP route 172.16.4.0 255.255.255.0 192.168.4.2?<br><br>1.&nbsp;The command is used to establish a static route.<br>2.&nbsp;The default administrative distance is used<br>3.&nbsp;The command is used to configure the default route.<br>4.&nbsp;The subnet mask for the source address is 255.255.255.0<br>', '1 and 2', '2 and 4', '3 and 4', 'All of the above', 1, 14),
	(1165, 'What is the maximum number of IP addresses that can be assigned to hosts on a local subnet that uses the 255.255.255.224 subnet mask?', '14', '16', '15', '30', 4, 14),
	(1166, 'Your router has the following IP address on Ethernet 0: 172.16.2.1/23. Which of the following can be valid host IDs on the LAN interface attached to the router ?<br><br>1.&nbsp;172.16.1.100<br>2.&nbsp;172.16.1.198<br>3.&nbsp;172.16.2.255<br>4.&nbsp;172.16.3.0', '1 only', '2 and 3 only', '3 and 4 only', 'None of the above', 3, 14),
	(1167, 'Which of the following services use TCP ?<br><br>1. DHCP<br>2. SMTP<br>3. HTTP<br>4. TFTP<br>5. FTP', '1 and 2', '2, 3 and 5', '1, 2 and 4', '1, 3 and 4', 2, 14),
	(1168, 'What does Router do in a network ?', 'Forwards a packet to all outgoing links', 'Determines on which outgoing link a packet is to be forwarded', 'Forwards a packet to the next free outgoing link', 'Forwards a packet to all outgoing links except the originated link', 2, 14),
	(1169, 'What is the address size of IPv6 ?', '32 bit', '128 bit', '64 bit', '256 bit', 1, 14),
	(1170, 'Which of the following provides reliable communication ?', 'TCP', 'IP', 'UDP', 'All of the above', 1, 14),
	(1171, 'Which of the below is not a valid container ?', 'mov', 'mpeg', 'mp4', 'All of the above', 2, 19),
	(1172, 'Which of the below is a video encoding standard ?', 'H.264', 'HLS', 'MOV', 'None of the above', 1, 19),
	(1173, 'Which compression technique is used in Video and Audio ?', 'Lossy Compression', 'Lossless Compression', 'Both of the above', 'None of the above', 1, 19),
	(1174, 'Which of the below is an audio encoding standard ?', 'AC3', 'AAC', 'PCM', 'All of the above', 4, 19),
	(1175, 'What are the dimensions of Full HD ?', '1920x1080', '3840x2160', '1280x720', 'None of the above', 1, 19),
	(1176, 'How many audio channels a stereo audio have ?', '2 Channels', '4 Channels', '6 Channels', '16 Channels', 1, 19),
	(1177, 'How many audio channels a 5.1 audio have ?', '2 Channels', '4 Channels', '6 Channels', '16 Channels', 3, 19),
	(1178, 'Which of the below is specific for carrying video signals ?', 'Coaxial cable', 'Fibre cable', 'Ethernet cable', 'Aux cable', 1, 19),
	(1179, 'What is the use of subtitles/captions in a video ?', 'To understand the video with the help of the local language', 'For deaf people to understand the video', 'Both of the above', 'None of the above', 3, 19),
	(1180, 'In PAL system weighting of R - Y and B - Y signals is done to', 'Prevent over modulation on saturated colours', 'Prevent under modulation of saturated colours', 'Allow over modulation of saturated colours', 'Allow under modulation on saturated colours', 1, 19),
	(1181, 'C++ language is the combination of these two languages?', 'C and Simula67.', 'C and JAVA.', 'C and FORTRAN.', '3 and 2.', 1, 17),
	(1182, 'What is the full form of OOPS?', 'Object Oriented Programming Software.', 'Object Oriented Programming System.', 'Object Oriented Programming Synchronisation.', 'Object Oriented Program System.', 4, 17),
	(1183, 'What is an Object?', 'A variable name of class.', 'A virtual class name in C++.', 'A normal variable declared in C++ class.', 'An alias of class name.', 1, 17),
	(1184, 'Which header file is required to run this program?<br>#include _______ <br>int main()<br>{<br>&nbsp;&nbsp;cout<<"Hello World.";<br>&nbsp;&nbsp;return 0;<br>}', '<stdio.h>', '<conio.h>', '<iostream.h>', '<ostream.h>', 3, 17),
	(1185, 'class sample<br>{<br>&nbsp;private:<br>&nbsp;&nbsp;&nbsp;&nbsp;int x;<br>&nbsp;public:<br>&nbsp;&nbsp;&nbsp;&nbsp;void sample(){ x=0;  printf("Object created.");<br>&nbsp;&nbsp;&nbsp;&nbsp;void sample(int a){  x=a;  }<br>};<br> <br>int main()<br>{<br>&nbsp;&nbsp;sample s;<br>&nbsp;&nbsp;return 0;<br>}', 'Compile Time Error', 'Object Created.', 'Run Time Error', 'Can’t be predicted', 2, 17),
	(1186, 'What will be the output of the following code?<br><br>class Example {<br>&nbsp;&nbsp;public:<br>&nbsp;&nbsp;&nbsp;&nbsp;Example() {<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; cout << "Constructor called ";<br>&nbsp;&nbsp;&nbsp;&nbsp;}<br>};<br><br>int main()<br>{<br>&nbsp;&nbsp;&nbsp;&nbsp;Example Ex1, Ex2;<br>&nbsp;&nbsp;&nbsp;&nbsp;return 0;<br>}', 'Constructor called', 'Constructor called Constructor called', 'Compile time error', 'Run time error', 2, 17),
	(1187, 'class sample<br>{<br>&nbsp;private:<br>&nbsp;&nbsp;&nbsp; int x,y;<br>};', 'public: void sample(){}', 'public: void sample(){ x=0; y=0;}', 'public: void sample(int a,int b){ x=a; y=b;}', 'Both 1 and 2', 4, 17),
	(1188, 'Constructor(s) which is/are added automatically with a class if we do not create our own constructor?', 'Default Constructor', 'Copy Constructor', 'Copy Constructor', 'None', 3, 17),
	(1189, 'If you created a parameterized and a copy constructor in the class and you create an object with no arguments (0 arguments), what will be the output?', 'Program will execute successfully', 'A compile-time will occur', 'A run-time error will occur', 'A syntax error will occur', 2, 17),
	(1190, 'Write statement to print value of var ?<br>int var=100;<br>class sample<br>{<br>&nbsp;private:<br>&nbsp;void showVal(void)<br>&nbsp;{<br>&nbsp;&nbsp;...<br>&nbsp;}<br>}', 'cout<<var;', 'cout<<::var;', 'Cannot access var inside class member function.', 'Both 1 and 2', 1, 17),
	(1191, 'int main()<br>{<br>&nbsp;&nbsp;&nbsp;&nbsp;int i;<br>&nbsp;&nbsp;&nbsp;&nbsp;for (i = 1; i<=5; i++)<br>&nbsp;&nbsp;&nbsp;&nbsp;{<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;int i;<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;i = 10;<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;printf("%d ", i) ;<br>&nbsp;&nbsp;&nbsp;&nbsp;}<br>&nbsp;&nbsp;&nbsp;&nbsp;return 0;<br>}<br><br>', '10 10 10 10 10 ', 'Error', '10 10 10 10 ', '10', 1, 18);
/*!40000 ALTER TABLE `questions` ENABLE KEYS */;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
