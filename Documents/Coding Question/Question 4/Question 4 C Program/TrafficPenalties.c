#include <stdio.h>
#include <string.h>
#include <stdlib.h>

// Function initialisations
char *calculatePenalties(const char **, const int [], char **, int, int);

void main(int argc, char **argv) {	
	
	const char *violationConditions[] = {"No Helmet", "No Driving Licence", "No Registration Certificate", "Triple Riding", "No Seat Belt", "Drink and Drive", "Minor Driving", "Using cellphone while driving", "Signal jumping", "Wrong way driving"};
	const int violationPenalties[] = {1000, 500, 300, 1000, 1500, 3000, 1000, 500, 300, 1000};
	const int totalTracking = 10;
	
	char delim[] = ",";
	
	char *violationsArray = strtok(argv[1], delim);
	
	char **violationsByIndividual = (char **)malloc(sizeof(char *) * strlen(argv[1]));
	
	int numberOfViolations = 0;
	int length = 0;
	
	while(violationsArray != NULL)
	{
		length = strlen(violationsArray);
		
		violationsByIndividual[numberOfViolations] = (char *)malloc(length * sizeof(char));
		strcpy(violationsByIndividual[numberOfViolations++], violationsArray);
			
		violationsArray = strtok(NULL, delim);
	}
	
	char *finalAmount = calculatePenalties(violationConditions, violationPenalties, violationsByIndividual, numberOfViolations, totalTracking);
	
	printf("%s", finalAmount);
}

// The candidate to complete the code in the below function
char *calculatePenalties(const char **violationConditions, const int violationPenalties[], char **violationsByIndividual, int numberOfViolations, int totalTracking) {
	
	int finalAmount = 0;
	
	for(int i=0; i<numberOfViolations; i++) {
		for(int j=0; j<totalTracking; j++) {
			if(strcmp(violationConditions[j], violationsByIndividual[i]) == 0) {
				finalAmount = finalAmount + violationPenalties[j];
			}
		}
	}
	
	char *finalResult = malloc(50);
	
	itoa(finalAmount, finalResult, 10);
	
	strcat(finalResult, ",");
	
	if(finalAmount >= 2000) {
		return strcat(finalResult, "Vehicle will be seized");
	}
	
	return strcat(finalResult, "Vehicle will not be seized");
}