New Traffic Rules Penalties

According to new traffic rules, the charges for different violations is changed. 
New charges are listed below. 

There will be two cases when vehicle will be seized.
1. If the total amount of fine is more than 2000
2. If there are more than three violations

You need to write a code to calculate total amount fined for a person and determine if licence will be seized or not.

No Helmet - 1000
No Driving Licence - 500
No Registration Certificate - 300
Triple Riding - 1000
No Seat Belt - 1500
Drink and Drive - 3000
Minor Driving - 1000
Using cellphone while driving - 500
Signal jumping - 300
Wrong way driving - 1000

The above data is already stored in two arrays in the code snippet.

Sample Testcase 1:

Inputs:

No Helmet,No Driving Licence,No Registration Certificate

Outputs:

1800,Vehicle will not be seized


Sample Testcase 2:

Inputs:

Minor Driving,Drink and Drive

Outputs:

4000,Vehicle will be seized

--------------------------------------------------------------------------------------------------------------

Base Class Name: TrafficPenalties