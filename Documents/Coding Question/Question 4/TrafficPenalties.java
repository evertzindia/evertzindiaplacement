import java.util.Arrays;

public class TrafficPenalties {

	// Array that stores the Traffic Violations and their fines
	// Use these two arrays to calculate the total fines
	// Do not change any values in the below 5 variables
	static String[] violationConditions = {"No Helmet", "No Driving Licence", "No Registration Certificate", "Triple Riding", "No Seat Belt", "Drink and Drive", "Minor Driving", "Using cellphone while driving", "Signal jumping", "Wrong way driving"};
	static int[] violationPenalties = {1000, 500, 300, 1000, 1500, 3000, 1000, 500, 300, 1000};
	static int numberOfViolationConditionsAndFines = 10;
	static int bikeLicenseSiezeThreshold = 2000;
	static int carLicenseSiezeThreshold = 4000;
	
	public static void main(String[] args) {

		String[] argumentListArray = args[0].split(":");
		
		// Getting the data from command line arguments
		String typeOfVehicle = argumentListArray[0];
		String[] trafficViolationsArrayByVehicle = argumentListArray[1].split(",");

		String calculatedPenaltyAndDecision = calculateTotalPenalty(typeOfVehicle, trafficViolationsArrayByVehicle);

		System.out.println(calculatedPenaltyAndDecision);

	}

	// This function to be populated by the candidate
	public static String calculateTotalPenalty(String typeOfVehicle, String[] trafficViolationsArrayByVehicle) {

		int numberOfViolations = trafficViolationsArrayByVehicle.length;
		int totalAmountOfFines = 0;
		
		for(int i=0; i<numberOfViolations; i++) {
			
			for(int j=0; j<numberOfViolationConditionsAndFines; j++) {
				
				if(trafficViolationsArrayByVehicle[i].equals(violationConditions[j])) {
					totalAmountOfFines = totalAmountOfFines + violationPenalties[j];
				}
			}
		}
		
		if(numberOfViolations > 3) {
			return Integer.toString(totalAmountOfFines) + ",Vehicle will be seized";
		}
		
		if(typeOfVehicle.equals("Bike") && totalAmountOfFines>=bikeLicenseSiezeThreshold) {
			return Integer.toString(totalAmountOfFines) + ",Vehicle will be seized";
		}
		else if(typeOfVehicle.equals("Bike") && totalAmountOfFines<bikeLicenseSiezeThreshold) {
			return Integer.toString(totalAmountOfFines) + ",Vehicle will not be seized";
		}
		else if(typeOfVehicle.equals("Car") && totalAmountOfFines>=carLicenseSiezeThreshold) {
			return Integer.toString(totalAmountOfFines) + ",Vehicle will be seized";
		}
		else if(typeOfVehicle.equals("Car") && totalAmountOfFines<carLicenseSiezeThreshold) {
			return Integer.toString(totalAmountOfFines) + ",Vehicle will not be seized";
		}

		return null;

	}

}