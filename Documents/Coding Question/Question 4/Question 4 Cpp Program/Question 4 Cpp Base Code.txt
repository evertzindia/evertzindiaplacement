class TrafficPenalties {
	
	public:
	
	// This function to be populated by the candidate
	char *calculatePenalties(const char **violationConditions, const int violationPenalties[], char **violationsByIndividual, int numberOfViolations, int totalTracking) {
		
		return "Null";
	}
};

int main(int argc, char **argv) {	

	const char *violationConditions[] = {"No Helmet", "No Driving Licence", "No Registration Certificate", "Triple Riding", "No Seat Belt", "Drink and Drive", "Minor Driving", "Using cellphone while driving", "Signal jumping", "Wrong way driving"};
	const int violationPenalties[] = {1000, 500, 300, 1000, 1500, 3000, 1000, 500, 300, 1000};
	const int totalTracking = 10;

	char delim[] = ",";
	
	char *violationsArray = strtok(argv[1], delim);
	
	char **violationsByIndividual = (char **)malloc(sizeof(char *) * strlen(argv[1]));
	
	int numberOfViolations = 0;
	int length = 0;
	
	while(violationsArray != NULL)
	{
		length = strlen(violationsArray);
		
		violationsByIndividual[numberOfViolations] = (char *)malloc(length * sizeof(char));
		strcpy(violationsByIndividual[numberOfViolations++], violationsArray);
			
		violationsArray = strtok(NULL, delim);
	}
	
	
	class TrafficPenalties tp;
	
	char *finalAmount = tp.calculatePenalties(violationConditions, violationPenalties, violationsByIndividual, numberOfViolations, totalTracking);
	
	cout<<finalAmount;
	
	return 0;
}