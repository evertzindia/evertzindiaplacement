import sys;

violationConditions = ["No Helmet", "No Driving Licence", "No Registration Certificate", "Triple Riding", "No Seat Belt", "Drink and Drive", "Minor Driving", "Using cellphone while driving", "Signal jumping", "Wrong way driving"]
violationPenalties = [1000, 500, 300, 1000, 1500, 3000, 1000, 500, 300, 1000]
numberOfViolationConditionsAndFines = 10
bikeLicenseSiezeThreshold = 2000
carLicenseSiezeThreshold = 4000

# This function to be populated by the candidate
def calculateTotalPenalty(typeOfVehicle, trafficViolationsArrayByVehicle):

    numberOfViolations = len(trafficViolationsArrayByVehicle);    
    totalAmountOfFines = 0;

    for i in range(numberOfViolations):
        for j in range(numberOfViolationConditionsAndFines) :
            if trafficViolationsArrayByVehicle[i] == violationConditions[j] :
                totalAmountOfFines = totalAmountOfFines + violationPenalties[j];
	
    if(numberOfViolations > 3):
        return str(totalAmountOfFines) + ",Vehicle will be seized";
	
    if ((typeOfVehicle == "Bike") and (totalAmountOfFines >= bikeLicenseSiezeThreshold)) :
        return str(totalAmountOfFines) + ",Vehicle will be seized";

    elif((typeOfVehicle == "Bike") and (totalAmountOfFines <  bikeLicenseSiezeThreshold)):
        return str(totalAmountOfFines) + ",Vehicle will not be seized";

    elif((typeOfVehicle == "Car") and (totalAmountOfFines >= carLicenseSiezeThreshold)):
        return str(totalAmountOfFines) + ",Vehicle will be seized";

    elif((typeOfVehicle == "Car") and (totalAmountOfFines < carLicenseSiezeThreshold)):
        return str(totalAmountOfFines) + ",Vehicle will not be seized";
  
    return "None";

# Getting the data from command line arguments    

argumentListArray = sys.argv[1].split(":");
typeOfVehicle = argumentListArray[0];
trafficViolationsArrayByVehicle = argumentListArray[1].split(",");
calculatedPenaltyAndDecision = calculateTotalPenalty(typeOfVehicle, trafficViolationsArrayByVehicle);

print(calculatedPenaltyAndDecision);