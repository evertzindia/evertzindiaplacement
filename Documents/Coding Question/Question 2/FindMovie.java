import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.stream.Collectors;

public class FindMovie {

	public static void main(String[] argsv) {

		// Getting the data from command line arguments
		String movieToFind = argsv[0];

		String adamMovieList = null;

		// Reading the file and storing the contents in a String variable
		// The UtilityFunction class has all the required functions
		// If you see the text as "Please contact Supervisor" in the Run output. Please
		// do contact Supervisor
		try {
			adamMovieList = readAdamFileContentsAndReturnString();
		} catch (Exception e) {
			System.out.println("Error: " + e.getMessage() + " - Please contact Supervisor");
			System.exit(1);
		}

		String resultFromFunction = calculateCountOfTrees(adamMovieList, movieToFind);

		System.out.println(resultFromFunction);

	}

	// This function to be populated by the candidate
	public static String calculateCountOfTrees(String adamMovieList, String movieToFind) {

		char[] adamMovieListInCharArray = adamMovieList.toCharArray();
		int adamMovieListCharLength = adamMovieListInCharArray.length;

		int movieToFindCharLength = movieToFind.length();

		int movieCount = 0;
		boolean existsFlag = false;

		for (int i = 0; i < (adamMovieListCharLength - movieToFindCharLength); i++) {

			String chunkFromAdamMovieList = adamMovieList.substring(i, (i + movieToFindCharLength));
			
			if (chunkFromAdamMovieList.equals(movieToFind)) {
				existsFlag = true;
				++movieCount;
			}

		}

		if (existsFlag) {
			return "Exists," + movieCount;
		} else {
			return "Does not exist,0";
		}
	}

	@SuppressWarnings("resource")
	public static String readAdamFileContentsAndReturnString() throws Exception {

		String adamMovieList = null;
		String adamMovieTextFileLocation = "C:\\\\EvertzInterviewApplication\\\\CandidatePrograms\\\\Adam_Movie_Database.txt";
		File file = new File(adamMovieTextFileLocation);

		BufferedReader br;

		// Protect file read using try/catch
		try {
			br = new BufferedReader(new FileReader(file));
			adamMovieList = br.lines().collect(Collectors.joining());

		} catch (FileNotFoundException e) {
			e.printStackTrace();
			throw new Exception(e.getMessage());
		}

		return adamMovieList;
	}

}