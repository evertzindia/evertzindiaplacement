import sys;
import numpy as np;

# The candidate has to populate this function
def findMovie(adamMovieList, movieToFind) :
    
    adamMovieListCharLength = len(adamMovieList);
    movieToFindCharLength = len(movieToFind);
	
    movieCount = 0;
    
    existsFlag = False;
	
    for i in range(adamMovieListCharLength - movieToFindCharLength):
        
        ob_slice = slice(i, i+movieToFindCharLength, 1);
        
        chunkFromAdamMovieList = adamMovieList[ob_slice];
		
        if chunkFromAdamMovieList == movieToFind: 
            existsFlag = True;
            movieCount = movieCount + 1;
	
    if (existsFlag): 
        return "Exists," + str(movieCount);
    else :
        return "Does not exist,0";
            
# This function to read the contents and return as a string
def readAdamFileContentsAndReturnString():

    adamMovieList = "";
    adamMovieTextFileLocation = "C:\\EvertzInterviewApplication\\CandidatePrograms\\Adam_Movie_Database.txt";
	
    file = open(adamMovieTextFileLocation, "r");
	
    try :
        data = file.read();
        for each in data :
            adamMovieList = adamMovieList + ",".join(each);
    except Exception as e :
        e.printStackTrace();
        raise Exception(e);
	
    return adamMovieList;
		
# Getting command line arguments
movieToFind = sys.argv[1];

# Return from function
resultFromFunction = calculateCountOfTrees(readAdamFileContentsAndReturnString(), movieToFind);

print(resultFromFunction);