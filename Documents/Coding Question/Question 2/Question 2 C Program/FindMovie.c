// Function initialisations
void readAdamFileContentsAndReturnString();

char *findMovie(char *);

char *buffer;

void main(int argc, char **argv) {	

	int movieToFindLength = strlen(argv[1]);
	
	readAdamFileContentsAndReturnString();
	
	char *result = findMovie(argv[1]);
	
	printf("%s", result);
	
	free(buffer);
}

// The candidate to complete the code in the below function
char *findMovie(char *movieToFind) {
	
	char *ptr = strstr(buffer, movieToFind);
	
	char *result = malloc(15);
	
	if (ptr != NULL)
	{
		strcpy(result, "Exists");
	}
	else
	{
		strcpy(result, "Does not exist");
	}
	
	return result;
	
}

// Read file and store in global variable
void readAdamFileContentsAndReturnString(char *dataFromTextFile) {
	
	char *filename = "C:\\\\EvertzInterviewApplication\\\\CandidatePrograms\\\\Adam_Movie_Database.txt";
	
	FILE *fp;
	long lSize;
	char *data;

	fp = fopen ( filename , "rb" );
	if( !fp ) perror(filename),exit(1);

	fseek( fp , 0L , SEEK_END);
	lSize = ftell( fp );
	rewind( fp );

	/* allocate memory for entire content */
	buffer = calloc( 1, lSize+1 );
	if( !buffer ) fclose(fp),fputs("memory alloc fails",stderr),exit(1);

	/* copy the file into the buffer */
	if( 1!=fread( buffer , lSize, 1 , fp) )
	  fclose(fp),free(buffer),fputs("entire read fails",stderr),exit(1);
	
	fclose(fp);
}