import sys;

# This function needs to be populated by the candidate
def calculateCountOfTrees(h, f):

    numberOfTrees = len(h);
    farmersPositionTreeHeight = int(h[f]);
    shortCountOnLeft = 0;
    shortCountOnRight = 0;
    tallCountOnLeft = 0;
    tallCountOnRight = 0;
    dummy = 0;
	
    for i in range(f):
        if (farmersPositionTreeHeight < h[i]):
            tallCountOnLeft += 1;
        elif (farmersPositionTreeHeight == h[i]):
            dummy = 1;
        else:
            shortCountOnLeft += 1;
			
    for i in range(f + 1, numberOfTrees):
        if (farmersPositionTreeHeight < h[i]):
            tallCountOnRight += 1;
        elif (farmersPositionTreeHeight == h[i]):
            dummy = 1;
        else :
            shortCountOnRight += 1;
			
    return str(shortCountOnLeft) + " " + str(tallCountOnLeft) + "," + str(shortCountOnRight) + " " + str(tallCountOnRight);

# Get data from command line argument
treeHeightsDataString = sys.argv[1].split(" ")[0];
farmerPositionString = sys.argv[1].split(" ")[1];
treeHeightsStringArray = treeHeightsDataString.split(",");
numberOfTrees = len(treeHeightsStringArray);

# Initialise empty array
h = [];
f = int(farmerPositionString);

# Populate array from command line argument string
for i in range(numberOfTrees):
	h.append(int(treeHeightsStringArray[i]));

# Call the function to do the work
calculatedCountOfTrees = calculateCountOfTrees(h, f);

print(calculatedCountOfTrees);