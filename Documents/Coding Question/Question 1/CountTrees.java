public class CountTrees {

	public static void main(String[] argsv) {

		//Getting the data from command line arguments
		String treeHeightsDataString = argsv[0].split(" ")[0];
		String farmerPositionString = argsv[0].split(" ")[1];

		String[] treeHeightsStringArray = treeHeightsDataString.split(",");
		int numberOfTrees = treeHeightsStringArray.length;
		int h[] = new int[numberOfTrees];
		int f = Integer.parseInt(farmerPositionString);

		//Converting the String data to Integer
		for (int i = 0; i < numberOfTrees; i++) {
			h[i] = Integer.parseInt(treeHeightsStringArray[i]);
		}

		String calculatedCountOfTrees = calculateCountOfTrees(h, f);

		System.out.println(calculatedCountOfTrees);

	}

	//This function to be populated by the candidate
	public static String calculateCountOfTrees(int[] h, int f) {

		int numberOfTrees = h.length;
		int farmersPositionTreeHeight = h[f];
		//System.out.println("Farmers Position Tree Height: " + farmersPositionTreeHeight);
		int shortCountOnLeft = 0;
		int shortCountOnRight = 0;
		int tallCountOnLeft = 0;
		int tallCountOnRight = 0;

		for (int i = 0; i < f; i++) {

			if (farmersPositionTreeHeight < h[i]) {
				++tallCountOnLeft;
				//System.out.println("shortCountOnLeft: " + h[i]);
			}
			else if(farmersPositionTreeHeight == h[i]){}
			else {
				++shortCountOnLeft;
				//System.out.println("tallCountOnLeft: " + h[i]);
			}
		}

		for (int i = (f + 1); i < numberOfTrees; i++) {

			if (farmersPositionTreeHeight < h[i]) {
				++tallCountOnRight;
				//System.out.println("shortCountOnRight: " + h[i]);
			}
			else if (farmersPositionTreeHeight == h[i]) {}
			else {
				++shortCountOnRight;
				//System.out.println("tallCountOnRight: " + h[i]);
			}
		}

		return Integer.toString(shortCountOnLeft) + " " + Integer.toString(tallCountOnLeft) + ","
				+ Integer.toString(shortCountOnRight) + " " + Integer.toString(tallCountOnRight);

	}

}
