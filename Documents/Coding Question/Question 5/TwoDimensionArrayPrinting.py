
import sys;

# This function to be populated by the candidate

def  printMatrix(arrayOfIntegers, matrixNumberOfRows, matrixNumberOfColumns) :
    
    n= 0;
	
    finalPrintStatement = "";
	
    for j in range(matrixNumberOfRows) :
        if ((j == 0) or  (j % 2 == 0)) : 
            for i in range(matrixNumberOfColumns):
                if (i == (matrixNumberOfColumns - 1)) :
                    finalPrintStatement = finalPrintStatement + str(str(arrayOfIntegers[n]) + "\n")
                    n+=1;
                else :
                    finalPrintStatement = finalPrintStatement + str(str(arrayOfIntegers[n]) + " ")
                    n+=1;
        else :
            c = n + matrixNumberOfColumns - 1;
            for i in range(matrixNumberOfColumns):
                if (i == (matrixNumberOfColumns - 1)) :
                    finalPrintStatement = finalPrintStatement + str(str(arrayOfIntegers[c]) + "\n")
                    c-=1;
                else :
                    finalPrintStatement = finalPrintStatement + str(str(arrayOfIntegers[c]) + " ")
                    c-=1;
                n+=1;
	
    print(finalPrintStatement);

# Getting data from Command line arguments
arrayOfIntegersAsString = sys.argv[1].split(" ")[0].split(",");
matrixStructure = sys.argv[1].split(" ")[1];
matrixNumberOfRows = int(matrixStructure.split("x")[0]);
matrixNumberOfColumns = int(matrixStructure.split("x")[1]);

lengthOfArray = len(arrayOfIntegersAsString);
arrayOfIntegers=[];

for i in range(lengthOfArray) :
	arrayOfIntegers.append(arrayOfIntegersAsString[i]);
	
printMatrix(arrayOfIntegers, matrixNumberOfRows, matrixNumberOfColumns);