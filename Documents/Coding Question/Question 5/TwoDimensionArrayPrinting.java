public class TwoDimensionArrayPrinting {

	public static void main(String[] argsv) {

		String[] arrayOfIntegersAsString = argsv[0].split(" ")[0].split(",");
		String matrixStructure = argsv[0].split(" ")[1];

		int matrixNumberOfRows = Integer.parseInt(matrixStructure.split("x")[0]);
		int matrixNumberOfColumns = Integer.parseInt(matrixStructure.split("x")[1]);

		int lengthOfArray = arrayOfIntegersAsString.length;
		int[] arrayOfIntegers = new int[lengthOfArray];

		// Converting the String data to Integer
		for (int i = 0; i < lengthOfArray; i++) {
			arrayOfIntegers[i] = Integer.parseInt(arrayOfIntegersAsString[i]);
		}

		printMatrix(arrayOfIntegers, matrixNumberOfRows, matrixNumberOfColumns);

	}

	// This function to be populated by the candidate
	public static void printMatrix(int[] arrayOfIntegers, int matrixNumberOfRows, int matrixNumberOfColumns) {

		int n = 0;

		for (int j = 0; j < matrixNumberOfRows; j++) {

			if (j == 0 || j % 2 == 0) {
				for (int i = 0; i < matrixNumberOfColumns; i++) {

					if (i == (matrixNumberOfColumns - 1)) {
						System.out.println(arrayOfIntegers[n++]);
					} else {
						System.out.print(arrayOfIntegers[n++] + " ");
					}

				}
			} else {
				int c = n + matrixNumberOfColumns - 1;

				for (int i = 0; i < matrixNumberOfColumns; i++) {

					if (i == (matrixNumberOfColumns - 1)) {
						System.out.println(arrayOfIntegers[c--]);
					} else {
						System.out.print(arrayOfIntegers[c--] + " ");
					}
					++n;
				}
			}
		}

	}
}