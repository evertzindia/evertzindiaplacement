#include <stdio.h>
#include <string.h>
#include <stdlib.h>

void replacedString(char *);

void main(int argc, char **argv) {	

	char *stringToReplace = argv[1];
	
	replacedString(stringToReplace);
	
	printf("%s", stringToReplace);
}

// The candidate to complete the code in the below function
void replacedString(char *stringToReplace) {
	
	int stringLength = strlen(stringToReplace);
	
	for(int i=0; i<stringLength; i++) {
		if(stringToReplace[i] == 'i') {
			stringToReplace[i] = 'j';
		}
	}
	
}