#include <iostream>
#include <stdlib.h>
#include <stdbool.h>
#include <string.h>

using namespace std;

void replacedString(char *);

int main(int argc, char **argv) {	

	char *stringToReplace = argv[1];
	
	replacedString(stringToReplace);
	
	printf("%s", stringToReplace);
	
	return 0;
}

// The candidate to complete the code in the below function
void replacedString(char *stringToReplace) {
	
	int stringLength = strlen(stringToReplace);
	
	for(int i=0; i<stringLength; i++) {
		if(stringToReplace[i] == 'i') {
			stringToReplace[i] = 'j';
		}
	}
	
}