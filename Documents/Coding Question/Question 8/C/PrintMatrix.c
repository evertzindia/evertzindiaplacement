#include <stdio.h>
#include <string.h>
#include <stdlib.h>

void printMatrix(char *);

int main(int argc, char **argv) {	

	char *inputString = argv[1];
	
	printMatrix(inputString);
	
	return 0;
}

// The candidate to complete the code in the below function
void printMatrix(char *inputString) {
	
	int stringLength = strlen(inputString);
	char c;
	
	for(int i=0; i<stringLength-1; i=i+2) {
		
		c = inputString[i+1];
		inputString[i+1] = inputString[i];
		inputString[i] = c;
	}
	
}