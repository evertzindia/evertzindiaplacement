import sys;
import numpy as np;

# To be populated by the candidate
def calculateReachingDuration(a, t, numberOfStops) :
		
    return 0;

# Reading command line arguments and handling the array split
averageTimeStringArray  = sys.argv[1].split(" ")[0].split(",");
averageTrafficStringArray  = sys.argv[1].split(" ")[1].split(",");

averageTimeStringArrayLength = len(averageTimeStringArray);
averageTrafficStringArrayLength  = len(averageTrafficStringArray);

if (averageTimeStringArrayLength != averageTrafficStringArrayLength):
	print("Time and Traffic Array Length doesnt match");
	sys.exit(1);

a =[]
t =[]

list(np.float_(a));
list(np.float_(t));

for i in range(averageTimeStringArrayLength) :
	a.append(averageTimeStringArray[i]);
	t.append(averageTrafficStringArray[i]);
	

calculatedReachingDuration = float(calculateReachingDuration(a, t, averageTimeStringArrayLength));

print(calculatedReachingDuration);