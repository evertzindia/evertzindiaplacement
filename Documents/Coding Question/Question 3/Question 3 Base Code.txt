public class BangaloreTraffic {

	public static void main(String[] argsv) {

		// Getting the data from command line arguments
		String[] averageTimeStringArray = argsv[0].split(" ")[0].split(",");
		String[] averageTrafficStringArray = argsv[0].split(" ")[1].split(",");

		int averageTimeStringArrayLength = averageTimeStringArray.length;
		int averageTrafficStringArrayLength = averageTrafficStringArray.length;

		if (averageTimeStringArrayLength != averageTrafficStringArrayLength) {
			System.out.println("Time and Traffic Array Length doesnt match");
			System.exit(1);
		}

		int[] a = new int[averageTimeStringArrayLength];
		int[] t = new int[averageTrafficStringArrayLength];

		// Converting the String data to Integer
		for (int i = 0; i < averageTimeStringArrayLength; i++) {
			a[i] = Integer.parseInt(averageTimeStringArray[i]);
			t[i] = Integer.parseInt(averageTrafficStringArray[i]);
		}

		float calculatedReachingDuration = calculateReachingDuration(a, t, averageTimeStringArrayLength);

		System.out.println(calculatedReachingDuration);

	}

	// This function to be populated by the candidate
	public static float calculateReachingDuration(int[] a, int[] t, int numberOfStops) {

		return 0;

	}

}