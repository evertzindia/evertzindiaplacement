import sys;
import numpy as np;

# To be populated by the candidate
def calculateReachingDuration(a, t, numberOfStops) :

    percent = float(0);
    reachingDuration = float(0);
    
    for i in range(numberOfStops):
        
        percent = float(float (a[i]) / float(100) * float(t[i]));
        reachingDuration = float((float(reachingDuration) +  float(a[i]) + float(percent)));
        percent = 0;
		
    return reachingDuration;

# Reading command line arguments and handling the array split
averageTimeStringArray  = sys.argv[1].split(" ")[0].split(",");
averageTrafficStringArray  = sys.argv[1].split(" ")[1].split(",");

averageTimeStringArrayLength = len(averageTimeStringArray);
averageTrafficStringArrayLength  = len(averageTrafficStringArray);

if (averageTimeStringArrayLength != averageTrafficStringArrayLength):
	print("Time and Traffic Array Length doesnt match");
	sys.exit(1);

a =[]
t =[]

list(np.float_(a));
list(np.float_(t));

for i in range(averageTimeStringArrayLength) :
	a.append(averageTimeStringArray[i]);
	t.append(averageTrafficStringArray[i]);
	

calculatedReachingDuration = float(calculateReachingDuration(a, t, averageTimeStringArrayLength));

print(calculatedReachingDuration);
