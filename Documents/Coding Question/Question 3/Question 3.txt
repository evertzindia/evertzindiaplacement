Bangalore Traffic :(

Due to increase of traffic in Bangalore, Government is planning to design an application which will determines the duration taken by a bus to reach the destination.
The government is using Google map API to get the traffic increased percentage in a particular route.

You need to write a program that calculates the duration taken by the bus to reach its destination based on the average time and increased traffic percentages.

Sample Testcase 1 - Silk Board to Hebbal

Bus Route - 500D that starts from Silk Board and reaches Hebbal.

Average time taken between each stops (a)
Silk Board ----------> Marathalli -----------> Tin Factory -----------> Hebbal
				30					   20						15

Traffic increased percentages provided by Google API (t)
Silk Board ----------> Marathalli -----------> Tin Factory -----------> Hebbal
				+15					   -5%						+10%

Inputs:

a = (30,20,15)
t = (15,-5,10)

30,20,15 -> Array of time taken by bus between two stops.
+15,-5,+10 -> Array of traffic increased percentages between each stops.

a>1<1000 - 	Array of numbers between 0 and 999 (Maximum size of array is 1000)
			Each value in the array is also between 0 and 999
			
-1000>t<1000 - 	Integer value between 0 and 999

Length of t always equal to length of a

Output:

70.0

70.0 -> Time taken by the bus to reach the destination

Sample Testcase 2 - KBS to Electronics City

Bus Route - 335 that starts from Majestic and reaches Whitefield.

Average time taken between each stops (a)
Majestic ----------> MG Road -----------> Domlur -----------> Marathalli -----------> Whitefield
			10					   20				15						  15

Traffic increased percentages provided by Google API (t)
Majestic ----------> MG Road -----------> Domlur -----------> Marathalli -----------> Whitefield
			+10					   -10				25						  30

Inputs:

a = (10,20,15,15)
t = (10,-10,25,30)

10,20,15,15 -> Array of time taken by bus between two stops.
10,-10,25,30 -> Array of traffic increased percentages between each stops.

a>1<1000 - 	Array of numbers between 0 and 999 (Maximum size of array is 1000)
			Each value in the array is also between 0 and 999
			
-1000>t<1000 - 	Integer value between 0 and 999

Length of t always equal to length of a

Output:
67.25

67.25 -> Time taken by the bus to reach the destination

--------------------------------------------------------------------------

Base Class Name: BangaloreTraffic