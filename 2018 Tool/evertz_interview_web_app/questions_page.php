<?php
	
	session_start();
	
	require_once 'dbconnect.php';
	
	function writeToErrorFile($error) {
		$logFile = "supportingfiles/logFile.log";
		$handle = fopen($logFile, 'w') or die('Cannot open file: ' . $logFile);
		fwrite($handle, $error);
		fclose($handle);
		return True;
	}
	
	function saveAnswerToDatabase($regNo, $questionIdToBeQueried, $choosenAnswer, $moduleIdToBeQueried) {
		
		mysql_query("UPDATE STUDENT_QUESTIONS SET TAKEN = 1, ANSWER_CHOOSEN = '$choosenAnswer' WHERE REGISTER_NUMBER = '$regNo' AND MODULE_ID = '$moduleIdToBeQueried' AND  QUESTION_ID = '$questionIdToBeQueried'");
		
		return True;
	}
	
	$errorCode = array("Please choose an answer to continue", "Invalid credentials");
	
	$encodedRegNo = $_GET['regno'];
	$regNo = base64_decode($encodedRegNo);
	$quesNo = $_GET['quesno'];
	$errorMessage = $_GET['errorcode'];
	
	$timeFromDBQuery = mysql_query("SELECT TIMER_VALUE FROM STUDENT_SESSION WHERE REGISTER_NUMBER = '$regNo'");
	$timeFromDBArray = mysql_fetch_array($timeFromDBQuery);
	$timeFromDB = $timeFromDBArray[0];
	
	$timerToBeSet = $timeFromDB;
	
	if(empty($errorMessage)) {
		$errMSG = $errorCode[$errorMessage];
	}
	
	$programmingModuleQuery = mysql_query("SELECT DISTINCT(MODULE_ID) FROM STUDENT_QUESTIONS WHERE REGISTER_NUMBER = '$regNo' AND MODULE_ID LIKE 'P%'");
	$programmingModuleQueryArray = mysql_fetch_array($programmingModuleQuery);
	$programmingModuleChosen = $programmingModuleQueryArray[0];
	
	$modulesForRegisteredUser = mysql_query("SELECT DISTINCT(MODULE_ID), TEMP_ORDER_ID FROM STUDENT_QUESTIONS WHERE REGISTER_NUMBER = '$regNo' ORDER BY TEMP_ORDER_ID");
	$moduleAndQuestionsMapping = array();
	
	while($row = mysql_fetch_assoc($modulesForRegisteredUser)) {
		$tempModuleId = $row['MODULE_ID'];
		$eachModuleTotalQuestions = mysql_query("SELECT COUNT(QUESTION_ID) AS NO_OF_QUESTIONS FROM STUDENT_QUESTIONS WHERE REGISTER_NUMBER = '$regNo' AND MODULE_ID = '$tempModuleId'");
		$numberOfQuestionsArray = mysql_fetch_array($eachModuleTotalQuestions);
		$numberOfQuestions = $numberOfQuestionsArray[0];
		
		$moduleAndQuestionsMapping[$row['MODULE_ID']] = $numberOfQuestions;
	}
	
	$moduleNameArray = array();
	$moduleIdArray = array_keys($moduleAndQuestionsMapping);	
	$numberOfModules = count($moduleIdArray);
	for($x=0; $x<$numberOfModules; $x++) {
		$tempModuleId = $moduleIdArray[$x];
		$tempModuleNameArray = mysql_query("SELECT MODULE_DESCRIPTION FROM MODULE_MAPPING WHERE MODULE_ID = '$tempModuleId'");
		$tempModuleName = mysql_fetch_array($tempModuleNameArray);
		$moduleNameArray[$x] = $tempModuleName[0];
	}
	
	$buttonArrangement = "<br>";
	$questionNumber = 1;
	for ($y=0; $y<$numberOfModules; $y++) {
		$eachModuleCount = $moduleAndQuestionsMapping[$moduleIdArray[$y]];
		$buttonArrangement = $buttonArrangement . "<u>" . $moduleNameArray[$y] . "</u><br><br>";
		for($z=1; $z<=$eachModuleCount; $z++) {
			$isQuestionTakenQuery = mysql_query("SELECT TAKEN FROM STUDENT_QUESTIONS WHERE REGISTER_NUMBER = '$regNo' AND TEMP_ID = '$questionNumber'");
			$isQuestionTakenArray = mysql_fetch_array($isQuestionTakenQuery);
			$isQuestionTaken = $isQuestionTakenArray[0];
			
			if($questionNumber == $quesNo) {
				$buttonArrangement = $buttonArrangement . '<button type="submit" class="inProgressButton" name="question" value="' . $regNo . "|" . $questionNumber . '">' . $questionNumber . '</button>';
				$currentQuestionTaken = $isQuestionTaken;
			}
			else if($isQuestionTaken == 0) {
				$buttonArrangement = $buttonArrangement . '<button type="submit" class="notTakenButton" name="question" value="' . $regNo . "|" . $questionNumber . '">' . $questionNumber . '</button>';
			}
			else if($isQuestionTaken == 1) {
				$buttonArrangement = $buttonArrangement . '<button type="submit" class="takenButton" name="question" value="' . $regNo . "|" . $questionNumber . '">' . $questionNumber . '</button>';
			}
			$questionNumber = $questionNumber + 1;
			if($z == $eachModuleCount) {
				$buttonArrangement = $buttonArrangement . "<br>";
			}
		}
		$buttonArrangement = $buttonArrangement . "<br>";
	}
	
	$navigationPanelDesign = $buttonArrangement;
	
	if( isset($_GET['question']) ) {
		
		$valueString = $_GET['question'];
		$regNo = explode("|", $valueString)[0];
		$buttonNumber = explode("|", $valueString)[1];
		$encodedRegNo = base64_encode($regNo);

		header("Location: questions_page.php?regno=$encodedRegNo&quesno=$buttonNumber");
	}
	
	$questionBlock = "";
	
	$questionMapModuleQuery = mysql_query("SELECT MODULE_ID FROM STUDENT_QUESTIONS WHERE TEMP_ID = '$quesNo' AND REGISTER_NUMBER = '$regNo'");
	$questionMapModuleArray = mysql_fetch_array($questionMapModuleQuery);
	$moduleIdToBeQueried = $questionMapModuleArray[0];
	
	$questionMapIDQuery = mysql_query("SELECT QUESTION_ID FROM STUDENT_QUESTIONS WHERE TEMP_ID = '$quesNo' AND REGISTER_NUMBER = '$regNo'");
	$questionMapIDArray = mysql_fetch_array($questionMapIDQuery);
	$questionIdToBeQueried = $questionMapIDArray[0];
	
	$questionToBePlacedQuery = mysql_query("SELECT QUESTION, OPTION1, OPTION2, OPTION3, OPTION4 FROM QUESTIONS WHERE MODULE_ID = '$moduleIdToBeQueried' AND ID_TEMP = '$questionIdToBeQueried'");
	$questionToBePlacedArray = mysql_fetch_array($questionToBePlacedQuery);
	
	$testAlreadyTakenFlagQuery = mysql_query("SELECT TEST_ALREADY_TAKEN FROM STUDENT_DETAILS WHERE REGISTER_NUMBER = '$regNo'");
	$testAlreadyTakenFlagArray = mysql_fetch_array($testAlreadyTakenFlagQuery);
	$testAlreadyTakenFlag = $testAlreadyTakenFlagArray[0];
	
	if($currentQuestionTaken == 1) {
		$answerChoosenQuery = mysql_query("SELECT ANSWER_CHOOSEN FROM STUDENT_QUESTIONS WHERE REGISTER_NUMBER = '$regNo' AND TEMP_ID = '$quesNo'");
		$answerChoosenArray = mysql_fetch_array($answerChoosenQuery);
		$answerChoosen = $answerChoosenArray[0];
	}
	
	$questionText = $questionToBePlacedArray[0];
	$option1 = $questionToBePlacedArray[1];
	$option2 = $questionToBePlacedArray[2];
	$option3 = $questionToBePlacedArray[3];
	$option4 = $questionToBePlacedArray[4];
	
	if($testAlreadyTakenFlag == 0) {
		if($answerChoosen == 1) {
			$questionBlock = '<form>' . $quesNo . ") <b>" . $questionText . "</b><br><br>" . '<input type="radio" name="optionRadio" value="1" class="form-radio" checked>' . $option1 . '<br>'
											. '<input type="radio" name="optionRadio" value="2" class="form-radio"> ' . $option2 . '<br>'
											. '<input type="radio" name="optionRadio" value="3" class="form-radio"> ' . $option3 . '<br>'
											. '<input type="radio" name="optionRadio" value="4" class="form-radio"> ' . $option4 . '<br><br><br>'
											. '<button type="submit" class="submitAnswer" name="saveAnswer" value="' . $regNo . '|' . $quesNo . '|' . $questionIdToBeQueried . '|' . $moduleIdToBeQueried . '|' . $questionNumber . '"><b>Save Answer</b></button>'
											. '<button type="submit" class="submitAnswer" name="nextQuestion" value="' . $regNo . '|' . $quesNo . '|' . $questionIdToBeQueried . '|' . $moduleIdToBeQueried . '|' . $questionNumber . '"><b>Next Question</b></button>'
											. '<button type="submit" class="submitAnswer" name="submitAnswer" value="' . $regNo . '"><b>Submit Test</b></button></form>';
		}
		else if($answerChoosen == 2) {
			$questionBlock = '<form>' . $quesNo . ") <b>" . $questionText 	. "</b><br><br>" . '<input type="radio" name="optionRadio" value="1" class="form-radio"> ' . $option1 . '<br>'
											. '<input type="radio" name="optionRadio" value="2" class="form-radio" checked> ' . $option2 . '<br>'
											. '<input type="radio" name="optionRadio" value="3" class="form-radio"> ' . $option3 . '<br>'
											. '<input type="radio" name="optionRadio" value="4" class="form-radio"> ' . $option4 . '<br><br><br>'
											. '<button type="submit" class="submitAnswer" name="saveAnswer" value="' . $regNo . '|' . $quesNo . '|' . $questionIdToBeQueried . '|' . $moduleIdToBeQueried . '|' . $questionNumber . '"><b>Save Answer</b></button>'
											. '<button type="submit" class="submitAnswer" name="nextQuestion" value="' . $regNo . '|' . $quesNo . '|' . $questionIdToBeQueried . '|' . $moduleIdToBeQueried . '|' . $questionNumber . '"><b>Next Question</b></button>'
											. '<button type="submit" class="submitAnswer" name="submitAnswer" value="' . $regNo . '"><b>Submit Test</b></button></form>';
		}
		else if($answerChoosen == 3) {
			$questionBlock = '<form>' . $quesNo . ") <b>" . $questionText 	. "</b><br><br>" . '<input type="radio" name="optionRadio" value="1" class="form-radio"> ' . $option1 . '<br>'
											. '<input type="radio" name="optionRadio" value="2" class="form-radio"> ' . $option2 . '<br>'
											. '<input type="radio" name="optionRadio" value="3" class="form-radio" checked> ' . $option3 . '<br>'
											. '<input type="radio" name="optionRadio" value="4" class="form-radio"> ' . $option4 . '<br><br><br>'
											. '<button type="submit" class="submitAnswer" name="saveAnswer" value="' . $regNo . '|' . $quesNo . '|' . $questionIdToBeQueried . '|' . $moduleIdToBeQueried . '|' . $questionNumber . '"><b>Save Answer</b></button>'
											. '<button type="submit" class="submitAnswer" name="nextQuestion" value="' . $regNo . '|' . $quesNo . '|' . $questionIdToBeQueried . '|' . $moduleIdToBeQueried . '|' . $questionNumber . '"><b>Next Question</b></button>'
											. '<button type="submit" class="submitAnswer" name="submitAnswer" value="' . $regNo . '"><b>Submit Test</b></button></form>';
		}
		else if($answerChoosen == 4) {
			$questionBlock = '<form>' . $quesNo . ") <b>" . $questionText 	. "</b><br><br>" . '<input type="radio" name="optionRadio" value="1" class="form-radio"> ' . $option1 . '<br>'
											. '<input type="radio" name="optionRadio" value="2" class="form-radio"> ' . $option2 . '<br>'
											. '<input type="radio" name="optionRadio" value="3" class="form-radio"> ' . $option3 . '<br>'
											. '<input type="radio" name="optionRadio" value="4" class="form-radio" checked> ' . $option4 . '<br><br><br>'
											. '<button type="submit" class="submitAnswer" name="saveAnswer" value="' . $regNo . '|' . $quesNo . '|' . $questionIdToBeQueried . '|' . $moduleIdToBeQueried . '|' . $questionNumber . '"><b>Save Answer</b></button>' 
											. '<button type="submit" class="submitAnswer" name="nextQuestion" value="' . $regNo . '|' . $quesNo . '|' . $questionIdToBeQueried . '|' . $moduleIdToBeQueried . '|' . $questionNumber . '"><b>Next Question</b></button>'
											. '<button type="submit" class="submitAnswer" name="submitAnswer" value="' . $regNo . '"><b>Submit Test</b></button></form>';
		}
		else {
			$questionBlock = '<form>' . $quesNo . ") <b>" . $questionText 	. "</b><br><br>" . '<input type="radio" name="optionRadio" value="1" class="form-radio"> ' . $option1 . '<br>'
											. '<input type="radio" name="optionRadio" value="2" class="form-radio"> ' . $option2 . '<br>'
											. '<input type="radio" name="optionRadio" value="3" class="form-radio"> ' . $option3 . '<br>'
											. '<input type="radio" name="optionRadio" value="4" class="form-radio"> ' . $option4 . '<br><br><br>'
											. '<button type="submit" class="submitAnswer" name="saveAnswer" value="' . $regNo . '|' . $quesNo . '|' . $questionIdToBeQueried . '|' . $moduleIdToBeQueried . '|' . $questionNumber . '"><b>Save Answer</b></button>'
											. '<button type="submit" class="submitAnswer" name="nextQuestion" value="' . $regNo . '|' . $quesNo . '|' . $questionIdToBeQueried . '|' . $moduleIdToBeQueried . '|' . $questionNumber . '"><b>Next Question</b></button>'
											. '<button type="submit" class="submitAnswer" name="submitAnswer" value="' . $regNo . '"><b>Submit Test</b></button></form>';
		}		
	}
	else {
		$questionBlock = '<h3 class="errorDisplay">This user have already taken the test.</h3>';
	}
	
	if( isset($_GET['saveAnswer']) ) {
		$choosenAnswer = 0;
		$choosenAnswer = $_GET['optionRadio'];
		if($choosenAnswer == 1 || $choosenAnswer == 2 || $choosenAnswer == 3 || $choosenAnswer == 4) {
			$valueString = $_GET['saveAnswer'];
			$regNo = explode("|", $valueString)[0];
			$currentQuestion = explode("|", $valueString)[1];
			$nextQuestion = explode("|", $valueString)[1] + 1;
			$questionIdToBeQueried = explode("|", $valueString)[2];
			$moduleIdToBeQueried = explode("|", $valueString)[3];
			$totalNumberOfQuestions = explode("|", $valueString)[4];
			
			$saveAnswerResult = saveAnswerToDatabase($regNo, $questionIdToBeQueried,  $choosenAnswer, $moduleIdToBeQueried);
			
			$encodedRegNo = base64_encode($regNo);
			
			header("Location: questions_page.php?regno=$encodedRegNo&quesno=$currentQuestion");
		}
		else {
			$valueString = $_GET['saveAnswer'];
			$regNo = explode("|", $valueString)[0];
			$encodedRegNo = base64_encode($regNo);
			$currentQuestion = explode("|", $valueString)[1];
			$errorcode = 0;
			header("Location: questions_page.php?regno=$encodedRegNo&quesno=$currentQuestion&errorcode=$errorcode");
		}
	}
	
	if( isset($_GET['nextQuestion']) ) {
			$choosenAnswer = 0;
			$choosenAnswer = $_GET['optionRadio'];
			if($choosenAnswer == 1 || $choosenAnswer == 2 || $choosenAnswer == 3 || $choosenAnswer == 4) {
				$valueString = $_GET['nextQuestion'];
				$regNo = explode("|", $valueString)[0];
				$nextQuestion = explode("|", $valueString)[1] + 1;
				$questionIdToBeQueried = explode("|", $valueString)[2];
				$moduleIdToBeQueried = explode("|", $valueString)[3];
				$totalNumberOfQuestions = explode("|", $valueString)[4];
				
				$saveAnswerResult = saveAnswerToDatabase($regNo, $questionIdToBeQueried,  $choosenAnswer, $moduleIdToBeQueried);
				
				$encodedRegNo = base64_encode($regNo);
				writeToErrorFile($totalNumberOfQuestions . "|" . $nextQuestion);
				if($totalNumberOfQuestions <= $nextQuestion) {
					header("Location: save_results.php?regno=$encodedRegNo");
				}
				else {
					header("Location: questions_page.php?regno=$encodedRegNo&quesno=$nextQuestion");
				}
			}
			else {
				$valueString = $_GET['nextQuestion'];
				$regNo = explode("|", $valueString)[0];
				$encodedRegNo = base64_encode($regNo);
				$currentQuestion = explode("|", $valueString)[1];
				$totalNumberOfQuestions = explode("|", $valueString)[4];
				$nextQuestion = $currentQuestion + 1;
				if($totalNumberOfQuestions <= $nextQuestion) {
					header("Location: save_results.php?regno=$encodedRegNo");
				}
				else {
					header("Location: questions_page.php?regno=$encodedRegNo&quesno=$nextQuestion");
				}
			}
	}
	
	if( isset($_GET['submitAnswer']) ) {
		$valueString = $_GET['submitAnswer'];
		$regNo = explode("|", $valueString)[0];
		
		$encodedRegNo = base64_encode($regNo);
		
		header("Location: save_results.php?optioncode=usualexit&regno=$encodedRegNo");
	}
	
?>
	
	<head>
		  <title>Evertz Interview - Written Test</title>
		  <link rel="stylesheet" href="css/style_questions_page.css"/>
		  <link rel='shortcut icon' href='images/evertz_favicon.ico'/>
		  <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1" />
	</head>
	
	<body>
		
		<img src="images/evertz_logo.png" id="logo">
		
		<h3 id="instructionHeading"><u>Evertz India - Online Test</u></h3>
		
		<table class="emptyTable">
			<tr class="emptyRow">
				<th class="emptyHeader">
					<div name="questionNavigation" class="questionNavigation">
						<form name="questionNavigationForm">
							<?php
								if ( isset($navigationPanelDesign) ) {
									
									echo "$navigationPanelDesign";
								}
							?>
						</form>
					
					</div>
				</th>
				<th class="emptyHeader">
					<div name="question" class="question">
							<?php
								if ( isset($questionBlock) ) {
											
											echo "$questionBlock";
										}
							?>
							<p class="errorDisplay">
								<?php
									if ( isset($errMSG) ) {
										
										echo "<br>$errMSG<br>";
									}
								?>
							<div class="instructions">
								<br><h4><u>Save Answer:</u> Saves the answer to the database</h4>
								<h4><u>Next Question:</u> Saves the answer to the database if selected. If not selected, navigates to the next question</h4>
								<h4><u>Submit Test:</u> Saves the answers to the database and completes the test. Only choose this option if you want to discontinue the test. You will not be able to re-take the test if this button is pressed</h4>
							</div>
					</div>
					
					</p>
				</th>
			</tr>
		</table>
		
		<div name="timer" class="timer" id="timer">
			
		</div>
		
		<script type = "text/javascript">
		
			function getParameterByName(name, url) {
				if (!url) url = window.location.href;
				name = name.replace(/[\[\]]/g, '\\$&');
				var regex = new RegExp('[?&]' + name + '(=([^&#]*)|&|#|$)'),
					results = regex.exec(url);
				if (!results) return null;
				if (!results[2]) return '';
				return decodeURIComponent(results[2].replace(/\+/g, ' '));
			}
			
			setInterval(function() {
					var xmlHTTP = new XMLHttpRequest();
					xmlHTTP.open("GET", "response.php", false);
					xmlHTTP.send(null);
					document.getElementById("timer").innerHTML = xmlHTTP.responseText;
					
					var registerNumber = getParameterByName('regno');
					
					if(xmlHTTP.responseText == "00:00:01") {
						window.location.href = 'save_results.php?optioncode=timerout&regno=' + registerNumber;
					}
					
				},1000);
				
			</script>
		
		
	</body>