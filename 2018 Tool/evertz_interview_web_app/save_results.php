<?php
	
	require_once 'dbconnect.php';
	
	function writeToErrorFile($error) {
		$logFile = "supportingfiles/logFile.log";
		$handle = fopen($logFile, 'w') or die('Cannot open file: ' . $logFile);
		fwrite($handle, $error);
		fclose($handle);
		return True;
	}
	
	function calculateAnswers($regNo) {
		
		$numberOfQuestionsForTheUserQuery = mysql_query("SELECT COUNT(ID) FROM STUDENT_QUESTIONS WHERE REGISTER_NUMBER = '$regNo'");
		$numberOfQuestionsForTheUserArray = mysql_fetch_array($numberOfQuestionsForTheUserQuery);
		$numberOfQuestionsForTheUser = $numberOfQuestionsForTheUserArray[0];
		
		mysql_query("UPDATE STUDENT_RESULTS SET MODULE_SCORE = 0 WHERE REGISTER_NUMBER = '$regNo'");
		
		$errorCode = "";
		
		for($i=1; $i<=$numberOfQuestionsForTheUser; $i++) {
			
			$questionIdQuery = mysql_query("SELECT QUESTION_ID FROM STUDENT_QUESTIONS WHERE REGISTER_NUMBER = '$regNo' AND TEMP_ID = '$i'");
			$questionIdArray = mysql_fetch_array($questionIdQuery);
			$questionId = $questionIdArray[0];
			
			$moduleIdQuery = mysql_query("SELECT MODULE_ID FROM STUDENT_QUESTIONS WHERE REGISTER_NUMBER = '$regNo' AND TEMP_ID = '$i'");
			$moduleIdArray = mysql_fetch_array($moduleIdQuery);
			$moduleId = $moduleIdArray[0];
			
			$moduleDescriptionQuery = mysql_query("SELECT MODULE_DESCRIPTION FROM MODULE_MAPPING WHERE MODULE_ID = '$moduleId'");
			$moduleDescriptionArray = mysql_fetch_array($moduleDescriptionQuery);
			$moduleDescription = $moduleDescriptionArray[0];
			
			$answerIdQuery = mysql_query("SELECT ANSWER_CHOOSEN FROM STUDENT_QUESTIONS WHERE REGISTER_NUMBER = '$regNo' AND TEMP_ID = '$i'");
			$answerIdArray = mysql_fetch_array($answerIdQuery);
			$answerId = $answerIdArray[0];
			
			$answerFromDatabaseQuery = mysql_query("SELECT RIGHT_ANSWER FROM QUESTIONS WHERE MODULE_ID = '$moduleId' AND ID_TEMP = '$questionId'");
			$answerFromDatabaseArray = mysql_fetch_array($answerFromDatabaseQuery);
			$answerFromDatabase = $answerFromDatabaseArray[0];
			
			$existingResultQuery = mysql_query("SELECT MODULE_SCORE FROM STUDENT_RESULTS WHERE REGISTER_NUMBER = '$regNo' AND MODULE_ID = '$moduleId'");
			$existingResultArray = mysql_fetch_array($existingResultQuery);
			$existingResult = $existingResultArray[0];
			
			if($answerId == $answerFromDatabase) {
				
				$newResult = $existingResult + 1; 
				mysql_query("UPDATE STUDENT_RESULTS SET MODULE_SCORE = '$newResult' WHERE MODULE_ID = '$moduleId' AND REGISTER_NUMBER = '$regNo'");
				$newResult = 0;
			}
		}
	}
	
	$encodedRegNo = $_GET['regno'];
	$regNo = base64_decode($encodedRegNo);
	$optionCode = $_GET['optioncode'];
	
	if($optionCode == "timerout") {
		$saveAnswerHTML = '<h3 id="instructionHeading">Thank you for taking the test !</h3><h3 id="instructionHeading">Your responses have been recorded, you may leave the hall.</h3>';
		calculateAnswers($regNo);
		mysql_query("DELETE FROM STUDENT_QUESTIONS WHERE REGISTER_NUMBER = '$regNo';");
		mysql_query("DELETE FROM STUDENT_SESSION WHERE REGISTER_NUMBER = '$regNo';");
		mysql_query("UPDATE STUDENT_DETAILS SET TEST_ALREADY_TAKEN = 1 WHERE REGISTER_NUMBER = '$regNo'");
	}
	else { 
		$testAlreadyTakenFlagQuery = mysql_query("SELECT TEST_ALREADY_TAKEN FROM STUDENT_DETAILS WHERE REGISTER_NUMBER = '$regNo'");
		$testAlreadyTakenFlagArray = mysql_fetch_array($testAlreadyTakenFlagQuery);
		$testAlreadyTakenFlag = $testAlreadyTakenFlagArray[0];
		
		if($testAlreadyTakenFlag == 0) {
			
			$saveAnswerHTML = '<h3 id="instructionHeading">Do you want to save the test and exit ?</h3><form>
								<button type="submit" class="saveAnswers" name="saveAnswers" value="' . $regNo . '|Yes"><b>Yes</b></button>
								<button type="submit" class="saveAnswers" name="saveAnswers" value="' . $regNo . '|No"><b>No</b></button></form>';
		}
		else {
			$saveAnswerHTML = '<br><h3 class="errorDisplay">This user have already taken the test.</h3>';
		}
		
		if ( isset($_GET['saveAnswers']) ) {
			$saveAnswersValue = $_GET['saveAnswers'];
			$regNo = explode("|", $saveAnswersValue)[0];
			$saveFlag = explode("|", $saveAnswersValue)[1];
			if($saveFlag == "Yes") {
				calculateAnswers($regNo);
				$saveAnswerHTML = '<h3 id="instructionHeading">Thank you for taking the test !</h3><h3 id="instructionHeading">Your responses have been recorded, you may leave the hall.</h3>';
				mysql_query("DELETE FROM STUDENT_QUESTIONS WHERE REGISTER_NUMBER = '$regNo';");
				mysql_query("DELETE FROM STUDENT_SESSION WHERE REGISTER_NUMBER = '$regNo';");
				mysql_query("UPDATE STUDENT_DETAILS SET TEST_ALREADY_TAKEN = 1 WHERE REGISTER_NUMBER = '$regNo'");
			}
			else {
				$encodedRegNo = base64_encode($regNo);
				header("Location: questions_page.php?regno=$encodedRegNo&quesno=1");
			}
		}
	}
	
?>
	
	<head>
		  <title>Evertz Interview - Written Test</title>
		  <link rel="stylesheet" href="css/style_questions_page.css"/>
		  <link rel='shortcut icon' href='images/evertz_favicon.ico'/>
		  <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1" />
	</head>
	
	<body>
		
		<img src="images/evertz_logo.png" id="logo">
		
		<h3 id="instructionHeading"><u>Evertz India - Online Test</u></h3><br>
		
		<?php
			if ( isset($saveAnswerHTML) ) {
				echo "$saveAnswerHTML";
			}
		?>
		

		
	</body>