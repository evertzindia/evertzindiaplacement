<?php
	
	$errMSG = "";
	
	session_start();
	
	function writeToErrorFile($error) {
		$logFile = "supportingfiles/logFile.log";
		$handle = fopen($logFile, 'w') or die('Cannot open file: ' . $logFile);
		fwrite($handle, $error);
		fclose($handle);
		return True;
	}
	
	function generateRandomQuestions($regno, $test_choosen) {
		
		$deleteExistingEntryIfAvailable = mysql_query("DELETE FROM STUDENT_QUESTIONS WHERE REGISTER_NUMBER = '$regno'");
		$numberOfModulesQueryResult = mysql_query("SELECT COUNT(DISTINCT(MODULE_NUMBER)) FROM MODULE_MAPPING WHERE MODULE_ACTIVE = 1");
		$numberOfModulesArray = mysql_fetch_array($numberOfModulesQueryResult);
		$numberOfModules = $numberOfModulesArray[0];
		
		$numberOfModulesQueryResult = mysql_query("	SELECT MODULE_ID FROM MODULE_MAPPING WHERE MODULE_ACTIVE = 1 AND MODULE_ID = '$test_choosen'
													UNION
													SELECT MODULE_ID FROM MODULE_MAPPING WHERE MODULE_ID NOT LIKE 'P%'AND MODULE_ACTIVE = 1");
	
		$j=1;
		$k=1;
		while($row = mysql_fetch_assoc($numberOfModulesQueryResult))
		{
			$moduleId = $row['MODULE_ID'];
			$numberOfQuestionsInModuleArray = mysql_query("SELECT COUNT(ID) AS NUMBER FROM QUESTIONS WHERE MODULE_ID = '$moduleId'");
			$numberOfQuestions = mysql_fetch_array($numberOfQuestionsInModuleArray);
			$numberOfQuestionsInModules = $numberOfQuestions[0];
			
			$randomArray = range(1, $numberOfQuestionsInModules);
			shuffle($randomArray);
			
			for($i=0; $i<count($randomArray); $i++) {
				mysql_query("INSERT INTO STUDENT_QUESTIONS (REGISTER_NUMBER, MODULE_ID, TEMP_ORDER_ID, QUESTION_ID, TEMP_ID, TAKEN, ANSWER_CHOOSEN) VALUES ('$regno', '$moduleId', $k, $randomArray[$i], $j, 0, 0)");
				$j=$j+1;
			}
			
			mysql_query("INSERT INTO STUDENT_RESULTS (REGISTER_NUMBER, MODULE_ID, MODULE_SCORE) VALUES ('$regno', '$moduleId', 0)");
			mysql_query("INSERT INTO STUDENT_SESSION (REGISTER_NUMBER, TIMER_VALUE) VALUES ('$regno', '45')");
			
			$i=0;
			$k=$k+1;
		}
		
		return True;
	}

	require_once 'dbconnect.php';
	$date= date("d-m-Y"); 

	session_start();

	if( isset($_GET['start']) )                                         
	{
		$regno = $_GET['regno'];
		$name = $_GET['name'];
		$dob = $_GET['dob'];
		$gender = $_GET['gender'];
		$branch = $_GET['stream'];
		$cgpa = $_GET['cgpa'];
		$cname = $_GET['college_name'];
		$email = $_GET['email'];
		$mobile = $_GET['mobile'];
		$degree = $_GET['degree'];
		$ypass = $_GET['ypass'];

		$regno = strip_tags(trim($regno));           
		$name = strip_tags(trim($name));
		$dob = strip_tags(trim($dob));
		$gender = strip_tags(trim($gender));
		$branch = strip_tags(trim($branch));
		$cgpa = strip_tags(trim($cgpa));
		$cname = strip_tags(trim($cname));
		$email = strip_tags(trim($email));
		$mobile = strip_tags(trim($mobile));
		$degree = strip_tags(trim($degree));
		$ypass = strip_tags(trim($ypass));
		
		$res1 = mysql_query("SELECT register_number FROM student_details WHERE register_number = '$regno'");
		$userRow1 = mysql_fetch_array($res1);
		$rcount = mysql_num_rows($res1);
		
		if($rcount>0)
		{
			$errMSG = "User already available.";
		}
		else
		{
			$query2= "INSERT INTO student_details (register_number, name, dob, gender, branch,cgpa, college_name, personal_email, contact_number, degree, test_date, year_passed, test_already_taken)
			VALUES ('$regno', '$name', '$dob', '$gender', '$branch', '$cgpa', '$cname', '$email', '$mobile', '$degree', '$date', '$ypass', '0')";
			$result2 = mysql_query($query2);

			if($result2)
			{
			   $test_choosen=$_GET['programming'];
			   $randomGeneratorResult = generateRandomQuestions($regno, $test_choosen);
			   
			   if($randomGeneratorResult != True) {
					$errMSG = "Please report to your supervisor";
			   }
			   
			   $encodedRegNo = base64_encode($regno);
			   
				$duration = "";
				$res = mysql_query("SELECT TIMER_VALUE FROM STUDENT_SESSION WHERE REGISTER_NUMBER = '$regno'");
				
				while($row = mysql_fetch_array($res)){
					$duration = $row["TIMER_VALUE"];
				}
				
				$_SESSION["duration"] = $duration;
				$_SESSION["start_time"] = date("Y-m-d H:i:s");

				$end_time = date('Y-m-d H:i:s', strtotime('+'.$_SESSION["duration"].'minutes',strtotime($_SESSION["start_time"])));

				$_SESSION["end_time"] = $end_time;
			   
			   header("Location: questions_page.php?regno=$encodedRegNo&quesno=1");
			}
			else
			{
			   $errMSG = "ERROR loading data, please report your supervisor";
			}
			
		} 
	}

	?>
	
	<head>
		  <title>Evertz Interview - Written Test</title>
		  <link rel="stylesheet" href="css/style.css"/>
		  <link rel='shortcut icon' href='images/evertz_favicon.ico'/>
	</head>
	
	<body>
		
		<img src="images/evertz_logo.png" id="logo">
		
		<form>
			<div class="centeringDiv">
			
				<input type="text" id="registrationNumber" name="regno" pattern="[A-Z0-9]{2,}" title="Only characters and numbers" placeholder="University Registration No." required><br>
				<input type="text" id="firstName" name="name" pattern="[A-Za-z ]{2,}" title="Only characters and numbers" placeholder="Full Name" required><br>
				<input type="text" id="dob" name="dob" placeholder="Date of Birth (DD-MM-YYYY)" pattern="(0[1-9]|1[0-9]|2[0-9]|3[01])-(0[1-9]|1[012])-[0-9]{4}" title="Valid format is DD-MM-YYYY" required><br>
				<select name="gender" required>
					<option value="M">Male</option>
					<option value="F">Female</option>
					<option value="O">Others</option>
				</select>
				
				 <select name="college_name" required>
					<option value="NMIT">NMIT</option>
				</select>

				<select name="degree" required>
					<option value="BE">B.E</option>
					<option value="B.TECH">B.TECH</option>
					<option value="M.E">M.E</option>
					<option value="M.TECH">M.TECH</option>
					<option value="MCA">MCA</option>
				</select>
				<br>
				  
				<select name="stream" required>
					<option value="CSE">CSE</option>
					<option value="ECE">ECE</option>
					<option value="EEE">EEE</option>
					<option value="IS">IS</option>
					<option value="IT">IT</option>
				</select>
				<br>
				 
				<input type="text" id="ypass" name="ypass" placeholder="Year of Passing" value="2019" class="input-field" required><br>
				
				<input type="text" id="overallCGPA" pattern="[0-9]+(\.[0-9]{0,2})?" title="Only percentage allowed. Eg. 85.25" name="cgpa" placeholder="Overall CGPA or Percentage" class="input-field" required><br>
				<input type="text" pattern="[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}$" id="personalEmailId" title="Must contain @ symbol and . character" name="email" placeholder="Personal Email Address" class="input-field" required><br>
				<input type="text" pattern="[0-9]{10}" title="Must contain only numbers of 10 length" id="personalMobileumber" name="mobile" placeholder="Personal Mobile Number" class="input-field" required><br>
			
			</div>
			
			<h3 id="instructionHeading"><u>Instructions</u></h3>
			
			<div class="instructions">
				<h5>1. Total duration of the test is 45 minutes. You will be presented 60 questions to answer.</h5>
				<h5>2. When the "Start Test" button is pressed, your time will start.</h5>
				<h5>3. No negative marking for incorrect answers.</h5>
				<h5>4. Each question carries 1 mark.</h5>
				<h5>5. The programming module can be chosen by the candidate. The programming module consists of 10 questions.</h5>
				<h5>6. Other modules. SQL, Networking, Linux, Aptitude and Broadcast consists of 10 questions each and need to be answered.</h5>
				<h5>7. Navigation between different questions is possible by choosing the question from the left navigation panel.</h5>
				<h5>8. If the timer runs out after 45 minutes, your answers will be saved the test will automatically exit.</h5>
				<h5>9. Please do not refresh or close the browser. Let the invigilator know if the browser is closed so the test can be re-taken.</h5>
			</div>
			
			<br>
			<caption><h3>Choose one Programming language</h3></caption>
			 
			<div class="radioButtons">
				
				<?php
					$numberOfModulesQueryResult = mysql_query("SELECT MODULE_DESCRIPTION FROM MODULE_MAPPING WHERE MODULE_ID LIKE 'P%' ORDER BY ID");
					$radioButtonString = "";
					$i=1;
					while($row = mysql_fetch_assoc($numberOfModulesQueryResult))
					{
						if($i == 1) {
							$radioButtonString = $radioButtonString . '<input type="radio" class="form-radio" name="programming" value="P' . $i . '" checked>' . $row['MODULE_DESCRIPTION'] . '<br>';
						}
						else {
							$radioButtonString = $radioButtonString . '<input type="radio" class="form-radio" name="programming" value="P' . $i . '">' . $row['MODULE_DESCRIPTION'] . '<br>';
						}

						$i = $i+1;
					}
					
					echo $radioButtonString;
				 ?>
				
				<br>
			</div>
				
			<button type="submit" name="start" >Start Test</button>
			
			
			<p class="errorDisplay">
				<?php
					if ( isset($errMSG) ) {
						
						echo "<br>$errMSG<br>";
					}
				?>
			</p>
				
		</form>
	</body>