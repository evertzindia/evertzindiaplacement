<?php
	
	require_once 'dbconnect.php';
	
	function writeToErrorFile($resultText, $collegeName) {
		$logFile = "results/" . $collegeName . "_results.csv";
		$handle = fopen($logFile, 'a') or die('Cannot open file: ' . $logFile);
		fwrite($handle, $resultText);
		fclose($handle);
		header("Location:" . $logFile);
		return True;
	}
	
	if( isset($_GET['getBase64ID']) ) 
	{	
		
		$regNo = $_GET['regNo'];
		
		$displayMessage = base64_encode($regNo);
		
		mysql_query("UPDATE STUDENT_SESSION VALUES TIMER_VALUE = '45' WHERE REGISTER_NUMBER = '$regNo'");
		
		header("Location: questions_page.php?regno=$displayMessage&quesno=1");
		
		
	}
?>

<html>
	<head>
		  <title>Evertz Interview - Written Test</title>
		  <link rel="stylesheet" href="css/style.css"/>
		  <link rel='shortcut icon' href='images/evertz_favicon.ico'/>
	</head>
	<body>
			<form>
				<div class="centeringDiv">
					<input type="text" id="regNo" name="regNo" placeholder="Reg. No" ><br>
				</div>
				<input type="submit" value="Go to Test" name="getBase64ID"></button>
			</form>
			
			<p class="errorDisplay">
				<?php
					if ( isset($displayMessage) ) {
						
						echo "<br>$displayMessage<br>";
					}
				?>
			</p>
	</body>
</html>
	
