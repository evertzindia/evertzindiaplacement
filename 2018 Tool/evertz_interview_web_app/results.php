<?php
	
	require_once 'dbconnect.php';
	
	function writeToErrorFile($resultText, $collegeName) {
		$logFile = "results/" . $collegeName . "_results.csv";
		$handle = fopen($logFile, 'a') or die('Cannot open file: ' . $logFile);
		fwrite($handle, $resultText);
		fclose($handle);
		header("Location:" . $logFile);
		return True;
	}
	
	if( isset($_GET['exportData']) ) 
	{	
		
		$collegeName = $_GET['collegeName'];
		
		$registerNumberArray = mysql_query("SELECT DISTINCT(SR.REGISTER_NUMBER) FROM STUDENT_RESULTS SR JOIN STUDENT_DETAILS SD ON SR.REGISTER_NUMBER = SD.REGISTER_NUMBER WHERE SD.COLLEGE_NAME = '$collegeName'");	
		
		while($regNoAssoc = mysql_fetch_assoc($registerNumberArray)) {
			$csvText = "";
			$regNo = $regNoAssoc['REGISTER_NUMBER'];
			
			$candidateNameQuery = mysql_query("SELECT NAME FROM STUDENT_DETAILS WHERE REGISTER_NUMBER = '$regNo'");
			$candidateNameArray = mysql_fetch_array($candidateNameQuery);
			$candidateName = $candidateNameArray[0];
			
			$emailQuery = mysql_query("SELECT PERSONAL_EMAIL FROM STUDENT_DETAILS WHERE REGISTER_NUMBER = '$regNo'");
			$emailArray = mysql_fetch_array($emailQuery);
			$email = $emailArray[0];
			
			$phoneNumberQuery = mysql_query("SELECT CONTACT_NUMBER FROM STUDENT_DETAILS WHERE REGISTER_NUMBER = '$regNo'");
			$phoneNumberArray = mysql_fetch_array($phoneNumberQuery);
			$phoneNumber = $phoneNumberArray[0];
			
			$degreeQuery = mysql_query("SELECT DEGREE FROM STUDENT_DETAILS WHERE REGISTER_NUMBER = '$regNo'");
			$degreeArray = mysql_fetch_array($degreeQuery);
			$degree = $degreeArray[0];
			
			$streamQuery = mysql_query("SELECT BRANCH FROM STUDENT_DETAILS WHERE REGISTER_NUMBER = '$regNo'");
			$streamArray = mysql_fetch_array($streamQuery);
			$stream = $streamArray[0];
			
			$csvText = $regNo . "|" . $candidateName . "|" . $email . "|" . $phoneNumber . "|" . $degree . " " . $stream . "|";
			
			$moduleScoreArray = mysql_query("SELECT MODULE_SCORE, MODULE_ID FROM STUDENT_RESULTS WHERE REGISTER_NUMBER = '$regNo'");
			$totalScore = 0;
			$countOfModules = 1;
			while($moduleScoreAssoc = mysql_fetch_assoc($moduleScoreArray)) {
				$moduleScore = $moduleScoreAssoc['MODULE_SCORE'];
				$moduleId = $moduleScoreAssoc['MODULE_ID'];
				
				$moduleDescriptionQuery = mysql_query("SELECT MODULE_DESCRIPTION FROM MODULE_MAPPING WHERE MODULE_ID = '$moduleId'");
				$moduleDescriptionArray = mysql_fetch_array($moduleDescriptionQuery);
				$moduleDescription = $moduleDescriptionArray[0];
				
				$csvText = $csvText . $moduleDescription . "," . $moduleScore . "|";
				
				$totalScore = $totalScore + (int)$moduleScore;
				
				if($countOfModules == 6) {
					break;
				}
				
				$countOfModules = $countOfModules + 1;
			}
			
			$csvText = $csvText . "|" . $totalScore  . "\n";
			
			writeToErrorFile($csvText, $collegeName);
		}
		$resultsFile = "results/" . $collegeName . "_results.csv";
		header("Location:" . $resultsFile);
		$displayMessage = "Export Data Completed to results.csv";
	}
?>

<html>
	<head>
		  <title>Evertz Interview - Written Test</title>
		  <link rel="stylesheet" href="css/style.css"/>
		  <link rel='shortcut icon' href='images/evertz_favicon.ico'/>
	</head>
	<body>
			<form>
				<div class="centeringDiv">
					<input type="text" id="College Name" name="collegeName" placeholder="College Name" ><br>
				</div>
				<input type="submit" value="Export Data" name="exportData"></button>
			</form>
			
			<p class="errorDisplay">
				<?php
					if ( isset($displayMessage) ) {
						
						echo "<br>$displayMessage<br>";
					}
				?>
			</p>
	</body>
</html>
	
