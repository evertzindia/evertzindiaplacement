/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import { Component, forwardRef, Inject, Input, NgZone } from '@angular/core';
import { NG_VALUE_ACCESSOR } from '@angular/forms';
import { fromEvent } from 'rxjs';
import { BaseEditor } from './base-editor';
import { NGX_MONACO_EDITOR_CONFIG } from './config';
export class EditorComponent extends BaseEditor {
    /**
     * @param {?} zone
     * @param {?} editorConfig
     */
    constructor(zone, editorConfig) {
        super(editorConfig);
        this.zone = zone;
        this.editorConfig = editorConfig;
        this._value = '';
        this.propagateChange = (/**
         * @param {?} _
         * @return {?}
         */
        (_) => { });
        this.onTouched = (/**
         * @return {?}
         */
        () => { });
    }
    /**
     * @param {?} model
     * @return {?}
     */
    set model(model) {
        this.options.model = model;
        if (this._editor) {
            this._editor.dispose();
            this.initMonaco(this.options);
        }
    }
    /**
     * @param {?} value
     * @return {?}
     */
    writeValue(value) {
        this._value = value || '';
        // Fix for value change while dispose in process.
        setTimeout((/**
         * @return {?}
         */
        () => {
            if (this._editor && !this.options.model) {
                this._editor.setValue(this._value);
            }
        }));
    }
    /**
     * @param {?} fn
     * @return {?}
     */
    registerOnChange(fn) {
        this.propagateChange = fn;
    }
    /**
     * @param {?} fn
     * @return {?}
     */
    registerOnTouched(fn) {
        this.onTouched = fn;
    }
    /**
     * @protected
     * @param {?} options
     * @return {?}
     */
    initMonaco(options) {
        /** @type {?} */
        const hasModel = !!options.model;
        if (hasModel) {
            /** @type {?} */
            const model = monaco.editor.getModel(options.model.uri || '');
            if (model) {
                options.model = model;
                options.model.setValue(this._value);
            }
            else {
                options.model = monaco.editor.createModel(options.model.value, options.model.language, options.model.uri);
            }
        }
        this._editor = monaco.editor.create(this._editorContainer.nativeElement, options);
        if (!hasModel) {
            this._editor.setValue(this._value);
        }
        this._editor.onDidChangeModelContent((/**
         * @param {?} e
         * @return {?}
         */
        (e) => {
            /** @type {?} */
            const value = this._editor.getValue();
            this.propagateChange(value);
            // value is not propagated to parent when executing outside zone.
            this.zone.run((/**
             * @return {?}
             */
            () => this._value = value));
        }));
        this._editor.onDidBlurEditorWidget((/**
         * @return {?}
         */
        () => {
            this.onTouched();
        }));
        // refresh layout on resize event.
        if (this._windowResizeSubscription) {
            this._windowResizeSubscription.unsubscribe();
        }
        this._windowResizeSubscription = fromEvent(window, 'resize').subscribe((/**
         * @return {?}
         */
        () => this._editor.layout()));
        this.onInit.emit(this._editor);
    }
}
EditorComponent.decorators = [
    { type: Component, args: [{
                selector: 'ngx-monaco-editor',
                template: '<div class="editor-container" #editorContainer></div>',
                providers: [{
                        provide: NG_VALUE_ACCESSOR,
                        useExisting: forwardRef((/**
                         * @return {?}
                         */
                        () => EditorComponent)),
                        multi: true
                    }],
                styles: [`
    :host {
      display: block;
      height: 200px;
    }

    .editor-container {
      width: 100%;
      height: 98%;
    }
  `]
            }] }
];
/** @nocollapse */
EditorComponent.ctorParameters = () => [
    { type: NgZone },
    { type: undefined, decorators: [{ type: Inject, args: [NGX_MONACO_EDITOR_CONFIG,] }] }
];
EditorComponent.propDecorators = {
    model: [{ type: Input, args: ['model',] }]
};
if (false) {
    /**
     * @type {?}
     * @private
     */
    EditorComponent.prototype._value;
    /** @type {?} */
    EditorComponent.prototype.propagateChange;
    /** @type {?} */
    EditorComponent.prototype.onTouched;
    /**
     * @type {?}
     * @private
     */
    EditorComponent.prototype.zone;
    /**
     * @type {?}
     * @private
     */
    EditorComponent.prototype.editorConfig;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiZWRpdG9yLmNvbXBvbmVudC5qcyIsInNvdXJjZVJvb3QiOiJuZzovL25neC1tb25hY28tZWRpdG9yLyIsInNvdXJjZXMiOlsibGliL2VkaXRvci5jb21wb25lbnQudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7OztBQUFBLE9BQU8sRUFBRSxTQUFTLEVBQUUsVUFBVSxFQUFFLE1BQU0sRUFBRSxLQUFLLEVBQUUsTUFBTSxFQUFFLE1BQU0sZUFBZSxDQUFDO0FBQzdFLE9BQU8sRUFBd0IsaUJBQWlCLEVBQUUsTUFBTSxnQkFBZ0IsQ0FBQztBQUN6RSxPQUFPLEVBQUUsU0FBUyxFQUFFLE1BQU0sTUFBTSxDQUFDO0FBRWpDLE9BQU8sRUFBRSxVQUFVLEVBQUUsTUFBTSxlQUFlLENBQUM7QUFDM0MsT0FBTyxFQUFFLHdCQUF3QixFQUF5QixNQUFNLFVBQVUsQ0FBQztBQXVCM0UsTUFBTSxPQUFPLGVBQWdCLFNBQVEsVUFBVTs7Ozs7SUFlN0MsWUFBb0IsSUFBWSxFQUE0QyxZQUFtQztRQUM3RyxLQUFLLENBQUMsWUFBWSxDQUFDLENBQUM7UUFERixTQUFJLEdBQUosSUFBSSxDQUFRO1FBQTRDLGlCQUFZLEdBQVosWUFBWSxDQUF1QjtRQWR2RyxXQUFNLEdBQVcsRUFBRSxDQUFDO1FBRTVCLG9CQUFlOzs7O1FBQUcsQ0FBQyxDQUFNLEVBQUUsRUFBRSxHQUFFLENBQUMsRUFBQztRQUNqQyxjQUFTOzs7UUFBRyxHQUFHLEVBQUUsR0FBRSxDQUFDLEVBQUM7SUFhckIsQ0FBQzs7Ozs7SUFYRCxJQUNJLEtBQUssQ0FBQyxLQUFxQjtRQUM3QixJQUFJLENBQUMsT0FBTyxDQUFDLEtBQUssR0FBRyxLQUFLLENBQUM7UUFDM0IsSUFBSSxJQUFJLENBQUMsT0FBTyxFQUFFO1lBQ2hCLElBQUksQ0FBQyxPQUFPLENBQUMsT0FBTyxFQUFFLENBQUM7WUFDdkIsSUFBSSxDQUFDLFVBQVUsQ0FBQyxJQUFJLENBQUMsT0FBTyxDQUFDLENBQUM7U0FDL0I7SUFDSCxDQUFDOzs7OztJQU1ELFVBQVUsQ0FBQyxLQUFVO1FBQ25CLElBQUksQ0FBQyxNQUFNLEdBQUcsS0FBSyxJQUFJLEVBQUUsQ0FBQztRQUMxQixpREFBaUQ7UUFDakQsVUFBVTs7O1FBQUMsR0FBRyxFQUFFO1lBQ2QsSUFBSSxJQUFJLENBQUMsT0FBTyxJQUFJLENBQUMsSUFBSSxDQUFDLE9BQU8sQ0FBQyxLQUFLLEVBQUU7Z0JBQ3ZDLElBQUksQ0FBQyxPQUFPLENBQUMsUUFBUSxDQUFDLElBQUksQ0FBQyxNQUFNLENBQUMsQ0FBQzthQUNwQztRQUNILENBQUMsRUFBQyxDQUFDO0lBQ0wsQ0FBQzs7Ozs7SUFFRCxnQkFBZ0IsQ0FBQyxFQUFPO1FBQ3RCLElBQUksQ0FBQyxlQUFlLEdBQUcsRUFBRSxDQUFDO0lBQzVCLENBQUM7Ozs7O0lBRUQsaUJBQWlCLENBQUMsRUFBTztRQUN2QixJQUFJLENBQUMsU0FBUyxHQUFHLEVBQUUsQ0FBQztJQUN0QixDQUFDOzs7Ozs7SUFFUyxVQUFVLENBQUMsT0FBWTs7Y0FFekIsUUFBUSxHQUFHLENBQUMsQ0FBQyxPQUFPLENBQUMsS0FBSztRQUVoQyxJQUFJLFFBQVEsRUFBRTs7a0JBQ04sS0FBSyxHQUFHLE1BQU0sQ0FBQyxNQUFNLENBQUMsUUFBUSxDQUFDLE9BQU8sQ0FBQyxLQUFLLENBQUMsR0FBRyxJQUFJLEVBQUUsQ0FBQztZQUM3RCxJQUFHLEtBQUssRUFBRTtnQkFDUixPQUFPLENBQUMsS0FBSyxHQUFHLEtBQUssQ0FBQztnQkFDdEIsT0FBTyxDQUFDLEtBQUssQ0FBQyxRQUFRLENBQUMsSUFBSSxDQUFDLE1BQU0sQ0FBQyxDQUFDO2FBQ3JDO2lCQUFNO2dCQUNMLE9BQU8sQ0FBQyxLQUFLLEdBQUcsTUFBTSxDQUFDLE1BQU0sQ0FBQyxXQUFXLENBQUMsT0FBTyxDQUFDLEtBQUssQ0FBQyxLQUFLLEVBQUUsT0FBTyxDQUFDLEtBQUssQ0FBQyxRQUFRLEVBQUUsT0FBTyxDQUFDLEtBQUssQ0FBQyxHQUFHLENBQUMsQ0FBQzthQUMzRztTQUNGO1FBRUQsSUFBSSxDQUFDLE9BQU8sR0FBRyxNQUFNLENBQUMsTUFBTSxDQUFDLE1BQU0sQ0FBQyxJQUFJLENBQUMsZ0JBQWdCLENBQUMsYUFBYSxFQUFFLE9BQU8sQ0FBQyxDQUFDO1FBRWxGLElBQUksQ0FBQyxRQUFRLEVBQUU7WUFDYixJQUFJLENBQUMsT0FBTyxDQUFDLFFBQVEsQ0FBQyxJQUFJLENBQUMsTUFBTSxDQUFDLENBQUM7U0FDcEM7UUFFRCxJQUFJLENBQUMsT0FBTyxDQUFDLHVCQUF1Qjs7OztRQUFDLENBQUMsQ0FBTSxFQUFFLEVBQUU7O2tCQUN4QyxLQUFLLEdBQUcsSUFBSSxDQUFDLE9BQU8sQ0FBQyxRQUFRLEVBQUU7WUFDckMsSUFBSSxDQUFDLGVBQWUsQ0FBQyxLQUFLLENBQUMsQ0FBQztZQUM1QixpRUFBaUU7WUFDakUsSUFBSSxDQUFDLElBQUksQ0FBQyxHQUFHOzs7WUFBQyxHQUFHLEVBQUUsQ0FBQyxJQUFJLENBQUMsTUFBTSxHQUFHLEtBQUssRUFBQyxDQUFDO1FBQzNDLENBQUMsRUFBQyxDQUFDO1FBRUgsSUFBSSxDQUFDLE9BQU8sQ0FBQyxxQkFBcUI7OztRQUFDLEdBQUcsRUFBRTtZQUN0QyxJQUFJLENBQUMsU0FBUyxFQUFFLENBQUM7UUFDbkIsQ0FBQyxFQUFDLENBQUM7UUFFSCxrQ0FBa0M7UUFDbEMsSUFBSSxJQUFJLENBQUMseUJBQXlCLEVBQUU7WUFDbEMsSUFBSSxDQUFDLHlCQUF5QixDQUFDLFdBQVcsRUFBRSxDQUFDO1NBQzlDO1FBQ0QsSUFBSSxDQUFDLHlCQUF5QixHQUFHLFNBQVMsQ0FBQyxNQUFNLEVBQUUsUUFBUSxDQUFDLENBQUMsU0FBUzs7O1FBQUMsR0FBRyxFQUFFLENBQUMsSUFBSSxDQUFDLE9BQU8sQ0FBQyxNQUFNLEVBQUUsRUFBQyxDQUFDO1FBQ3BHLElBQUksQ0FBQyxNQUFNLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxPQUFPLENBQUMsQ0FBQztJQUNqQyxDQUFDOzs7WUE5RkYsU0FBUyxTQUFDO2dCQUNULFFBQVEsRUFBRSxtQkFBbUI7Z0JBQzdCLFFBQVEsRUFBRSx1REFBdUQ7Z0JBWWpFLFNBQVMsRUFBRSxDQUFDO3dCQUNWLE9BQU8sRUFBRSxpQkFBaUI7d0JBQzFCLFdBQVcsRUFBRSxVQUFVOzs7d0JBQUMsR0FBRyxFQUFFLENBQUMsZUFBZSxFQUFDO3dCQUM5QyxLQUFLLEVBQUUsSUFBSTtxQkFDWixDQUFDO3lCQWZPOzs7Ozs7Ozs7O0dBVVI7YUFNRjs7OztZQTNCOEMsTUFBTTs0Q0EyQ2hCLE1BQU0sU0FBQyx3QkFBd0I7OztvQkFUakUsS0FBSyxTQUFDLE9BQU87Ozs7Ozs7SUFMZCxpQ0FBNEI7O0lBRTVCLDBDQUFpQzs7SUFDakMsb0NBQXFCOzs7OztJQVdULCtCQUFvQjs7Ozs7SUFBRSx1Q0FBNkUiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBDb21wb25lbnQsIGZvcndhcmRSZWYsIEluamVjdCwgSW5wdXQsIE5nWm9uZSB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xuaW1wb3J0IHsgQ29udHJvbFZhbHVlQWNjZXNzb3IsIE5HX1ZBTFVFX0FDQ0VTU09SIH0gZnJvbSAnQGFuZ3VsYXIvZm9ybXMnO1xuaW1wb3J0IHsgZnJvbUV2ZW50IH0gZnJvbSAncnhqcyc7XG5cbmltcG9ydCB7IEJhc2VFZGl0b3IgfSBmcm9tICcuL2Jhc2UtZWRpdG9yJztcbmltcG9ydCB7IE5HWF9NT05BQ09fRURJVE9SX0NPTkZJRywgTmd4TW9uYWNvRWRpdG9yQ29uZmlnIH0gZnJvbSAnLi9jb25maWcnO1xuaW1wb3J0IHsgTmd4RWRpdG9yTW9kZWwgfSBmcm9tICcuL3R5cGVzJztcblxuQENvbXBvbmVudCh7XG4gIHNlbGVjdG9yOiAnbmd4LW1vbmFjby1lZGl0b3InLFxuICB0ZW1wbGF0ZTogJzxkaXYgY2xhc3M9XCJlZGl0b3ItY29udGFpbmVyXCIgI2VkaXRvckNvbnRhaW5lcj48L2Rpdj4nLFxuICBzdHlsZXM6IFtgXG4gICAgOmhvc3Qge1xuICAgICAgZGlzcGxheTogYmxvY2s7XG4gICAgICBoZWlnaHQ6IDIwMHB4O1xuICAgIH1cblxuICAgIC5lZGl0b3ItY29udGFpbmVyIHtcbiAgICAgIHdpZHRoOiAxMDAlO1xuICAgICAgaGVpZ2h0OiA5OCU7XG4gICAgfVxuICBgXSxcbiAgcHJvdmlkZXJzOiBbe1xuICAgIHByb3ZpZGU6IE5HX1ZBTFVFX0FDQ0VTU09SLFxuICAgIHVzZUV4aXN0aW5nOiBmb3J3YXJkUmVmKCgpID0+IEVkaXRvckNvbXBvbmVudCksXG4gICAgbXVsdGk6IHRydWVcbiAgfV1cbn0pXG5leHBvcnQgY2xhc3MgRWRpdG9yQ29tcG9uZW50IGV4dGVuZHMgQmFzZUVkaXRvciBpbXBsZW1lbnRzIENvbnRyb2xWYWx1ZUFjY2Vzc29yIHtcbiAgcHJpdmF0ZSBfdmFsdWU6IHN0cmluZyA9ICcnO1xuXG4gIHByb3BhZ2F0ZUNoYW5nZSA9IChfOiBhbnkpID0+IHt9O1xuICBvblRvdWNoZWQgPSAoKSA9PiB7fTtcblxuICBASW5wdXQoJ21vZGVsJylcbiAgc2V0IG1vZGVsKG1vZGVsOiBOZ3hFZGl0b3JNb2RlbCkge1xuICAgIHRoaXMub3B0aW9ucy5tb2RlbCA9IG1vZGVsO1xuICAgIGlmICh0aGlzLl9lZGl0b3IpIHtcbiAgICAgIHRoaXMuX2VkaXRvci5kaXNwb3NlKCk7XG4gICAgICB0aGlzLmluaXRNb25hY28odGhpcy5vcHRpb25zKTtcbiAgICB9XG4gIH1cblxuICBjb25zdHJ1Y3Rvcihwcml2YXRlIHpvbmU6IE5nWm9uZSwgQEluamVjdChOR1hfTU9OQUNPX0VESVRPUl9DT05GSUcpIHByaXZhdGUgZWRpdG9yQ29uZmlnOiBOZ3hNb25hY29FZGl0b3JDb25maWcpIHtcbiAgICBzdXBlcihlZGl0b3JDb25maWcpO1xuICB9XG5cbiAgd3JpdGVWYWx1ZSh2YWx1ZTogYW55KTogdm9pZCB7XG4gICAgdGhpcy5fdmFsdWUgPSB2YWx1ZSB8fCAnJztcbiAgICAvLyBGaXggZm9yIHZhbHVlIGNoYW5nZSB3aGlsZSBkaXNwb3NlIGluIHByb2Nlc3MuXG4gICAgc2V0VGltZW91dCgoKSA9PiB7XG4gICAgICBpZiAodGhpcy5fZWRpdG9yICYmICF0aGlzLm9wdGlvbnMubW9kZWwpIHtcbiAgICAgICAgdGhpcy5fZWRpdG9yLnNldFZhbHVlKHRoaXMuX3ZhbHVlKTtcbiAgICAgIH1cbiAgICB9KTtcbiAgfVxuXG4gIHJlZ2lzdGVyT25DaGFuZ2UoZm46IGFueSk6IHZvaWQge1xuICAgIHRoaXMucHJvcGFnYXRlQ2hhbmdlID0gZm47XG4gIH1cblxuICByZWdpc3Rlck9uVG91Y2hlZChmbjogYW55KTogdm9pZCB7XG4gICAgdGhpcy5vblRvdWNoZWQgPSBmbjtcbiAgfVxuXG4gIHByb3RlY3RlZCBpbml0TW9uYWNvKG9wdGlvbnM6IGFueSk6IHZvaWQge1xuXG4gICAgY29uc3QgaGFzTW9kZWwgPSAhIW9wdGlvbnMubW9kZWw7XG5cbiAgICBpZiAoaGFzTW9kZWwpIHtcbiAgICAgIGNvbnN0IG1vZGVsID0gbW9uYWNvLmVkaXRvci5nZXRNb2RlbChvcHRpb25zLm1vZGVsLnVyaSB8fCAnJyk7XG4gICAgICBpZihtb2RlbCkge1xuICAgICAgICBvcHRpb25zLm1vZGVsID0gbW9kZWw7XG4gICAgICAgIG9wdGlvbnMubW9kZWwuc2V0VmFsdWUodGhpcy5fdmFsdWUpO1xuICAgICAgfSBlbHNlIHtcbiAgICAgICAgb3B0aW9ucy5tb2RlbCA9IG1vbmFjby5lZGl0b3IuY3JlYXRlTW9kZWwob3B0aW9ucy5tb2RlbC52YWx1ZSwgb3B0aW9ucy5tb2RlbC5sYW5ndWFnZSwgb3B0aW9ucy5tb2RlbC51cmkpO1xuICAgICAgfVxuICAgIH1cblxuICAgIHRoaXMuX2VkaXRvciA9IG1vbmFjby5lZGl0b3IuY3JlYXRlKHRoaXMuX2VkaXRvckNvbnRhaW5lci5uYXRpdmVFbGVtZW50LCBvcHRpb25zKTtcblxuICAgIGlmICghaGFzTW9kZWwpIHtcbiAgICAgIHRoaXMuX2VkaXRvci5zZXRWYWx1ZSh0aGlzLl92YWx1ZSk7XG4gICAgfVxuXG4gICAgdGhpcy5fZWRpdG9yLm9uRGlkQ2hhbmdlTW9kZWxDb250ZW50KChlOiBhbnkpID0+IHtcbiAgICAgIGNvbnN0IHZhbHVlID0gdGhpcy5fZWRpdG9yLmdldFZhbHVlKCk7XG4gICAgICB0aGlzLnByb3BhZ2F0ZUNoYW5nZSh2YWx1ZSk7XG4gICAgICAvLyB2YWx1ZSBpcyBub3QgcHJvcGFnYXRlZCB0byBwYXJlbnQgd2hlbiBleGVjdXRpbmcgb3V0c2lkZSB6b25lLlxuICAgICAgdGhpcy56b25lLnJ1bigoKSA9PiB0aGlzLl92YWx1ZSA9IHZhbHVlKTtcbiAgICB9KTtcblxuICAgIHRoaXMuX2VkaXRvci5vbkRpZEJsdXJFZGl0b3JXaWRnZXQoKCkgPT4ge1xuICAgICAgdGhpcy5vblRvdWNoZWQoKTtcbiAgICB9KTtcblxuICAgIC8vIHJlZnJlc2ggbGF5b3V0IG9uIHJlc2l6ZSBldmVudC5cbiAgICBpZiAodGhpcy5fd2luZG93UmVzaXplU3Vic2NyaXB0aW9uKSB7XG4gICAgICB0aGlzLl93aW5kb3dSZXNpemVTdWJzY3JpcHRpb24udW5zdWJzY3JpYmUoKTtcbiAgICB9XG4gICAgdGhpcy5fd2luZG93UmVzaXplU3Vic2NyaXB0aW9uID0gZnJvbUV2ZW50KHdpbmRvdywgJ3Jlc2l6ZScpLnN1YnNjcmliZSgoKSA9PiB0aGlzLl9lZGl0b3IubGF5b3V0KCkpO1xuICAgIHRoaXMub25Jbml0LmVtaXQodGhpcy5fZWRpdG9yKTtcbiAgfVxuXG59XG4iXX0=