/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import { InjectionToken } from '@angular/core';
/** @type {?} */
export const NGX_MONACO_EDITOR_CONFIG = new InjectionToken('NGX_MONACO_EDITOR_CONFIG');
/**
 * @record
 */
export function NgxMonacoEditorConfig() { }
if (false) {
    /** @type {?|undefined} */
    NgxMonacoEditorConfig.prototype.baseUrl;
    /** @type {?|undefined} */
    NgxMonacoEditorConfig.prototype.defaultOptions;
    /** @type {?|undefined} */
    NgxMonacoEditorConfig.prototype.onMonacoLoad;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiY29uZmlnLmpzIiwic291cmNlUm9vdCI6Im5nOi8vbmd4LW1vbmFjby1lZGl0b3IvIiwic291cmNlcyI6WyJsaWIvY29uZmlnLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7QUFBQSxPQUFPLEVBQUUsY0FBYyxFQUFFLE1BQU0sZUFBZSxDQUFDOztBQUUvQyxNQUFNLE9BQU8sd0JBQXdCLEdBQUcsSUFBSSxjQUFjLENBQUMsMEJBQTBCLENBQUM7Ozs7QUFFdEYsMkNBSUM7OztJQUhDLHdDQUFpQjs7SUFDakIsK0NBQXlDOztJQUN6Qyw2Q0FBd0IiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBJbmplY3Rpb25Ub2tlbiB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xuXG5leHBvcnQgY29uc3QgTkdYX01PTkFDT19FRElUT1JfQ09ORklHID0gbmV3IEluamVjdGlvblRva2VuKCdOR1hfTU9OQUNPX0VESVRPUl9DT05GSUcnKTtcblxuZXhwb3J0IGludGVyZmFjZSBOZ3hNb25hY29FZGl0b3JDb25maWcge1xuICBiYXNlVXJsPzogc3RyaW5nO1xuICBkZWZhdWx0T3B0aW9ucz86IHsgW2tleTogc3RyaW5nXTogYW55OyB9LFxuICBvbk1vbmFjb0xvYWQ/OiBGdW5jdGlvbjtcbn1cbiJdfQ==