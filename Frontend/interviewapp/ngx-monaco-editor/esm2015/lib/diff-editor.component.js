/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import { Component, Inject, Input } from '@angular/core';
import { fromEvent } from 'rxjs';
import { BaseEditor } from './base-editor';
import { NGX_MONACO_EDITOR_CONFIG } from './config';
export class DiffEditorComponent extends BaseEditor {
    /**
     * @param {?} editorConfig
     */
    constructor(editorConfig) {
        super(editorConfig);
        this.editorConfig = editorConfig;
    }
    /**
     * @param {?} model
     * @return {?}
     */
    set originalModel(model) {
        this._originalModel = model;
        if (this._editor) {
            this._editor.dispose();
            this.initMonaco(this.options);
        }
    }
    /**
     * @param {?} model
     * @return {?}
     */
    set modifiedModel(model) {
        this._modifiedModel = model;
        if (this._editor) {
            this._editor.dispose();
            this.initMonaco(this.options);
        }
    }
    /**
     * @protected
     * @param {?} options
     * @return {?}
     */
    initMonaco(options) {
        if (!this._originalModel || !this._modifiedModel) {
            throw new Error('originalModel or modifiedModel not found for ngx-monaco-diff-editor');
        }
        this._originalModel.language = this._originalModel.language || options.language;
        this._modifiedModel.language = this._modifiedModel.language || options.language;
        /** @type {?} */
        let originalModel = monaco.editor.createModel(this._originalModel.code, this._originalModel.language);
        /** @type {?} */
        let modifiedModel = monaco.editor.createModel(this._modifiedModel.code, this._modifiedModel.language);
        this._editorContainer.nativeElement.innerHTML = '';
        /** @type {?} */
        const theme = options.theme;
        this._editor = monaco.editor.createDiffEditor(this._editorContainer.nativeElement, options);
        options.theme = theme;
        this._editor.setModel({
            original: originalModel,
            modified: modifiedModel
        });
        // refresh layout on resize event.
        if (this._windowResizeSubscription) {
            this._windowResizeSubscription.unsubscribe();
        }
        this._windowResizeSubscription = fromEvent(window, 'resize').subscribe((/**
         * @return {?}
         */
        () => this._editor.layout()));
        this.onInit.emit(this._editor);
    }
}
DiffEditorComponent.decorators = [
    { type: Component, args: [{
                selector: 'ngx-monaco-diff-editor',
                template: '<div class="editor-container" #editorContainer></div>',
                styles: [`
    :host {
      display: block;
      height: 200px;
    }

    .editor-container {
      width: 100%;
      height: 98%;
    }
  `]
            }] }
];
/** @nocollapse */
DiffEditorComponent.ctorParameters = () => [
    { type: undefined, decorators: [{ type: Inject, args: [NGX_MONACO_EDITOR_CONFIG,] }] }
];
DiffEditorComponent.propDecorators = {
    originalModel: [{ type: Input, args: ['originalModel',] }],
    modifiedModel: [{ type: Input, args: ['modifiedModel',] }]
};
if (false) {
    /** @type {?} */
    DiffEditorComponent.prototype._originalModel;
    /** @type {?} */
    DiffEditorComponent.prototype._modifiedModel;
    /**
     * @type {?}
     * @private
     */
    DiffEditorComponent.prototype.editorConfig;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiZGlmZi1lZGl0b3IuY29tcG9uZW50LmpzIiwic291cmNlUm9vdCI6Im5nOi8vbmd4LW1vbmFjby1lZGl0b3IvIiwic291cmNlcyI6WyJsaWIvZGlmZi1lZGl0b3IuY29tcG9uZW50LnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7QUFBQSxPQUFPLEVBQUUsU0FBUyxFQUFFLE1BQU0sRUFBRSxLQUFLLEVBQUUsTUFBTSxlQUFlLENBQUM7QUFDekQsT0FBTyxFQUFFLFNBQVMsRUFBRSxNQUFNLE1BQU0sQ0FBQztBQUVqQyxPQUFPLEVBQUUsVUFBVSxFQUFFLE1BQU0sZUFBZSxDQUFDO0FBQzNDLE9BQU8sRUFBRSx3QkFBd0IsRUFBeUIsTUFBTSxVQUFVLENBQUM7QUFrQjNFLE1BQU0sT0FBTyxtQkFBb0IsU0FBUSxVQUFVOzs7O0lBdUJqRCxZQUFzRCxZQUFtQztRQUN2RixLQUFLLENBQUMsWUFBWSxDQUFDLENBQUM7UUFEZ0MsaUJBQVksR0FBWixZQUFZLENBQXVCO0lBRXpGLENBQUM7Ozs7O0lBcEJELElBQ0ksYUFBYSxDQUFDLEtBQXNCO1FBQ3RDLElBQUksQ0FBQyxjQUFjLEdBQUcsS0FBSyxDQUFDO1FBQzVCLElBQUksSUFBSSxDQUFDLE9BQU8sRUFBRTtZQUNoQixJQUFJLENBQUMsT0FBTyxDQUFDLE9BQU8sRUFBRSxDQUFDO1lBQ3ZCLElBQUksQ0FBQyxVQUFVLENBQUMsSUFBSSxDQUFDLE9BQU8sQ0FBQyxDQUFDO1NBQy9CO0lBQ0gsQ0FBQzs7Ozs7SUFFRCxJQUNJLGFBQWEsQ0FBQyxLQUFzQjtRQUN0QyxJQUFJLENBQUMsY0FBYyxHQUFHLEtBQUssQ0FBQztRQUM1QixJQUFJLElBQUksQ0FBQyxPQUFPLEVBQUU7WUFDaEIsSUFBSSxDQUFDLE9BQU8sQ0FBQyxPQUFPLEVBQUUsQ0FBQztZQUN2QixJQUFJLENBQUMsVUFBVSxDQUFDLElBQUksQ0FBQyxPQUFPLENBQUMsQ0FBQztTQUMvQjtJQUNILENBQUM7Ozs7OztJQU1TLFVBQVUsQ0FBQyxPQUFZO1FBRS9CLElBQUksQ0FBQyxJQUFJLENBQUMsY0FBYyxJQUFJLENBQUMsSUFBSSxDQUFDLGNBQWMsRUFBRTtZQUNoRCxNQUFNLElBQUksS0FBSyxDQUFDLHFFQUFxRSxDQUFDLENBQUM7U0FDeEY7UUFFRCxJQUFJLENBQUMsY0FBYyxDQUFDLFFBQVEsR0FBRyxJQUFJLENBQUMsY0FBYyxDQUFDLFFBQVEsSUFBSSxPQUFPLENBQUMsUUFBUSxDQUFDO1FBQ2hGLElBQUksQ0FBQyxjQUFjLENBQUMsUUFBUSxHQUFHLElBQUksQ0FBQyxjQUFjLENBQUMsUUFBUSxJQUFJLE9BQU8sQ0FBQyxRQUFRLENBQUM7O1lBRTVFLGFBQWEsR0FBRyxNQUFNLENBQUMsTUFBTSxDQUFDLFdBQVcsQ0FBQyxJQUFJLENBQUMsY0FBYyxDQUFDLElBQUksRUFBRSxJQUFJLENBQUMsY0FBYyxDQUFDLFFBQVEsQ0FBQzs7WUFDakcsYUFBYSxHQUFHLE1BQU0sQ0FBQyxNQUFNLENBQUMsV0FBVyxDQUFDLElBQUksQ0FBQyxjQUFjLENBQUMsSUFBSSxFQUFFLElBQUksQ0FBQyxjQUFjLENBQUMsUUFBUSxDQUFDO1FBRXJHLElBQUksQ0FBQyxnQkFBZ0IsQ0FBQyxhQUFhLENBQUMsU0FBUyxHQUFHLEVBQUUsQ0FBQzs7Y0FDN0MsS0FBSyxHQUFHLE9BQU8sQ0FBQyxLQUFLO1FBQzNCLElBQUksQ0FBQyxPQUFPLEdBQUcsTUFBTSxDQUFDLE1BQU0sQ0FBQyxnQkFBZ0IsQ0FBQyxJQUFJLENBQUMsZ0JBQWdCLENBQUMsYUFBYSxFQUFFLE9BQU8sQ0FBQyxDQUFDO1FBQzVGLE9BQU8sQ0FBQyxLQUFLLEdBQUcsS0FBSyxDQUFDO1FBQ3RCLElBQUksQ0FBQyxPQUFPLENBQUMsUUFBUSxDQUFDO1lBQ3BCLFFBQVEsRUFBRSxhQUFhO1lBQ3ZCLFFBQVEsRUFBRSxhQUFhO1NBQ3hCLENBQUMsQ0FBQztRQUVILGtDQUFrQztRQUNsQyxJQUFJLElBQUksQ0FBQyx5QkFBeUIsRUFBRTtZQUNsQyxJQUFJLENBQUMseUJBQXlCLENBQUMsV0FBVyxFQUFFLENBQUM7U0FDOUM7UUFDRCxJQUFJLENBQUMseUJBQXlCLEdBQUcsU0FBUyxDQUFDLE1BQU0sRUFBRSxRQUFRLENBQUMsQ0FBQyxTQUFTOzs7UUFBQyxHQUFHLEVBQUUsQ0FBQyxJQUFJLENBQUMsT0FBTyxDQUFDLE1BQU0sRUFBRSxFQUFDLENBQUM7UUFDcEcsSUFBSSxDQUFDLE1BQU0sQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLE9BQU8sQ0FBQyxDQUFDO0lBQ2pDLENBQUM7OztZQXJFRixTQUFTLFNBQUM7Z0JBQ1QsUUFBUSxFQUFFLHdCQUF3QjtnQkFDbEMsUUFBUSxFQUFFLHVEQUF1RDt5QkFDeEQ7Ozs7Ozs7Ozs7R0FVUjthQUNGOzs7OzRDQXdCYyxNQUFNLFNBQUMsd0JBQXdCOzs7NEJBbEIzQyxLQUFLLFNBQUMsZUFBZTs0QkFTckIsS0FBSyxTQUFDLGVBQWU7Ozs7SUFadEIsNkNBQWdDOztJQUNoQyw2Q0FBZ0M7Ozs7O0lBb0JwQiwyQ0FBNkUiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBDb21wb25lbnQsIEluamVjdCwgSW5wdXQgfSBmcm9tICdAYW5ndWxhci9jb3JlJztcbmltcG9ydCB7IGZyb21FdmVudCB9IGZyb20gJ3J4anMnO1xuXG5pbXBvcnQgeyBCYXNlRWRpdG9yIH0gZnJvbSAnLi9iYXNlLWVkaXRvcic7XG5pbXBvcnQgeyBOR1hfTU9OQUNPX0VESVRPUl9DT05GSUcsIE5neE1vbmFjb0VkaXRvckNvbmZpZyB9IGZyb20gJy4vY29uZmlnJztcbmltcG9ydCB7IERpZmZFZGl0b3JNb2RlbCB9IGZyb20gJy4vdHlwZXMnO1xuXG5AQ29tcG9uZW50KHtcbiAgc2VsZWN0b3I6ICduZ3gtbW9uYWNvLWRpZmYtZWRpdG9yJyxcbiAgdGVtcGxhdGU6ICc8ZGl2IGNsYXNzPVwiZWRpdG9yLWNvbnRhaW5lclwiICNlZGl0b3JDb250YWluZXI+PC9kaXY+JyxcbiAgc3R5bGVzOiBbYFxuICAgIDpob3N0IHtcbiAgICAgIGRpc3BsYXk6IGJsb2NrO1xuICAgICAgaGVpZ2h0OiAyMDBweDtcbiAgICB9XG5cbiAgICAuZWRpdG9yLWNvbnRhaW5lciB7XG4gICAgICB3aWR0aDogMTAwJTtcbiAgICAgIGhlaWdodDogOTglO1xuICAgIH1cbiAgYF1cbn0pXG5leHBvcnQgY2xhc3MgRGlmZkVkaXRvckNvbXBvbmVudCBleHRlbmRzIEJhc2VFZGl0b3Ige1xuXG4gIF9vcmlnaW5hbE1vZGVsOiBEaWZmRWRpdG9yTW9kZWw7XG4gIF9tb2RpZmllZE1vZGVsOiBEaWZmRWRpdG9yTW9kZWw7XG5cbiAgQElucHV0KCdvcmlnaW5hbE1vZGVsJylcbiAgc2V0IG9yaWdpbmFsTW9kZWwobW9kZWw6IERpZmZFZGl0b3JNb2RlbCkge1xuICAgIHRoaXMuX29yaWdpbmFsTW9kZWwgPSBtb2RlbDtcbiAgICBpZiAodGhpcy5fZWRpdG9yKSB7XG4gICAgICB0aGlzLl9lZGl0b3IuZGlzcG9zZSgpO1xuICAgICAgdGhpcy5pbml0TW9uYWNvKHRoaXMub3B0aW9ucyk7XG4gICAgfVxuICB9XG5cbiAgQElucHV0KCdtb2RpZmllZE1vZGVsJylcbiAgc2V0IG1vZGlmaWVkTW9kZWwobW9kZWw6IERpZmZFZGl0b3JNb2RlbCkge1xuICAgIHRoaXMuX21vZGlmaWVkTW9kZWwgPSBtb2RlbDtcbiAgICBpZiAodGhpcy5fZWRpdG9yKSB7XG4gICAgICB0aGlzLl9lZGl0b3IuZGlzcG9zZSgpO1xuICAgICAgdGhpcy5pbml0TW9uYWNvKHRoaXMub3B0aW9ucyk7XG4gICAgfVxuICB9XG5cbiAgY29uc3RydWN0b3IoQEluamVjdChOR1hfTU9OQUNPX0VESVRPUl9DT05GSUcpIHByaXZhdGUgZWRpdG9yQ29uZmlnOiBOZ3hNb25hY29FZGl0b3JDb25maWcpIHtcbiAgICBzdXBlcihlZGl0b3JDb25maWcpO1xuICB9XG5cbiAgcHJvdGVjdGVkIGluaXRNb25hY28ob3B0aW9uczogYW55KTogdm9pZCB7XG5cbiAgICBpZiAoIXRoaXMuX29yaWdpbmFsTW9kZWwgfHwgIXRoaXMuX21vZGlmaWVkTW9kZWwpIHtcbiAgICAgIHRocm93IG5ldyBFcnJvcignb3JpZ2luYWxNb2RlbCBvciBtb2RpZmllZE1vZGVsIG5vdCBmb3VuZCBmb3Igbmd4LW1vbmFjby1kaWZmLWVkaXRvcicpO1xuICAgIH1cblxuICAgIHRoaXMuX29yaWdpbmFsTW9kZWwubGFuZ3VhZ2UgPSB0aGlzLl9vcmlnaW5hbE1vZGVsLmxhbmd1YWdlIHx8IG9wdGlvbnMubGFuZ3VhZ2U7XG4gICAgdGhpcy5fbW9kaWZpZWRNb2RlbC5sYW5ndWFnZSA9IHRoaXMuX21vZGlmaWVkTW9kZWwubGFuZ3VhZ2UgfHwgb3B0aW9ucy5sYW5ndWFnZTtcblxuICAgIGxldCBvcmlnaW5hbE1vZGVsID0gbW9uYWNvLmVkaXRvci5jcmVhdGVNb2RlbCh0aGlzLl9vcmlnaW5hbE1vZGVsLmNvZGUsIHRoaXMuX29yaWdpbmFsTW9kZWwubGFuZ3VhZ2UpO1xuICAgIGxldCBtb2RpZmllZE1vZGVsID0gbW9uYWNvLmVkaXRvci5jcmVhdGVNb2RlbCh0aGlzLl9tb2RpZmllZE1vZGVsLmNvZGUsIHRoaXMuX21vZGlmaWVkTW9kZWwubGFuZ3VhZ2UpO1xuXG4gICAgdGhpcy5fZWRpdG9yQ29udGFpbmVyLm5hdGl2ZUVsZW1lbnQuaW5uZXJIVE1MID0gJyc7XG4gICAgY29uc3QgdGhlbWUgPSBvcHRpb25zLnRoZW1lO1xuICAgIHRoaXMuX2VkaXRvciA9IG1vbmFjby5lZGl0b3IuY3JlYXRlRGlmZkVkaXRvcih0aGlzLl9lZGl0b3JDb250YWluZXIubmF0aXZlRWxlbWVudCwgb3B0aW9ucyk7XG4gICAgb3B0aW9ucy50aGVtZSA9IHRoZW1lO1xuICAgIHRoaXMuX2VkaXRvci5zZXRNb2RlbCh7XG4gICAgICBvcmlnaW5hbDogb3JpZ2luYWxNb2RlbCxcbiAgICAgIG1vZGlmaWVkOiBtb2RpZmllZE1vZGVsXG4gICAgfSk7XG5cbiAgICAvLyByZWZyZXNoIGxheW91dCBvbiByZXNpemUgZXZlbnQuXG4gICAgaWYgKHRoaXMuX3dpbmRvd1Jlc2l6ZVN1YnNjcmlwdGlvbikge1xuICAgICAgdGhpcy5fd2luZG93UmVzaXplU3Vic2NyaXB0aW9uLnVuc3Vic2NyaWJlKCk7XG4gICAgfVxuICAgIHRoaXMuX3dpbmRvd1Jlc2l6ZVN1YnNjcmlwdGlvbiA9IGZyb21FdmVudCh3aW5kb3csICdyZXNpemUnKS5zdWJzY3JpYmUoKCkgPT4gdGhpcy5fZWRpdG9yLmxheW91dCgpKTtcbiAgICB0aGlzLm9uSW5pdC5lbWl0KHRoaXMuX2VkaXRvcik7XG4gIH1cblxufVxuIl19