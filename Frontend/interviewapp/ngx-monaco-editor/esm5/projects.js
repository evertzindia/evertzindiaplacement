/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
/*
 * Public API Surface of editor
 */
export { EditorComponent } from './lib/editor.component';
export { MonacoEditorModule } from './lib/editor.module';
export { NGX_MONACO_EDITOR_CONFIG } from './lib/config';
export {} from './lib/types';
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoicHJvamVjdHMuanMiLCJzb3VyY2VSb290Ijoibmc6Ly9uZ3gtbW9uYWNvLWVkaXRvci8iLCJzb3VyY2VzIjpbInByb2plY3RzLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7Ozs7QUFJQSxnQ0FBYyx3QkFBd0IsQ0FBQztBQUN2QyxtQ0FBYyxxQkFBcUIsQ0FBQztBQUNwQyx5Q0FBYyxjQUFjLENBQUM7QUFDN0IsZUFBYyxhQUFhLENBQUMiLCJzb3VyY2VzQ29udGVudCI6WyIvKlxuICogUHVibGljIEFQSSBTdXJmYWNlIG9mIGVkaXRvclxuICovXG5cbmV4cG9ydCAqIGZyb20gJy4vbGliL2VkaXRvci5jb21wb25lbnQnO1xuZXhwb3J0ICogZnJvbSAnLi9saWIvZWRpdG9yLm1vZHVsZSc7XG5leHBvcnQgKiBmcm9tICcuL2xpYi9jb25maWcnO1xuZXhwb3J0ICogZnJvbSAnLi9saWIvdHlwZXMnO1xuIl19