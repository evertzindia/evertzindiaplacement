/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
/**
 * @record
 */
export function DiffEditorModel() { }
if (false) {
    /** @type {?} */
    DiffEditorModel.prototype.code;
    /** @type {?} */
    DiffEditorModel.prototype.language;
}
/**
 * @record
 */
export function NgxEditorModel() { }
if (false) {
    /** @type {?} */
    NgxEditorModel.prototype.value;
    /** @type {?|undefined} */
    NgxEditorModel.prototype.language;
    /** @type {?|undefined} */
    NgxEditorModel.prototype.uri;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoidHlwZXMuanMiLCJzb3VyY2VSb290Ijoibmc6Ly9uZ3gtbW9uYWNvLWVkaXRvci8iLCJzb3VyY2VzIjpbImxpYi90eXBlcy50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7Ozs7O0FBQ0EscUNBR0M7OztJQUZHLCtCQUFhOztJQUNiLG1DQUFpQjs7Ozs7QUFFckIsb0NBSUM7OztJQUhHLCtCQUFjOztJQUNkLGtDQUFrQjs7SUFDbEIsNkJBQVUiLCJzb3VyY2VzQ29udGVudCI6WyIvLy8gPHJlZmVyZW5jZSBwYXRoPVwibW9uYWNvLmQudHNcIiAvPlxuZXhwb3J0IGludGVyZmFjZSBEaWZmRWRpdG9yTW9kZWwge1xuICAgIGNvZGU6IHN0cmluZztcbiAgICBsYW5ndWFnZTogc3RyaW5nO1xufVxuZXhwb3J0IGludGVyZmFjZSBOZ3hFZGl0b3JNb2RlbCB7XG4gICAgdmFsdWU6IHN0cmluZztcbiAgICBsYW5ndWFnZT86IHN0cmluZztcbiAgICB1cmk/OiBhbnk7XG59XG4iXX0=