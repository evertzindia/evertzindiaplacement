/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import * as tslib_1 from "tslib";
import { Component, Inject, Input } from '@angular/core';
import { fromEvent } from 'rxjs';
import { BaseEditor } from './base-editor';
import { NGX_MONACO_EDITOR_CONFIG } from './config';
var DiffEditorComponent = /** @class */ (function (_super) {
    tslib_1.__extends(DiffEditorComponent, _super);
    function DiffEditorComponent(editorConfig) {
        var _this = _super.call(this, editorConfig) || this;
        _this.editorConfig = editorConfig;
        return _this;
    }
    Object.defineProperty(DiffEditorComponent.prototype, "originalModel", {
        set: /**
         * @param {?} model
         * @return {?}
         */
        function (model) {
            this._originalModel = model;
            if (this._editor) {
                this._editor.dispose();
                this.initMonaco(this.options);
            }
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(DiffEditorComponent.prototype, "modifiedModel", {
        set: /**
         * @param {?} model
         * @return {?}
         */
        function (model) {
            this._modifiedModel = model;
            if (this._editor) {
                this._editor.dispose();
                this.initMonaco(this.options);
            }
        },
        enumerable: true,
        configurable: true
    });
    /**
     * @protected
     * @param {?} options
     * @return {?}
     */
    DiffEditorComponent.prototype.initMonaco = /**
     * @protected
     * @param {?} options
     * @return {?}
     */
    function (options) {
        var _this = this;
        if (!this._originalModel || !this._modifiedModel) {
            throw new Error('originalModel or modifiedModel not found for ngx-monaco-diff-editor');
        }
        this._originalModel.language = this._originalModel.language || options.language;
        this._modifiedModel.language = this._modifiedModel.language || options.language;
        /** @type {?} */
        var originalModel = monaco.editor.createModel(this._originalModel.code, this._originalModel.language);
        /** @type {?} */
        var modifiedModel = monaco.editor.createModel(this._modifiedModel.code, this._modifiedModel.language);
        this._editorContainer.nativeElement.innerHTML = '';
        /** @type {?} */
        var theme = options.theme;
        this._editor = monaco.editor.createDiffEditor(this._editorContainer.nativeElement, options);
        options.theme = theme;
        this._editor.setModel({
            original: originalModel,
            modified: modifiedModel
        });
        // refresh layout on resize event.
        if (this._windowResizeSubscription) {
            this._windowResizeSubscription.unsubscribe();
        }
        this._windowResizeSubscription = fromEvent(window, 'resize').subscribe((/**
         * @return {?}
         */
        function () { return _this._editor.layout(); }));
        this.onInit.emit(this._editor);
    };
    DiffEditorComponent.decorators = [
        { type: Component, args: [{
                    selector: 'ngx-monaco-diff-editor',
                    template: '<div class="editor-container" #editorContainer></div>',
                    styles: ["\n    :host {\n      display: block;\n      height: 200px;\n    }\n\n    .editor-container {\n      width: 100%;\n      height: 98%;\n    }\n  "]
                }] }
    ];
    /** @nocollapse */
    DiffEditorComponent.ctorParameters = function () { return [
        { type: undefined, decorators: [{ type: Inject, args: [NGX_MONACO_EDITOR_CONFIG,] }] }
    ]; };
    DiffEditorComponent.propDecorators = {
        originalModel: [{ type: Input, args: ['originalModel',] }],
        modifiedModel: [{ type: Input, args: ['modifiedModel',] }]
    };
    return DiffEditorComponent;
}(BaseEditor));
export { DiffEditorComponent };
if (false) {
    /** @type {?} */
    DiffEditorComponent.prototype._originalModel;
    /** @type {?} */
    DiffEditorComponent.prototype._modifiedModel;
    /**
     * @type {?}
     * @private
     */
    DiffEditorComponent.prototype.editorConfig;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiZGlmZi1lZGl0b3IuY29tcG9uZW50LmpzIiwic291cmNlUm9vdCI6Im5nOi8vbmd4LW1vbmFjby1lZGl0b3IvIiwic291cmNlcyI6WyJsaWIvZGlmZi1lZGl0b3IuY29tcG9uZW50LnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7O0FBQUEsT0FBTyxFQUFFLFNBQVMsRUFBRSxNQUFNLEVBQUUsS0FBSyxFQUFFLE1BQU0sZUFBZSxDQUFDO0FBQ3pELE9BQU8sRUFBRSxTQUFTLEVBQUUsTUFBTSxNQUFNLENBQUM7QUFFakMsT0FBTyxFQUFFLFVBQVUsRUFBRSxNQUFNLGVBQWUsQ0FBQztBQUMzQyxPQUFPLEVBQUUsd0JBQXdCLEVBQXlCLE1BQU0sVUFBVSxDQUFDO0FBRzNFO0lBZXlDLCtDQUFVO0lBdUJqRCw2QkFBc0QsWUFBbUM7UUFBekYsWUFDRSxrQkFBTSxZQUFZLENBQUMsU0FDcEI7UUFGcUQsa0JBQVksR0FBWixZQUFZLENBQXVCOztJQUV6RixDQUFDO0lBcEJELHNCQUNJLDhDQUFhOzs7OztRQURqQixVQUNrQixLQUFzQjtZQUN0QyxJQUFJLENBQUMsY0FBYyxHQUFHLEtBQUssQ0FBQztZQUM1QixJQUFJLElBQUksQ0FBQyxPQUFPLEVBQUU7Z0JBQ2hCLElBQUksQ0FBQyxPQUFPLENBQUMsT0FBTyxFQUFFLENBQUM7Z0JBQ3ZCLElBQUksQ0FBQyxVQUFVLENBQUMsSUFBSSxDQUFDLE9BQU8sQ0FBQyxDQUFDO2FBQy9CO1FBQ0gsQ0FBQzs7O09BQUE7SUFFRCxzQkFDSSw4Q0FBYTs7Ozs7UUFEakIsVUFDa0IsS0FBc0I7WUFDdEMsSUFBSSxDQUFDLGNBQWMsR0FBRyxLQUFLLENBQUM7WUFDNUIsSUFBSSxJQUFJLENBQUMsT0FBTyxFQUFFO2dCQUNoQixJQUFJLENBQUMsT0FBTyxDQUFDLE9BQU8sRUFBRSxDQUFDO2dCQUN2QixJQUFJLENBQUMsVUFBVSxDQUFDLElBQUksQ0FBQyxPQUFPLENBQUMsQ0FBQzthQUMvQjtRQUNILENBQUM7OztPQUFBOzs7Ozs7SUFNUyx3Q0FBVTs7Ozs7SUFBcEIsVUFBcUIsT0FBWTtRQUFqQyxpQkEyQkM7UUF6QkMsSUFBSSxDQUFDLElBQUksQ0FBQyxjQUFjLElBQUksQ0FBQyxJQUFJLENBQUMsY0FBYyxFQUFFO1lBQ2hELE1BQU0sSUFBSSxLQUFLLENBQUMscUVBQXFFLENBQUMsQ0FBQztTQUN4RjtRQUVELElBQUksQ0FBQyxjQUFjLENBQUMsUUFBUSxHQUFHLElBQUksQ0FBQyxjQUFjLENBQUMsUUFBUSxJQUFJLE9BQU8sQ0FBQyxRQUFRLENBQUM7UUFDaEYsSUFBSSxDQUFDLGNBQWMsQ0FBQyxRQUFRLEdBQUcsSUFBSSxDQUFDLGNBQWMsQ0FBQyxRQUFRLElBQUksT0FBTyxDQUFDLFFBQVEsQ0FBQzs7WUFFNUUsYUFBYSxHQUFHLE1BQU0sQ0FBQyxNQUFNLENBQUMsV0FBVyxDQUFDLElBQUksQ0FBQyxjQUFjLENBQUMsSUFBSSxFQUFFLElBQUksQ0FBQyxjQUFjLENBQUMsUUFBUSxDQUFDOztZQUNqRyxhQUFhLEdBQUcsTUFBTSxDQUFDLE1BQU0sQ0FBQyxXQUFXLENBQUMsSUFBSSxDQUFDLGNBQWMsQ0FBQyxJQUFJLEVBQUUsSUFBSSxDQUFDLGNBQWMsQ0FBQyxRQUFRLENBQUM7UUFFckcsSUFBSSxDQUFDLGdCQUFnQixDQUFDLGFBQWEsQ0FBQyxTQUFTLEdBQUcsRUFBRSxDQUFDOztZQUM3QyxLQUFLLEdBQUcsT0FBTyxDQUFDLEtBQUs7UUFDM0IsSUFBSSxDQUFDLE9BQU8sR0FBRyxNQUFNLENBQUMsTUFBTSxDQUFDLGdCQUFnQixDQUFDLElBQUksQ0FBQyxnQkFBZ0IsQ0FBQyxhQUFhLEVBQUUsT0FBTyxDQUFDLENBQUM7UUFDNUYsT0FBTyxDQUFDLEtBQUssR0FBRyxLQUFLLENBQUM7UUFDdEIsSUFBSSxDQUFDLE9BQU8sQ0FBQyxRQUFRLENBQUM7WUFDcEIsUUFBUSxFQUFFLGFBQWE7WUFDdkIsUUFBUSxFQUFFLGFBQWE7U0FDeEIsQ0FBQyxDQUFDO1FBRUgsa0NBQWtDO1FBQ2xDLElBQUksSUFBSSxDQUFDLHlCQUF5QixFQUFFO1lBQ2xDLElBQUksQ0FBQyx5QkFBeUIsQ0FBQyxXQUFXLEVBQUUsQ0FBQztTQUM5QztRQUNELElBQUksQ0FBQyx5QkFBeUIsR0FBRyxTQUFTLENBQUMsTUFBTSxFQUFFLFFBQVEsQ0FBQyxDQUFDLFNBQVM7OztRQUFDLGNBQU0sT0FBQSxLQUFJLENBQUMsT0FBTyxDQUFDLE1BQU0sRUFBRSxFQUFyQixDQUFxQixFQUFDLENBQUM7UUFDcEcsSUFBSSxDQUFDLE1BQU0sQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLE9BQU8sQ0FBQyxDQUFDO0lBQ2pDLENBQUM7O2dCQXJFRixTQUFTLFNBQUM7b0JBQ1QsUUFBUSxFQUFFLHdCQUF3QjtvQkFDbEMsUUFBUSxFQUFFLHVEQUF1RDs2QkFDeEQsaUpBVVI7aUJBQ0Y7Ozs7Z0RBd0JjLE1BQU0sU0FBQyx3QkFBd0I7OztnQ0FsQjNDLEtBQUssU0FBQyxlQUFlO2dDQVNyQixLQUFLLFNBQUMsZUFBZTs7SUEwQ3hCLDBCQUFDO0NBQUEsQUF2RUQsQ0FleUMsVUFBVSxHQXdEbEQ7U0F4RFksbUJBQW1COzs7SUFFOUIsNkNBQWdDOztJQUNoQyw2Q0FBZ0M7Ozs7O0lBb0JwQiwyQ0FBNkUiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBDb21wb25lbnQsIEluamVjdCwgSW5wdXQgfSBmcm9tICdAYW5ndWxhci9jb3JlJztcbmltcG9ydCB7IGZyb21FdmVudCB9IGZyb20gJ3J4anMnO1xuXG5pbXBvcnQgeyBCYXNlRWRpdG9yIH0gZnJvbSAnLi9iYXNlLWVkaXRvcic7XG5pbXBvcnQgeyBOR1hfTU9OQUNPX0VESVRPUl9DT05GSUcsIE5neE1vbmFjb0VkaXRvckNvbmZpZyB9IGZyb20gJy4vY29uZmlnJztcbmltcG9ydCB7IERpZmZFZGl0b3JNb2RlbCB9IGZyb20gJy4vdHlwZXMnO1xuXG5AQ29tcG9uZW50KHtcbiAgc2VsZWN0b3I6ICduZ3gtbW9uYWNvLWRpZmYtZWRpdG9yJyxcbiAgdGVtcGxhdGU6ICc8ZGl2IGNsYXNzPVwiZWRpdG9yLWNvbnRhaW5lclwiICNlZGl0b3JDb250YWluZXI+PC9kaXY+JyxcbiAgc3R5bGVzOiBbYFxuICAgIDpob3N0IHtcbiAgICAgIGRpc3BsYXk6IGJsb2NrO1xuICAgICAgaGVpZ2h0OiAyMDBweDtcbiAgICB9XG5cbiAgICAuZWRpdG9yLWNvbnRhaW5lciB7XG4gICAgICB3aWR0aDogMTAwJTtcbiAgICAgIGhlaWdodDogOTglO1xuICAgIH1cbiAgYF1cbn0pXG5leHBvcnQgY2xhc3MgRGlmZkVkaXRvckNvbXBvbmVudCBleHRlbmRzIEJhc2VFZGl0b3Ige1xuXG4gIF9vcmlnaW5hbE1vZGVsOiBEaWZmRWRpdG9yTW9kZWw7XG4gIF9tb2RpZmllZE1vZGVsOiBEaWZmRWRpdG9yTW9kZWw7XG5cbiAgQElucHV0KCdvcmlnaW5hbE1vZGVsJylcbiAgc2V0IG9yaWdpbmFsTW9kZWwobW9kZWw6IERpZmZFZGl0b3JNb2RlbCkge1xuICAgIHRoaXMuX29yaWdpbmFsTW9kZWwgPSBtb2RlbDtcbiAgICBpZiAodGhpcy5fZWRpdG9yKSB7XG4gICAgICB0aGlzLl9lZGl0b3IuZGlzcG9zZSgpO1xuICAgICAgdGhpcy5pbml0TW9uYWNvKHRoaXMub3B0aW9ucyk7XG4gICAgfVxuICB9XG5cbiAgQElucHV0KCdtb2RpZmllZE1vZGVsJylcbiAgc2V0IG1vZGlmaWVkTW9kZWwobW9kZWw6IERpZmZFZGl0b3JNb2RlbCkge1xuICAgIHRoaXMuX21vZGlmaWVkTW9kZWwgPSBtb2RlbDtcbiAgICBpZiAodGhpcy5fZWRpdG9yKSB7XG4gICAgICB0aGlzLl9lZGl0b3IuZGlzcG9zZSgpO1xuICAgICAgdGhpcy5pbml0TW9uYWNvKHRoaXMub3B0aW9ucyk7XG4gICAgfVxuICB9XG5cbiAgY29uc3RydWN0b3IoQEluamVjdChOR1hfTU9OQUNPX0VESVRPUl9DT05GSUcpIHByaXZhdGUgZWRpdG9yQ29uZmlnOiBOZ3hNb25hY29FZGl0b3JDb25maWcpIHtcbiAgICBzdXBlcihlZGl0b3JDb25maWcpO1xuICB9XG5cbiAgcHJvdGVjdGVkIGluaXRNb25hY28ob3B0aW9uczogYW55KTogdm9pZCB7XG5cbiAgICBpZiAoIXRoaXMuX29yaWdpbmFsTW9kZWwgfHwgIXRoaXMuX21vZGlmaWVkTW9kZWwpIHtcbiAgICAgIHRocm93IG5ldyBFcnJvcignb3JpZ2luYWxNb2RlbCBvciBtb2RpZmllZE1vZGVsIG5vdCBmb3VuZCBmb3Igbmd4LW1vbmFjby1kaWZmLWVkaXRvcicpO1xuICAgIH1cblxuICAgIHRoaXMuX29yaWdpbmFsTW9kZWwubGFuZ3VhZ2UgPSB0aGlzLl9vcmlnaW5hbE1vZGVsLmxhbmd1YWdlIHx8IG9wdGlvbnMubGFuZ3VhZ2U7XG4gICAgdGhpcy5fbW9kaWZpZWRNb2RlbC5sYW5ndWFnZSA9IHRoaXMuX21vZGlmaWVkTW9kZWwubGFuZ3VhZ2UgfHwgb3B0aW9ucy5sYW5ndWFnZTtcblxuICAgIGxldCBvcmlnaW5hbE1vZGVsID0gbW9uYWNvLmVkaXRvci5jcmVhdGVNb2RlbCh0aGlzLl9vcmlnaW5hbE1vZGVsLmNvZGUsIHRoaXMuX29yaWdpbmFsTW9kZWwubGFuZ3VhZ2UpO1xuICAgIGxldCBtb2RpZmllZE1vZGVsID0gbW9uYWNvLmVkaXRvci5jcmVhdGVNb2RlbCh0aGlzLl9tb2RpZmllZE1vZGVsLmNvZGUsIHRoaXMuX21vZGlmaWVkTW9kZWwubGFuZ3VhZ2UpO1xuXG4gICAgdGhpcy5fZWRpdG9yQ29udGFpbmVyLm5hdGl2ZUVsZW1lbnQuaW5uZXJIVE1MID0gJyc7XG4gICAgY29uc3QgdGhlbWUgPSBvcHRpb25zLnRoZW1lO1xuICAgIHRoaXMuX2VkaXRvciA9IG1vbmFjby5lZGl0b3IuY3JlYXRlRGlmZkVkaXRvcih0aGlzLl9lZGl0b3JDb250YWluZXIubmF0aXZlRWxlbWVudCwgb3B0aW9ucyk7XG4gICAgb3B0aW9ucy50aGVtZSA9IHRoZW1lO1xuICAgIHRoaXMuX2VkaXRvci5zZXRNb2RlbCh7XG4gICAgICBvcmlnaW5hbDogb3JpZ2luYWxNb2RlbCxcbiAgICAgIG1vZGlmaWVkOiBtb2RpZmllZE1vZGVsXG4gICAgfSk7XG5cbiAgICAvLyByZWZyZXNoIGxheW91dCBvbiByZXNpemUgZXZlbnQuXG4gICAgaWYgKHRoaXMuX3dpbmRvd1Jlc2l6ZVN1YnNjcmlwdGlvbikge1xuICAgICAgdGhpcy5fd2luZG93UmVzaXplU3Vic2NyaXB0aW9uLnVuc3Vic2NyaWJlKCk7XG4gICAgfVxuICAgIHRoaXMuX3dpbmRvd1Jlc2l6ZVN1YnNjcmlwdGlvbiA9IGZyb21FdmVudCh3aW5kb3csICdyZXNpemUnKS5zdWJzY3JpYmUoKCkgPT4gdGhpcy5fZWRpdG9yLmxheW91dCgpKTtcbiAgICB0aGlzLm9uSW5pdC5lbWl0KHRoaXMuX2VkaXRvcik7XG4gIH1cblxufVxuIl19