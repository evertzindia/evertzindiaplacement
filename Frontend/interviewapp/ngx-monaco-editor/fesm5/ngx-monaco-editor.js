import { __extends } from 'tslib';
import { EventEmitter, ViewChild, Output, Input, InjectionToken, Component, forwardRef, NgZone, Inject, NgModule } from '@angular/core';
import { NG_VALUE_ACCESSOR } from '@angular/forms';
import { fromEvent } from 'rxjs';
import { CommonModule } from '@angular/common';

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
/** @type {?} */
var loadedMonaco = false;
/** @type {?} */
var loadPromise;
/**
 * @abstract
 */
var BaseEditor = /** @class */ (function () {
    function BaseEditor(config) {
        this.config = config;
        this.onInit = new EventEmitter();
    }
    Object.defineProperty(BaseEditor.prototype, "options", {
        get: /**
         * @return {?}
         */
        function () {
            return this._options;
        },
        set: /**
         * @param {?} options
         * @return {?}
         */
        function (options) {
            this._options = Object.assign({}, this.config.defaultOptions, options);
            if (this._editor) {
                this._editor.dispose();
                this.initMonaco(options);
            }
        },
        enumerable: true,
        configurable: true
    });
    /**
     * @return {?}
     */
    BaseEditor.prototype.ngAfterViewInit = /**
     * @return {?}
     */
    function () {
        var _this = this;
        if (loadedMonaco) {
            // Wait until monaco editor is available
            loadPromise.then((/**
             * @return {?}
             */
            function () {
                _this.initMonaco(_this.options);
            }));
        }
        else {
            loadedMonaco = true;
            loadPromise = new Promise((/**
             * @param {?} resolve
             * @return {?}
             */
            function (resolve) {
                /** @type {?} */
                var baseUrl = _this.config.baseUrl || './assets';
                if (typeof (((/** @type {?} */ (window))).monaco) === 'object') {
                    resolve();
                    return;
                }
                /** @type {?} */
                var onGotAmdLoader = (/**
                 * @return {?}
                 */
                function () {
                    // Load monaco
                    ((/** @type {?} */ (window))).require.config({ paths: { 'vs': baseUrl + "/monaco/vs" } });
                    ((/** @type {?} */ (window))).require(['vs/editor/editor.main'], (/**
                     * @return {?}
                     */
                    function () {
                        if (typeof _this.config.onMonacoLoad === 'function') {
                            _this.config.onMonacoLoad();
                        }
                        _this.initMonaco(_this.options);
                        resolve();
                    }));
                });
                // Load AMD loader if necessary
                if (!((/** @type {?} */ (window))).require) {
                    /** @type {?} */
                    var loaderScript = document.createElement('script');
                    loaderScript.type = 'text/javascript';
                    loaderScript.src = baseUrl + "/monaco/vs/loader.js";
                    loaderScript.addEventListener('load', onGotAmdLoader);
                    document.body.appendChild(loaderScript);
                }
                else {
                    onGotAmdLoader();
                }
            }));
        }
    };
    /**
     * @return {?}
     */
    BaseEditor.prototype.ngOnDestroy = /**
     * @return {?}
     */
    function () {
        if (this._windowResizeSubscription) {
            this._windowResizeSubscription.unsubscribe();
        }
        if (this._editor) {
            this._editor.dispose();
            this._editor = undefined;
        }
    };
    BaseEditor.propDecorators = {
        _editorContainer: [{ type: ViewChild, args: ['editorContainer', { static: true },] }],
        onInit: [{ type: Output }],
        options: [{ type: Input, args: ['options',] }]
    };
    return BaseEditor;
}());
if (false) {
    /** @type {?} */
    BaseEditor.prototype._editorContainer;
    /** @type {?} */
    BaseEditor.prototype.onInit;
    /**
     * @type {?}
     * @protected
     */
    BaseEditor.prototype._editor;
    /**
     * @type {?}
     * @private
     */
    BaseEditor.prototype._options;
    /**
     * @type {?}
     * @protected
     */
    BaseEditor.prototype._windowResizeSubscription;
    /**
     * @type {?}
     * @private
     */
    BaseEditor.prototype.config;
    /**
     * @abstract
     * @protected
     * @param {?} options
     * @return {?}
     */
    BaseEditor.prototype.initMonaco = function (options) { };
}

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
/** @type {?} */
var NGX_MONACO_EDITOR_CONFIG = new InjectionToken('NGX_MONACO_EDITOR_CONFIG');
/**
 * @record
 */
function NgxMonacoEditorConfig() { }
if (false) {
    /** @type {?|undefined} */
    NgxMonacoEditorConfig.prototype.baseUrl;
    /** @type {?|undefined} */
    NgxMonacoEditorConfig.prototype.defaultOptions;
    /** @type {?|undefined} */
    NgxMonacoEditorConfig.prototype.onMonacoLoad;
}

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
var EditorComponent = /** @class */ (function (_super) {
    __extends(EditorComponent, _super);
    function EditorComponent(zone, editorConfig) {
        var _this = _super.call(this, editorConfig) || this;
        _this.zone = zone;
        _this.editorConfig = editorConfig;
        _this._value = '';
        _this.propagateChange = (/**
         * @param {?} _
         * @return {?}
         */
        function (_) { });
        _this.onTouched = (/**
         * @return {?}
         */
        function () { });
        return _this;
    }
    Object.defineProperty(EditorComponent.prototype, "model", {
        set: /**
         * @param {?} model
         * @return {?}
         */
        function (model) {
            this.options.model = model;
            if (this._editor) {
                this._editor.dispose();
                this.initMonaco(this.options);
            }
        },
        enumerable: true,
        configurable: true
    });
    /**
     * @param {?} value
     * @return {?}
     */
    EditorComponent.prototype.writeValue = /**
     * @param {?} value
     * @return {?}
     */
    function (value) {
        var _this = this;
        this._value = value || '';
        // Fix for value change while dispose in process.
        setTimeout((/**
         * @return {?}
         */
        function () {
            if (_this._editor && !_this.options.model) {
                _this._editor.setValue(_this._value);
            }
        }));
    };
    /**
     * @param {?} fn
     * @return {?}
     */
    EditorComponent.prototype.registerOnChange = /**
     * @param {?} fn
     * @return {?}
     */
    function (fn) {
        this.propagateChange = fn;
    };
    /**
     * @param {?} fn
     * @return {?}
     */
    EditorComponent.prototype.registerOnTouched = /**
     * @param {?} fn
     * @return {?}
     */
    function (fn) {
        this.onTouched = fn;
    };
    /**
     * @protected
     * @param {?} options
     * @return {?}
     */
    EditorComponent.prototype.initMonaco = /**
     * @protected
     * @param {?} options
     * @return {?}
     */
    function (options) {
        var _this = this;
        /** @type {?} */
        var hasModel = !!options.model;
        if (hasModel) {
            /** @type {?} */
            var model = monaco.editor.getModel(options.model.uri || '');
            if (model) {
                options.model = model;
                options.model.setValue(this._value);
            }
            else {
                options.model = monaco.editor.createModel(options.model.value, options.model.language, options.model.uri);
            }
        }
        this._editor = monaco.editor.create(this._editorContainer.nativeElement, options);
        if (!hasModel) {
            this._editor.setValue(this._value);
        }
        this._editor.onDidChangeModelContent((/**
         * @param {?} e
         * @return {?}
         */
        function (e) {
            /** @type {?} */
            var value = _this._editor.getValue();
            _this.propagateChange(value);
            // value is not propagated to parent when executing outside zone.
            _this.zone.run((/**
             * @return {?}
             */
            function () { return _this._value = value; }));
        }));
        this._editor.onDidBlurEditorWidget((/**
         * @return {?}
         */
        function () {
            _this.onTouched();
        }));
        // refresh layout on resize event.
        if (this._windowResizeSubscription) {
            this._windowResizeSubscription.unsubscribe();
        }
        this._windowResizeSubscription = fromEvent(window, 'resize').subscribe((/**
         * @return {?}
         */
        function () { return _this._editor.layout(); }));
        this.onInit.emit(this._editor);
    };
    EditorComponent.decorators = [
        { type: Component, args: [{
                    selector: 'ngx-monaco-editor',
                    template: '<div class="editor-container" #editorContainer></div>',
                    providers: [{
                            provide: NG_VALUE_ACCESSOR,
                            useExisting: forwardRef((/**
                             * @return {?}
                             */
                            function () { return EditorComponent; })),
                            multi: true
                        }],
                    styles: ["\n    :host {\n      display: block;\n      height: 200px;\n    }\n\n    .editor-container {\n      width: 100%;\n      height: 98%;\n    }\n  "]
                }] }
    ];
    /** @nocollapse */
    EditorComponent.ctorParameters = function () { return [
        { type: NgZone },
        { type: undefined, decorators: [{ type: Inject, args: [NGX_MONACO_EDITOR_CONFIG,] }] }
    ]; };
    EditorComponent.propDecorators = {
        model: [{ type: Input, args: ['model',] }]
    };
    return EditorComponent;
}(BaseEditor));
if (false) {
    /**
     * @type {?}
     * @private
     */
    EditorComponent.prototype._value;
    /** @type {?} */
    EditorComponent.prototype.propagateChange;
    /** @type {?} */
    EditorComponent.prototype.onTouched;
    /**
     * @type {?}
     * @private
     */
    EditorComponent.prototype.zone;
    /**
     * @type {?}
     * @private
     */
    EditorComponent.prototype.editorConfig;
}

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
var DiffEditorComponent = /** @class */ (function (_super) {
    __extends(DiffEditorComponent, _super);
    function DiffEditorComponent(editorConfig) {
        var _this = _super.call(this, editorConfig) || this;
        _this.editorConfig = editorConfig;
        return _this;
    }
    Object.defineProperty(DiffEditorComponent.prototype, "originalModel", {
        set: /**
         * @param {?} model
         * @return {?}
         */
        function (model) {
            this._originalModel = model;
            if (this._editor) {
                this._editor.dispose();
                this.initMonaco(this.options);
            }
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(DiffEditorComponent.prototype, "modifiedModel", {
        set: /**
         * @param {?} model
         * @return {?}
         */
        function (model) {
            this._modifiedModel = model;
            if (this._editor) {
                this._editor.dispose();
                this.initMonaco(this.options);
            }
        },
        enumerable: true,
        configurable: true
    });
    /**
     * @protected
     * @param {?} options
     * @return {?}
     */
    DiffEditorComponent.prototype.initMonaco = /**
     * @protected
     * @param {?} options
     * @return {?}
     */
    function (options) {
        var _this = this;
        if (!this._originalModel || !this._modifiedModel) {
            throw new Error('originalModel or modifiedModel not found for ngx-monaco-diff-editor');
        }
        this._originalModel.language = this._originalModel.language || options.language;
        this._modifiedModel.language = this._modifiedModel.language || options.language;
        /** @type {?} */
        var originalModel = monaco.editor.createModel(this._originalModel.code, this._originalModel.language);
        /** @type {?} */
        var modifiedModel = monaco.editor.createModel(this._modifiedModel.code, this._modifiedModel.language);
        this._editorContainer.nativeElement.innerHTML = '';
        /** @type {?} */
        var theme = options.theme;
        this._editor = monaco.editor.createDiffEditor(this._editorContainer.nativeElement, options);
        options.theme = theme;
        this._editor.setModel({
            original: originalModel,
            modified: modifiedModel
        });
        // refresh layout on resize event.
        if (this._windowResizeSubscription) {
            this._windowResizeSubscription.unsubscribe();
        }
        this._windowResizeSubscription = fromEvent(window, 'resize').subscribe((/**
         * @return {?}
         */
        function () { return _this._editor.layout(); }));
        this.onInit.emit(this._editor);
    };
    DiffEditorComponent.decorators = [
        { type: Component, args: [{
                    selector: 'ngx-monaco-diff-editor',
                    template: '<div class="editor-container" #editorContainer></div>',
                    styles: ["\n    :host {\n      display: block;\n      height: 200px;\n    }\n\n    .editor-container {\n      width: 100%;\n      height: 98%;\n    }\n  "]
                }] }
    ];
    /** @nocollapse */
    DiffEditorComponent.ctorParameters = function () { return [
        { type: undefined, decorators: [{ type: Inject, args: [NGX_MONACO_EDITOR_CONFIG,] }] }
    ]; };
    DiffEditorComponent.propDecorators = {
        originalModel: [{ type: Input, args: ['originalModel',] }],
        modifiedModel: [{ type: Input, args: ['modifiedModel',] }]
    };
    return DiffEditorComponent;
}(BaseEditor));
if (false) {
    /** @type {?} */
    DiffEditorComponent.prototype._originalModel;
    /** @type {?} */
    DiffEditorComponent.prototype._modifiedModel;
    /**
     * @type {?}
     * @private
     */
    DiffEditorComponent.prototype.editorConfig;
}

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
var MonacoEditorModule = /** @class */ (function () {
    function MonacoEditorModule() {
    }
    /**
     * @param {?=} config
     * @return {?}
     */
    MonacoEditorModule.forRoot = /**
     * @param {?=} config
     * @return {?}
     */
    function (config) {
        if (config === void 0) { config = {}; }
        return {
            ngModule: MonacoEditorModule,
            providers: [
                { provide: NGX_MONACO_EDITOR_CONFIG, useValue: config }
            ]
        };
    };
    MonacoEditorModule.decorators = [
        { type: NgModule, args: [{
                    imports: [
                        CommonModule
                    ],
                    declarations: [
                        EditorComponent,
                        DiffEditorComponent
                    ],
                    exports: [
                        EditorComponent,
                        DiffEditorComponent
                    ]
                },] }
    ];
    return MonacoEditorModule;
}());

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
/**
 * @record
 */
function DiffEditorModel() { }
if (false) {
    /** @type {?} */
    DiffEditorModel.prototype.code;
    /** @type {?} */
    DiffEditorModel.prototype.language;
}
/**
 * @record
 */
function NgxEditorModel() { }
if (false) {
    /** @type {?} */
    NgxEditorModel.prototype.value;
    /** @type {?|undefined} */
    NgxEditorModel.prototype.language;
    /** @type {?|undefined} */
    NgxEditorModel.prototype.uri;
}

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */

export { EditorComponent, MonacoEditorModule, NGX_MONACO_EDITOR_CONFIG, BaseEditor as ɵa, DiffEditorComponent as ɵb };
//# sourceMappingURL=ngx-monaco-editor.js.map
