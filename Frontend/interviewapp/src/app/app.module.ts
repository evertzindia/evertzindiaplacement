import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { AppComponent } from './app.component';
import { AppRoutingModule } from './app-routing.module';
import { CandidateRegistrationComponent } from './candidate-registration/candidate-registration.component';
import { HttpClientModule } from '@angular/common/http';
import { FormsModule, ReactiveFormsModule }   from '@angular/forms';
import { QuestionsPageComponent } from './questions-page/questions-page.component';
import { SystemConfig } from './ws-calls/system-config';
import { AdminLoginComponent } from './admin-login/admin-login.component';
import {enableProdMode} from '@angular/core';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { AdminControlComponent } from './admin-control/admin-control.component';
import { LayoutModule } from '@angular/cdk/layout';
import { MatToolbarModule, MatButtonModule, MatSidenavModule, MatIconModule, MatListModule } from '@angular/material';
import { ResultsPageComponent } from './results-page/results-page.component';
import { AddQuestionsComponent } from './add-questions/add-questions.component';
import { SplitterModule } from '@syncfusion/ej2-angular-layouts';
import { MonacoEditorModule } from 'ngx-monaco-editor';
import { CodingTestComponent } from './coding-test/coding-test.component';
import { LoadingBarModule } from '@ngx-loading-bar/core';
import * as $ from 'jquery';
import * as bootstrap from 'bootstrap';
import { CountdownModule } from 'ngx-countdown';
import { AdminConfigAdditionComponent } from './admin-config-addition/admin-config-addition.component';
import { AdminResetTestComponent } from './admin-reset-test/admin-reset-test.component';
import { InfoPageComponent } from './info-page/info-page.component';
import { NoRightClickDirective } from './no-right-click.directive';


enableProdMode();

@NgModule({
  declarations: [
    AppComponent,
    CandidateRegistrationComponent,
    QuestionsPageComponent,
    AdminLoginComponent,
    AdminControlComponent,
    ResultsPageComponent,
    AddQuestionsComponent,
    CodingTestComponent,
    AdminConfigAdditionComponent,
    AdminResetTestComponent,
    InfoPageComponent,
    NoRightClickDirective

  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    FormsModule,
    ReactiveFormsModule,
    BrowserAnimationsModule,
    LayoutModule,
    MatToolbarModule,
    MatButtonModule,
    MatSidenavModule,
    MatIconModule,
    MatListModule,
    SplitterModule,
    LoadingBarModule,
    CountdownModule,
    MonacoEditorModule.forRoot()
  ],
  providers: [SystemConfig],
  bootstrap: [AppComponent]
})
export class AppModule { }

