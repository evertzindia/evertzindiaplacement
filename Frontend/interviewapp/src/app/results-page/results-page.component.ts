import { Component,ViewChild, ElementRef } from '@angular/core';
import { BreakpointObserver, Breakpoints } from '@angular/cdk/layout';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { HttpResponse, HttpErrorResponse } from '@angular/common/http';
import { SystemConfig } from '../ws-calls/system-config';
import { PostToServerService } from '../post-to-server.service';
import * as XLSX from 'xlsx';


@Component({
  selector: 'app-results-page',
  templateUrl: './results-page.component.html',
  styleUrls: ['./results-page.component.css']
})
export class ResultsPageComponent {

  @ViewChild('table') table: ElementRef;
 

  public serverResponse = "";
  public requestList = "";
  public collegeList = [];
  public resultList = [];
  public moduleList = [];
  public sortByOption = "";
  public isCountAvailable = 0;
  public sortedCutOffResult = [];
  public sortOrderOption = "DSC";
  public sortedResult = [];
  public collegeChoosen = "";
  public cutOff: number = null;
  public serverStatus_Error_Class = "";
  public serverStatus_Error_Box = "";
  public serverStatus_Error_Message = "";
  public serverStatus_Success_Class = "";
  public serverStatus_Success_Message = "";

  
  isHandset$: Observable<boolean> = this.breakpointObserver.observe(Breakpoints.Handset)
    .pipe(
      map(result => result.matches)
    );

    ngOnInit(){

      this.requestList = "College";
      this.AdminGetAll(this.requestList);
      this.AdminGetMandatoryModules("Module");

    }

    ExportToExcel()
    {
      console.log(this.table.nativeElement);
      const ws: XLSX.WorkSheet=XLSX.utils.table_to_sheet(this.table.nativeElement);
      const wb: XLSX.WorkBook = XLSX.utils.book_new();
      XLSX.utils.book_append_sheet(wb, ws, 'Sheet1');

      /* save to file */
      XLSX.writeFile(wb, 'interviewResults.xlsx');

    }
    

  constructor(private breakpointObserver: BreakpointObserver, private postToServerObject: PostToServerService, private systemConfig: SystemConfig) {}

   
  getResult(){

    this.resultList = [];
    this.sortedCutOffResult = [];
    //this.resultList = this.systemConfig.questionsList["EvertzInterviewApp"]["ParameterList"];
    this.AdminGenerateResult(this.collegeChoosen);
        
  }

  bindResult(module,scoreList){
    return this.resultList[scoreList]["Score"][module];
  }

  selectedCollege(event: any){
    this.collegeChoosen = event.target.value;
  }

  sortBy(event: any){
    this.sortByOption = event.target.value;
  }

  sortOrder(event: any){
    this.sortOrderOption = event.target.value;
  }

  sortResult(){

    if(this.sortByOption == ""){
      //add error CSS
      console.log("Please enter a valid option");
    }
    else{
      if(this.sortOrderOption == "DSC"){
        this.sortResultsDSC(this.sortByOption);
      }else{
        this.sortResultsASC(this.sortByOption);
      }
      
    }

  }

  sortResultsDSC(sortByOption){

    for(var i=0; i<this.resultList.length; i++){
      //console.log(this.resultList[i]);
      for(var j=i+1; j<this.resultList.length; j++){
          if( parseInt( this.resultList[i]["Score"][sortByOption]) < parseInt (this.resultList[j]["Score"][sortByOption]) ){
            var temp = this.resultList[i];
            this.resultList[i] = this.resultList[j];
            this.resultList[j] = temp;
          }
      }
    }
    this.isCountAvailable = this.resultList.length;
  }

  sortResultsASC(sortByOption){

    for(var i=0; i<this.resultList.length; i++){
      //console.log(this.resultList[i]);
      for(var j=i+1; j<this.resultList.length; j++){
          if( parseInt( this.resultList[i]["Score"][sortByOption]) > parseInt (this.resultList[j]["Score"][sortByOption]) ){
            var temp = this.resultList[i];
            this.resultList[i] = this.resultList[j];
            this.resultList[j] = temp;
          }
      }
    }
    this.isCountAvailable = this.resultList.length;
  }

  public AdminGetAll(requestList) {

    var wsCallStatus = "";
    this.postToServerObject.AdminGetAll(requestList).subscribe((res: HttpResponse<any>)=> {
    this.serverResponse = JSON.parse(JSON.stringify(res.body));
    console.log(this.serverResponse);
    wsCallStatus = this.serverResponse["EvertzInterviewApp"]["Success"];
  
    if(!wsCallStatus){
      this.serverStatus_Error_Class = "isa_error";
      this.serverStatus_Error_Box = "fa fa-times-circle";
      this.serverStatus_Error_Message = this.serverResponse["EvertzInterviewApp"]["Reason"]
      window.scrollTo(0, 0);
      }
      else{
        this.collegeList = this.serverResponse["EvertzInterviewApp"]["ParameterList"]["CollegeList"];
        console.log("College List Loaded Sucessfully...!");
        
      }
      
  },
    (err: HttpErrorResponse) => { 
  
      if(err.status == 0) {
        this.serverStatus_Error_Class = "isa_error";
        this.serverStatus_Error_Box = "fa fa-times-circle";
        this.serverStatus_Error_Message = "ERROR connecting to server."
        window.scrollTo(0, 0);
  
      }
      else {
        console.log("Sucessfully Updated: College List");
      }
  
    }
  
  );
  
  };

  public AdminGetMandatoryModules(requestList) {

    var wsCallStatus = "";
    this.postToServerObject.AdminGetMandatoryModules(requestList).subscribe((res: HttpResponse<any>)=> {
    this.serverResponse = JSON.parse(JSON.stringify(res.body));
    console.log(this.serverResponse);
    wsCallStatus = this.serverResponse["EvertzInterviewApp"]["Success"];
  
    if(!wsCallStatus){
      this.serverStatus_Error_Class = "isa_error";
      this.serverStatus_Error_Box = "fa fa-times-circle";
      this.serverStatus_Error_Message = this.serverResponse["EvertzInterviewApp"]["Reason"]
      window.scrollTo(0, 0);
      }
      else{
        var response = this.serverResponse["EvertzInterviewApp"]["ParameterList"]["ModuleList"];
        for (let res in response){
          this.moduleList.push(response[res]["moduleName"]);
        }
        console.log("ModuleList Loaded Sucessfully...!");
        
      }
      
  },
    (err: HttpErrorResponse) => { 
  
      if(err.status == 0) {
        this.serverStatus_Error_Class = "isa_error";
        this.serverStatus_Error_Box = "fa fa-times-circle";
        this.serverStatus_Error_Message = "ERROR connecting to server."
        window.scrollTo(0, 0);
  
      }
      else {
        console.log("Sucessfully Updated: ModuleList");
      }
  
    }
  
  );
  
  };

  public AdminGenerateResult(requestList) {

    var wsCallStatus = "";
    this.postToServerObject.AdminGenerateResult(requestList).subscribe((res: HttpResponse<any>)=> {
    this.serverResponse = JSON.parse(JSON.stringify(res.body));
    console.log(this.serverResponse);
    wsCallStatus = this.serverResponse["EvertzInterviewApp"]["Success"];
  
    if(!wsCallStatus){
      this.serverStatus_Error_Class = "isa_error";
      this.serverStatus_Error_Box = "fa fa-times-circle";
      this.serverStatus_Error_Message = "Server Unable to Get Results, Please contact Your Supervisor."
      window.scrollTo(0, 0);
      }
      else{
        //this.collegeList = this.serverResponse["EvertzInterviewApp"]["ParameterList"]["CollegeList"];
        console.log("Results Updated sucessfully...");
        this.resultList = this.serverResponse["EvertzInterviewApp"]["ParameterList"];
        console.log(this.serverResponse["EvertzInterviewApp"]["ParameterList"]);
        if(this.cutOff != null){

          for(let score in this.resultList){
            if(parseInt(this.resultList[score]["Score"]["Total"]) >= this.cutOff){
              this.sortedCutOffResult.push(this.resultList[score]);
            }
          }
          this.resultList = [];
          for(let sortedCutOff in this.sortedCutOffResult){
            this.resultList.push(this.sortedCutOffResult[sortedCutOff]);
          }
          
        }
        this.isCountAvailable = this.resultList.length;
      }
      
  },
    (err: HttpErrorResponse) => { 
  
      if(err.status == 0) {
        this.serverStatus_Error_Class = "isa_error";
        this.serverStatus_Error_Box = "fa fa-times-circle";
        this.serverStatus_Error_Message = "ERROR connecting to server."
        window.scrollTo(0, 0);
  
      }
      else {
        console.log("Sucessfully Updated: Result List");
      }
  
    }
  
  );
  
  };

}
