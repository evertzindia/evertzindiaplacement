import { Component } from '@angular/core';
import { PostToServerService } from '../post-to-server.service';
import { BreakpointObserver, Breakpoints } from '@angular/cdk/layout';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { HttpResponse, HttpErrorResponse } from '@angular/common/http';
import { SystemConfig } from '../ws-calls/system-config';

@Component({
  selector: 'app-admin-config-addition',
  templateUrl: './admin-config-addition.component.html',
  styleUrls: ['./admin-config-addition.component.css']
})
export class AdminConfigAdditionComponent {

  public collegeName: string;
  public degree: string;
  public stream: string;
  public branch: string;
  public userName: string = "";
  public password: string = "";
  public College: any;
  public confirmpassword: string = "";
  public oldpassword: string = "";
  public newpassword: string = "";
  public userName1:string = "";
  public submitted = false;
  public serverResponse = "";
  public requestList = "";
  public serverStatus_Error_Class = "";
  public serverStatus_Error_Box = "";
  public serverStatus_Error_Message = "";
  public serverStatus_Success_Class = "";
  public serverStatus_Success_Message = "";
  public selectedcollege :string="";
  seconds: number = this.systemConfig.errorResetTime;
  interval;
  

  isHandset$: Observable<boolean> = this.breakpointObserver.observe(Breakpoints.Handset)
    .pipe(
      map(result => result.matches)
    );


  constructor(private breakpointObserver: BreakpointObserver,private postToServerObject: PostToServerService, private systemConfig: SystemConfig) {}

  ngOnInit(){
    this.startTimer();
    this.requestList = "College";
    this.AdminGetAll(this.requestList);
  }

  startTimer() {
    this.interval = setInterval(() => {
      if(this.seconds > 0) {
        this.seconds--;
      } else {
        this.seconds = this.systemConfig.errorResetTime;
        this.serverStatus_Error_Class = "";
        this.serverStatus_Error_Box = "";
        this.serverStatus_Error_Message = "";
        this.serverStatus_Success_Class = "";
        this.serverStatus_Success_Message = "";
      }
    },1000)
  }

  addCollege()
  {
    console.log("adding college " + this.collegeName);
    this.requestList = "College";
    this.degree = "";
    this.branch = "";
    this.AddDropDownList(this.requestList,this.collegeName);
  }

  addDegree()
  {
    console.log(this.degree +" Degree added")
    this.collegeName = "";
    this.branch = "";
    this.requestList = "Degree";
    this.AddDropDownList(this.requestList,this.degree);
  }
  selectCollege(event: any) {
    this.selectedcollege = event.target.value;
    
   }

   setCollege()
   {
     console.log(this.selectedcollege);
    this.AdminSetCollege(this.selectedcollege)
   }

   
  changePassword()
  {
    console.log(this.oldpassword +" "+this.newpassword+" "+this.userName1);
    if(this.oldpassword != "" && this.newpassword!= "" && this.userName1!= ""){
      this.adminChangePassword(this.userName1,this.newpassword,this.oldpassword);
    }else{
      this.serverStatus_Error_Class = "isa_error";
      this.serverStatus_Error_Box = "fa fa-times-circle";
      this.serverStatus_Error_Message = "One of the required field is empty";
    }
    this.userName1 = "";
    this.oldpassword = "";
    this.newpassword = "";
  }
  addBranch()
  {
    console.log(this.branch +" stream added")
    this.collegeName = "";
    this.degree = "";
    this.requestList = "Branch";
    this.AddDropDownList(this.requestList,this.branch);
  }

  addUser()
  {
    console.log(this.userName);
    console.log(this.password,this.confirmpassword);
    //this.adminAddUser(this.userName,this.password);
    if(this.password.length < 6){
      this.serverStatus_Error_Class = "isa_error";
      this.serverStatus_Error_Box = "fa fa-times-circle";
      this.serverStatus_Error_Message = "Password must have 6 charecters";
    }else{
        if(this.password != this.confirmpassword)
        {
          this.serverStatus_Error_Class = "isa_error";
          this.serverStatus_Error_Box = "fa fa-times-circle";
          this.serverStatus_Error_Message = "Can't confirm your password, Please provide a proper passowrd.";
          console.log("password incorrect");
        }
        else
      {
        this.adminAddUser(this.userName,this.password);
        console.log("user added");
      }
    }
  }

  public AdminGetAll(requestList) {

    var wsCallStatus = "";
    this.postToServerObject.AdminGetAll(requestList).subscribe((res: HttpResponse<any>)=> {
    this.serverResponse = JSON.parse(JSON.stringify(res.body));
    console.log(this.serverResponse);
    wsCallStatus = this.serverResponse["EvertzInterviewApp"]["Success"];
  
    if(!wsCallStatus){
      this.serverStatus_Error_Class = "isa_error";
      this.serverStatus_Error_Box = "fa fa-times-circle";
      this.serverStatus_Error_Message = "Server Unable to load "+ requestList +" list, Please contact Your Supervisor."
      window.scrollTo(0, 0);
      }
      else{
        this.College = this.serverResponse["EvertzInterviewApp"]["ParameterList"]["CollegeList"];
        console.log("College List Loaded Sucessfully...!");
      }
      
  },
    (err: HttpErrorResponse) => { 
  
      if(err.status == 0) {
        this.serverStatus_Error_Class = "isa_error";
        this.serverStatus_Error_Box = "fa fa-times-circle";
        this.serverStatus_Error_Message = "Server Unable to load "+ requestList +" list, Please contact Your Supervisor."
        window.scrollTo(0, 0);
  
      }
      else {
        console.log("Sucessfully Updated: "+ Request + "List");
      }
  
    }
  
  );
  
  };

  public AdminSetCollege(collegeId) {

    var wsCallStatus = "";
    this.postToServerObject.AdminSetCollege(collegeId).subscribe((res: HttpResponse<any>)=> {
    this.serverResponse = JSON.parse(JSON.stringify(res.body));
    console.log(this.serverResponse);
    wsCallStatus = this.serverResponse["EvertzInterviewApp"]["Success"];
  
    if(!wsCallStatus){
      this.serverStatus_Error_Class = "isa_error";
      this.serverStatus_Error_Box = "fa fa-times-circle";
      this.serverStatus_Error_Message = "Server Unable to set College, Please contact Your Supervisor."
      window.scrollTo(0, 0);
      }
      else{
        this.serverStatus_Success_Class = "alert alert-success";
        this.serverStatus_Success_Message = "College has been set Sucessfully...!";
        console.log("College has been set Sucessfully...!");
      }
      
  },
    (err: HttpErrorResponse) => { 
  
      if(err.status == 0) {
        this.serverStatus_Error_Class = "isa_error";
        this.serverStatus_Error_Box = "fa fa-times-circle";
        this.serverStatus_Error_Message = "Server Unable to load "+ collegeId +" list, Please contact Your Supervisor."
        window.scrollTo(0, 0);
  
      }
      else {
        console.log("Sucessfully Updated: "+ Request + "List");
      }
  
    }
  
  );
  
  };
  
  public AddDropDownList(requestList,data) {

    var wsCallStatus = "";
    this.postToServerObject.AddDropDownList(requestList,data).subscribe((res: HttpResponse<any>)=> {
    this.serverResponse = JSON.parse(JSON.stringify(res.body));
    wsCallStatus = this.serverResponse["EvertzInterviewApp"]["Success"];

    if(wsCallStatus){    

      //success CSS
      console.log("list added");
      this.serverStatus_Success_Class = "alert alert-success";
      this.serverStatus_Success_Message = data + " - " + requestList + " Added Sucessfully.";

    }
    else{

      this.serverStatus_Error_Class = "isa_error";
      this.serverStatus_Error_Box = "fa fa-times-circle";
      this.serverStatus_Error_Message = requestList + " - " + data +" Already exists, Please add with different name.";
      window.scrollTo(0, 0);
      window.stop();

    } 
    
  },
    (err: HttpErrorResponse) => { 

      if(err.status == 0) {
        this.serverStatus_Error_Class = "isa_error";
        this.serverStatus_Error_Box = "fa fa-times-circle";
        this.serverStatus_Error_Message = "Server Unable to Add"+ requestList +" list, Please contact Your Administrator."
        window.scrollTo(0, 0);
        window.stop();
      }
      else {
        console.log("Sucessfully Updated: "+ Request + "List");
      }

    }

   );
  }

  public adminAddUser(userName,password) {

    var wsCallStatus = "";
    this.postToServerObject.adminAddUser(userName,password).subscribe((res: HttpResponse<any>)=> {
    this.serverResponse = JSON.parse(JSON.stringify(res.body));
    console.log(this.serverResponse);
    wsCallStatus = this.serverResponse["EvertzInterviewApp"]["Success"];
    if(!wsCallStatus){
      window.scrollTo(0, 0);
      this.serverStatus_Error_Class = "isa_error";
      this.serverStatus_Error_Box = "fa fa-times-circle";
      this.serverStatus_Error_Message = this.serverResponse["EvertzInterviewApp"]["ParameterList"]["Status"];
       
    }else{
      //success CSS
      this.serverStatus_Success_Class = "alert alert-success";
      this.serverStatus_Success_Message = "User " + userName + " Added Sucessfully.";

    }
        
  },
    (err: HttpErrorResponse) => { 

      if(err.status == 0) {
        this.serverStatus_Error_Class = "isa_error";
        this.serverStatus_Error_Box = "fa fa-times-circle";
        this.serverStatus_Error_Message = "Failed to login."
        window.scrollTo(0, 0);
      }
      else {
        console.log("Sucessfully Updated: "+ Request + "List");
      }

    }

  );

};

public adminChangePassword(userName,newpassword,oldpassword) {

  var wsCallStatus = "";
  this.postToServerObject.adminChangePassword(userName,newpassword,oldpassword).subscribe((res: HttpResponse<any>)=> {
  this.serverResponse = JSON.parse(JSON.stringify(res.body));
  console.log(this.serverResponse);
  wsCallStatus = this.serverResponse["EvertzInterviewApp"]["Success"];
  if(!wsCallStatus){
    window.scrollTo(0, 0);
    this.serverStatus_Error_Class = "isa_error";
    this.serverStatus_Error_Box = "fa fa-times-circle";
    this.serverStatus_Error_Message = this.serverResponse["EvertzInterviewApp"]["ParameterList"]["Status"];
     
  }else{
    //success CSS
    this.serverStatus_Success_Class = "alert alert-success";
    this.serverStatus_Success_Message = "User " + userName + "Password Changed Sucessfully.";

  }
      
},
  (err: HttpErrorResponse) => { 

    if(err.status == 0) {
      this.serverStatus_Error_Class = "isa_error";
      this.serverStatus_Error_Box = "fa fa-times-circle";
      this.serverStatus_Error_Message = "Failed to login."
      window.scrollTo(0, 0);
    }
    else {
      console.log("Sucessfully Updated: "+ Request + "List");
    }

  }

);

};


}
