import { Component, OnInit } from '@angular/core';
import {ActivatedRoute, Router, NavigationExtras} from "@angular/router";
import { SystemConfig } from '../ws-calls/system-config';

@Component({
  selector: 'app-info-page',
  templateUrl: './info-page.component.html',
  styleUrls: ['./info-page.component.css']
})
export class InfoPageComponent implements OnInit {

  public registerNumber: string;
  public status: number;
  public message: string;
  public status1: string;
  public status2: string;
  public event: string;
  

  constructor(private route: ActivatedRoute,private systemConfig: SystemConfig) {
      this.route.queryParams.subscribe(params => {
        this.registerNumber = params["registerNumber"];
        this.status = params["status"]; 
        this.event =  params["event"];          
    });
   }

  ngOnInit() {

    if(this.status == 1){
      this.message = this.systemConfig.mcqTestStatus;
      this.status1 = this.message;
    }else if(this.status == 2){
      this.message = this.systemConfig.codingTestStatus;
      this.status1 = this.message;
    }else if(this.status == 3){
      this.message = this.systemConfig.testStatus;
      this.status2 = this.message;
    }else if(this.status == 4){
      if(this.event == "Tab"){
      this.message = "Browser window reloaded. Test saved. You may leave the hall.";
      this.status1 = this.event + this.message
      }else{
      this.message = this.systemConfig.malpractise;
      this.status1 = this.event + this.message
      }
    }else if(this.status == 5){
      this.message = this.systemConfig.serverTimedOut;
      this.status1 = this.message;
    }
    

  }


}
