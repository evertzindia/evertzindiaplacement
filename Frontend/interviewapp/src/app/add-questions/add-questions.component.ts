import { Component } from '@angular/core';
import { BreakpointObserver, Breakpoints } from '@angular/cdk/layout';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { FormGroup,  FormBuilder,  Validators } from '@angular/forms';
import { PostToServerService } from '../post-to-server.service';
import { HttpErrorResponse, HttpResponse } from '@angular/common/http';
import { SystemConfig } from '../ws-calls/system-config';

@Component({
  selector: 'app-add-questions',
  templateUrl: './add-questions.component.html',
  styleUrls: ['./add-questions.component.css']
})
export class AddQuestionsComponent {

  title = 'ADDQUESTION';
  public Module = [];
  public moduleName = "";
  public Answer :any = ["1","2"];
  public buttonName:any = 'addQuestion';
  public Expand:boolean = false;
  public buttonName1:any = '+';
  public serverResponse = "";
  public requestList = "";
  public QuestionOptions :any;
  public serverStatus_Error_Class = "";
  public serverStatus_Error_Box = "";
  public serverStatus_Error_Message = "";
  public serverStatus_Success_Class = "";
  public serverStatus_Success_Message = "";
  
  seconds: number = this.systemConfig.errorResetTime;
  interval;
  Option1: string = "";
  Option2: string = "";
  Option3: string = "";
  Option4: string = "";
  Questiondata: string = "";
  AnswerValue: string;
  selectedAnswer: string = "";
  selectedModuleId: string = "";

  isHandset$: Observable<boolean> = this.breakpointObserver.observe(Breakpoints.Handset)
    .pipe(
      map(result => result.matches)
    );

    ngOnInit(){
        //updating Modules List
        this.requestList = "Module";
        this.AdminGetAll(this.requestList);
        this.startTimer();
    }
  constructor(private breakpointObserver: BreakpointObserver,private postToServerObject: PostToServerService, private systemConfig: SystemConfig) {}

  toggle() {
    
    this.Expand = !this.Expand;
    
    // CHANGE THE NAME OF THE BUTTON.
    if(this.Expand)  
    {
      this.buttonName1 = "-";
      this.Answer.push("3","4")
      console.log(this.Answer);
    }
    else
     {
      this.Answer.splice(2,2);
      this.buttonName1 = "+";
      
      console.log(this.Answer)
     }
     
  }


  mapModuleName(){
    for(let modId in this.Module){
      if(this.Module[modId]["Id"] == this.selectedModuleId){
        this.moduleName = this.Module[modId]["Name"];
        break;
      }
    }
  }

  startTimer() {
    this.interval = setInterval(() => {
      if(this.seconds > 0) {
        this.seconds--;
      } else {
        this.seconds = this.systemConfig.errorResetTime;
        this.serverStatus_Error_Class = "";
        this.serverStatus_Error_Box = "";
        this.serverStatus_Error_Message = "";
        this.serverStatus_Success_Class = "";
        this.serverStatus_Success_Message = "";
      }
    },1000)
  }

  addQuestion(event: Event) {
    //console.log("Click!", event)
    console.log(this.Questiondata,this.Option1,this.Option2,this.Option3,this.Option4,this.selectedModuleId,this.selectedAnswer);
    if(this.Questiondata == "" || this.Option1 == "" || this.Option2 == ""){

      console.log("Question or Question option (2) is null");

    }else if(this.selectedModuleId == "" || this.selectedAnswer == ""){

      console.log("Module or Answer not seleced");
      //console.log(this.buttonName1);

    }
    if(this.buttonName1 == "-")
    {
      if(this.buttonName1 === "-" && this.Questiondata == "" || this.Option1 == "" || this.Option2 == "" || this.Option3== "" || this.Option4 == "" ||this.selectedModuleId == "" || this.selectedAnswer == ""){

        
        console.log("Reruired field is empty");
        this.serverStatus_Error_Class = "isa_error";
        this.serverStatus_Error_Box = "fa fa-times-circle";
        this.serverStatus_Error_Message = "One of the Required field is empty."
            
      }
      else
      {
        //call api to add 4 questions
        console.log("API for adding 4 questions");
        var parameters = {
          "QuestionOptions": [
            {
              "OptionId": 1,
              "OptionValue": this.Option1
            },
            {
              "OptionId": 2,
              "OptionValue": this.Option2
            },
            {
              "OptionId": 3,
              "OptionValue": this.Option3
            },
            {
              "OptionId": 4,
              "OptionValue": this.Option4
            }
          ]
        }

        this.QuestionOptions = parameters["QuestionOptions"];
        this.adminAddQuestions(this.Questiondata,this.selectedModuleId,this.selectedAnswer,this.QuestionOptions);
      }
    }
      if(this.buttonName1 == "+")
      {
        if(this.Questiondata != "" && this.Option1 != "" && this.Option2 != "" && this.selectedModuleId != "" && this.selectedAnswer != ""){

          //API call for add Questions
          console.log("API for adding 2 questions");
          var parameters = {
            "QuestionOptions": [
              {
                "OptionId": 1,
                "OptionValue": this.Option1
              },
              {
                "OptionId": 2,
                "OptionValue": this.Option2
              }
            ]
          }
  
          this.QuestionOptions = parameters["QuestionOptions"];
          this.adminAddQuestions(this.Questiondata,this.selectedModuleId,this.selectedAnswer,this.QuestionOptions);
        }
      }
    }

  

  
  
  selectChangeHandler (event: any) {
    //console.log(event);
    this.selectedModuleId = event.target.value;
    this.mapModuleName();
  }


  
  selectAnswerHandler(event: any) {
    this.selectedAnswer = event.target.value;
  }

  public AdminGetAll(requestList) {

    var wsCallStatus = "";
    this.postToServerObject.AdminGetAll(requestList).subscribe((res: HttpResponse<any>)=> {
    this.serverResponse = JSON.parse(JSON.stringify(res.body));
    console.log(this.serverResponse);
    wsCallStatus = this.serverResponse["EvertzInterviewApp"]["Success"];
  
    if(!wsCallStatus){
      this.serverStatus_Error_Class = "isa_error";
      this.serverStatus_Error_Box = "fa fa-times-circle";
      this.serverStatus_Error_Message = "Server Unable to load "+ requestList +" list, Please contact Your Supervisor."
      window.scrollTo(0, 0);
      }
      else{
        this.Module = this.serverResponse["EvertzInterviewApp"]["ParameterList"]["ModuleList"];
        console.log("Modules List Loaded Sucessfully...!");
      }
      
  },
    (err: HttpErrorResponse) => { 
  
      if(err.status == 0) {
        this.serverStatus_Error_Class = "isa_error";
        this.serverStatus_Error_Box = "fa fa-times-circle";
        this.serverStatus_Error_Message = "Server Unable to load "+ requestList +" list, Please contact Your Supervisor."
        window.scrollTo(0, 0);
  
      }
      else {
        console.log("Sucessfully Updated: "+ Request + "List");
      }
  
    }
  
  );
  
  };

  public adminAddQuestions(question,moduleId,answerId,options) {

    var wsCallStatus = "";
    this.postToServerObject.adminAddQuestion(question,moduleId,answerId,options).subscribe((res: HttpResponse<any>)=> {
    this.serverResponse = JSON.parse(JSON.stringify(res.body));
    console.log(this.serverResponse);
    wsCallStatus = this.serverResponse["EvertzInterviewApp"]["Success"];
  
    if(!wsCallStatus){
      this.serverStatus_Error_Class = "isa_error";
      this.serverStatus_Error_Box = "fa fa-times-circle";
      this.serverStatus_Error_Message = "Server Unable to add Question to DB, Please contact Your Administrator."
      window.scrollTo(0, 0);
      }
      else{
        
        console.log("Question Added Sucessfully...");
        this.serverStatus_Success_Class = "alert alert-success";
        this.serverStatus_Success_Message = this.moduleName+" Question Added to DB Sucessfully.";

      }
      
  },
    (err: HttpErrorResponse) => { 
  
      if(err.status == 0) {
        this.serverStatus_Error_Class = "isa_error";
        this.serverStatus_Error_Box = "fa fa-times-circle";
        this.serverStatus_Error_Message = "Server Unable to add Question to DB, Please contact Your Administrator."
        window.scrollTo(0, 0);
  
      }
      else {
        console.log("Question Added Sucessfully...");
      }
  
    }
  
  );
  
  };

}
