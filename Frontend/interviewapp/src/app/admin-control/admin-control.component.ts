import { Component } from '@angular/core';
import { BreakpointObserver, Breakpoints } from '@angular/cdk/layout';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { Router, ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-admin-control',
  templateUrl: './admin-control.component.html',
  styleUrls: ['./admin-control.component.css']
})
export class AdminControlComponent {

  isHandset$: Observable<boolean> = this.breakpointObserver.observe(Breakpoints.Handset)
    .pipe(
      map(result => result.matches)
    );

  constructor(private breakpointObserver: BreakpointObserver, private router: Router, public activatedRoute: ActivatedRoute) {}
  addQuestion(){

    this.router.navigate(['/addquestion']);
  }

  resultsPage(){

    this.router.navigate(['/results']);

  }

  resetPage(){

    this.router.navigate(['/reset']);

  }

  configPage(){

    this.router.navigate(['/config']);

  }
}
