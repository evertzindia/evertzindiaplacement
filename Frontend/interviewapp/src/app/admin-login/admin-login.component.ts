import { Component, OnInit } from '@angular/core';
import { PostToServerService } from '../post-to-server.service';
import { HttpErrorResponse, HttpResponse } from '@angular/common/http';

@Component({
  selector: 'app-admin-login',
  templateUrl: './admin-login.component.html',
  styleUrls: ['./admin-login.component.css']
})
export class AdminLoginComponent implements OnInit {

  public serverResponse;
  public serverStatus_Error_Class = "";
  public serverStatus_Error_Box = "";
  public serverStatus_Error_Message = "";
  constructor(private postToServerObject: PostToServerService) { }

  ngOnInit() {
    
  }
  public adminLogin(userName,password) {

    var wsCallStatus = "";
    this.postToServerObject.adminLogin(userName,password).subscribe((res: HttpResponse<any>)=> {
    this.serverResponse = JSON.parse(JSON.stringify(res.body));
    console.log(this.serverResponse);
    wsCallStatus = this.serverResponse["EvertzInterviewApp"]["Success"];
    if(!wsCallStatus){
      window.scrollTo(0, 0);
      this.serverStatus_Error_Class = "isa_error";
      this.serverStatus_Error_Box = "fa fa-times-circle";
      this.serverStatus_Error_Message = "Failed to login."
       
    }
    
    
  },
    (err: HttpErrorResponse) => { 

      if(err.status == 0) {
        this.serverStatus_Error_Class = "isa_error";
        this.serverStatus_Error_Box = "fa fa-times-circle";
        this.serverStatus_Error_Message = "Failed to login."
        window.scrollTo(0, 0);
      }
      else {
        console.log("Sucessfully Updated: "+ Request + "List");
      }

    }

  );

};
}
