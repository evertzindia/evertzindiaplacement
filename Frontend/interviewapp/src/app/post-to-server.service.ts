import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpErrorResponse,HttpResponse } from '@angular/common/http';
import { CheckServer } from './ws-calls/check-server';
import { GetUIList } from './ws-calls/get-ui-list';
import { SystemConfig } from './ws-calls/system-config';
import { RegisterStudentDetails } from './ws-calls/save-student-details';
import { CandidateQuestions } from './ws-calls/candidate-questions';
import { CandidateSaveAnswer } from './ws-calls/candidate-save-answer';
import { CandidateCodingQuestions } from './ws-calls/candidate-coding';
import { Observable } from 'rxjs';
import { AdminResetTest } from './ws-calls/admin-reset-test';
import { AdminLogin } from './ws-calls/admin-login';
import{ CandidateTestStatus } from './ws-calls/candidate-test-status';
import{ AddUIList } from './ws-calls/add-ui-list';
import{ AdminAddQuestions } from './ws-calls/admin-add-questions';
import{ SetCollege } from './ws-calls/admin-set-college';


@Injectable({
  providedIn: 'root'
})

export class PostToServerService {
  //Constructor to initialize data
  constructor(private httpClientObject: HttpClient, private systemConfig: SystemConfig) {
  }
  
  //URL for WS-Call
  public serverIpAddress = this.systemConfig.serverIp;
  private urlWithEndPoint = "http://" + this.serverIpAddress + ":8080/api";

  //Description: API Call Structure to validate server
  //Usage: To get the server ip and send to backend server for validation and get the response
  //Arg's: Takes server ip as arguement
  //Return's: Server response corresponding to the ValidateServer WS-Call
  checkServer(serverIp: CheckServer): Observable<HttpResponse<CheckServer>> {
    console.log();
    var parameters = {
      EvertzInterviewApp: {
        Subsystem: "Server",
        Command: "ValidateServer",
        ParameterList: {
                "ServerIp": serverIp
            }
        }
    }

    return this.httpClientObject.post<CheckServer>(`${this.urlWithEndPoint}`, parameters,{
      headers: new HttpHeaders({
        "Content-Type": "application/json",
        "Authentication": "frontendui|frontendui"
     }),
		  observe: 'response'
    });
    
  }

  //Description: API Call Structure to Validate and save Student Details
  //Usage: To get all mandatory field data from HTML and send to backend server for saving the student details into the DB
  //Arg's: Takes RegisterNumber, StudentName, DOB, Gender, College, Degree, Branch, YearOfPassing, CGPA, Email, Mobile, LanguageSelected as arguement
  //Return's: Server response corresponding to the StudentRegistration WS-Call
  saveStudentDetails(registerNumber: RegisterStudentDetails, studentName: RegisterStudentDetails, dateOfBirth: RegisterStudentDetails, gender: RegisterStudentDetails, college: RegisterStudentDetails, degree:RegisterStudentDetails,
    branch: RegisterStudentDetails, yearOfPassing: RegisterStudentDetails, CGPA: RegisterStudentDetails, email: RegisterStudentDetails, mobile: RegisterStudentDetails, languageSelected: RegisterStudentDetails): Observable<HttpResponse<RegisterStudentDetails>> {

      var parameters = {
        EvertzInterviewApp: {
          Subsystem: "Candidate",
          Command: "Save",
          ParameterList:{
            "RegisterNumber": registerNumber,
            "StudentName": studentName,
            "DOB": dateOfBirth,
            "Gender": gender,
            "College": college,
            "Degree": degree,
            "Stream": branch,
            "YearOfPassing": yearOfPassing,
            "CGPA": CGPA,
            "Email": email,
            "Mobile": mobile,
            "LanguageSelected": languageSelected
          }
        }
      }

      return this.httpClientObject.post<RegisterStudentDetails>(`${this.urlWithEndPoint}`, parameters,{
        headers: new HttpHeaders({
          "Content-Type": "application/json",
          "Authentication": "frontendui|frontendui"
       }),
        observe: 'response'
      });

  }

  //Description: API Call Structure to Get the DropDown List's and Update in UI
  //Usage: To Get all drop down list deails from the DB using WS-Call and send it to the UI
  //Arg's: Takes College, Branch, Degree, Modules as arguement which will be explicitly passed in Request variable
  //Return's: Server response corresponding to the UI-Get WS-Call
  getDropDownList(Request: GetUIList): Observable<HttpResponse<GetUIList>> {
    
    var parameters = {
      EvertzInterviewApp: {
        Subsystem: "UI",
        Command: "Get",
        ParameterList: {
                "UIDropdownField": Request
            }
        }
    }

    return this.httpClientObject.post<GetUIList>(`${this.urlWithEndPoint}`, parameters,{
      headers: new HttpHeaders({
        "Content-Type": "application/json",
        "Authentication": "frontendui|frontendui"
     }),
      observe: 'response'
    });

  }


  candiateGenerateQuestoins(registerNumber: CandidateQuestions,questionType: CandidateQuestions): Observable<HttpResponse<CandidateQuestions>>{

    var parameters = {
      EvertzInterviewApp: {
        Subsystem: "Candidate",
        Command: "GenerateQuestions",
        ParameterList: {
                "RegisterNumber": registerNumber,
                "QuestionType": questionType
            }
        }
    }
    console.log(parameters);
    return this.httpClientObject.post<CandidateQuestions>(`${this.urlWithEndPoint}`, parameters,{
      headers: new HttpHeaders({
        "Content-Type": "application/json",
        "Authentication": "frontendui|frontendui"
     }),
      observe: 'response'
    });    

  }

  candiateGetQuestoins(registerNumber: CandidateQuestions,questionType: CandidateQuestions): Observable<HttpResponse<CandidateQuestions>>{

    var parameters = {
      EvertzInterviewApp: {
        Subsystem: "Candidate",
        Command: "GetQuestions",
        ParameterList: {
                "RegisterNumber": registerNumber,
                "QuestionType": questionType
            }
        }
    }

    return this.httpClientObject.post<CandidateQuestions>(`${this.urlWithEndPoint}`, parameters,{
      headers: new HttpHeaders({
        "Content-Type": "application/json",
        "Authentication": "frontendui|frontendui"
     }),
      observe: 'response'
    });    

  }

  candiateGetSavedProgram(registerNumber: CandidateQuestions,questionId: CandidateQuestions): Observable<HttpResponse<CandidateQuestions>>{

    var parameters = {
      EvertzInterviewApp: {
        Subsystem: "Candidate",
        Command: "GetSavedProgram",
        ParameterList: {
                "RegisterNumber": registerNumber,
                "QuestionId": questionId
            }
        }
    }

    return this.httpClientObject.post<CandidateQuestions>(`${this.urlWithEndPoint}`, parameters,{
      headers: new HttpHeaders({
        "Content-Type": "application/json",
        "Authentication": "frontendui|frontendui"
     }),
      observe: 'response'
    });    

  }

  candiateSaveAnswer(registerNumber: CandidateSaveAnswer,questionId:CandidateSaveAnswer,answer:CandidateSaveAnswer,questionType:CandidateSaveAnswer): Observable<HttpResponse<CandidateSaveAnswer>>{

    var parameters = {
      EvertzInterviewApp: {
        Subsystem: "Candidate",
        Command: "SaveAnswer",
        ParameterList: {
                "RegisterNumber": registerNumber,
                "QuestionId": questionId,
                "QuestionType": questionType,
                "Answer": answer
            }
        }
    }
    
    return this.httpClientObject.post<CandidateSaveAnswer>(`${this.urlWithEndPoint}`, parameters,{
      headers: new HttpHeaders({
        "Content-Type": "application/json",
        "Authentication": "frontendui|frontendui"
     }),
      observe: 'response'
    });    

  }

  candiateSubmitAnswer(registerNumber: CandidateSaveAnswer,questionId:CandidateSaveAnswer,answer:CandidateSaveAnswer,questionType:CandidateSaveAnswer): Observable<HttpResponse<CandidateSaveAnswer>>{

    var parameters = {
      EvertzInterviewApp: {
        Subsystem: "Candidate",
        Command: "SubmitAnswer",
        ParameterList: {
                "RegisterNumber": registerNumber,
                "QuestionId": questionId,
                "QuestionType": questionType,
                "Answer": answer
            }
        }
    }
    
    return this.httpClientObject.post<CandidateSaveAnswer>(`${this.urlWithEndPoint}`, parameters,{
      headers: new HttpHeaders({
        "Content-Type": "application/json",
        "Authentication": "frontendui|frontendui"
     }),
      observe: 'response'
    });    

  }

  candiateCodingTask(command: CandidateCodingQuestions,registerNumber: CandidateCodingQuestions,questionId:CandidateCodingQuestions,questionType:CandidateCodingQuestions): Observable<HttpResponse<CandidateCodingQuestions>>{

    var parameters = {
      EvertzInterviewApp: {
        Subsystem: "Candidate",
        Command: command,
        ParameterList: {
                "RegisterNumber": registerNumber,
                "QuestionId": questionId,
                "QuestionType": questionType
            }
        }
    }

    return this.httpClientObject.post<CandidateCodingQuestions>(`${this.urlWithEndPoint}`, parameters,{
      headers: new HttpHeaders({
        "Content-Type": "application/json",
        "Authentication": "frontendui|frontendui"
     }),
      observe: 'response'
    });    

  }

  adminResetTest(registerNumber: AdminResetTest,questionType: AdminResetTest): Observable<HttpResponse<AdminResetTest>>{

    var parameters = {
      EvertzInterviewApp: {
        Subsystem: "Candidate",
        Command: "ResetTest",
        ParameterList: {
                "RegisterNumber": registerNumber,
                "QuestionType": questionType
            }
        }
    }
    console.log(parameters);
    return this.httpClientObject.post<AdminResetTest>(`${this.urlWithEndPoint}`, parameters,{
      headers: new HttpHeaders({
        "Content-Type": "application/json",
        "Authentication": "frontendui|frontendui"
     }),
      observe: 'response'
    });    

  }

  adminLogin(userName: AdminLogin,password: AdminLogin): Observable<HttpResponse<AdminLogin>>{

    var parameters = {
      EvertzInterviewApp: {
        Subsystem: "Admin",
        Command: "Login",
        ParameterList: {
                "UserName": userName,
                "Password": password
            }
        }
    }
    console.log(parameters);
    return this.httpClientObject.post<AdminLogin>(`${this.urlWithEndPoint}`, parameters,{
      headers: new HttpHeaders({
        "Content-Type": "application/json",
        "Authentication": "frontendui|frontendui"
     }),
      observe: 'response'
    });    

  }

  adminAddUser(userName: AdminLogin,password: AdminLogin): Observable<HttpResponse<AdminLogin>>{

    var parameters = {
      EvertzInterviewApp: {
        Subsystem: "Admin",
        Command: "AddUser",
        ParameterList: {
                "UserName": userName,
                "Password": password
            }
        }
    }
    console.log(parameters);
    return this.httpClientObject.post<AdminLogin>(`${this.urlWithEndPoint}`, parameters,{
      headers: new HttpHeaders({
        "Content-Type": "application/json",
        "Authentication": "frontendui|frontendui"
     }),
      observe: 'response'
    });    

  }

  adminChangePassword(userName: AdminLogin,password: AdminLogin, oldPassword: AdminLogin): Observable<HttpResponse<AdminLogin>>{

    var parameters = {
      EvertzInterviewApp: {
        Subsystem: "Admin",
        Command: "UpdateUser",
        ParameterList: {
                "UserName": userName,
                "NewPassword": password,
                "OldPassword" : oldPassword
            }
        }
    }
    console.log(parameters);
    return this.httpClientObject.post<AdminLogin>(`${this.urlWithEndPoint}`, parameters,{
      headers: new HttpHeaders({
        "Content-Type": "application/json",
        "Authentication": "frontendui|frontendui"
     }),
      observe: 'response'
    });    

  }

  adminAddQuestion(question: AdminAddQuestions,moduleId: AdminAddQuestions, answerId: AdminAddQuestions, options: AdminAddQuestions): Observable<HttpResponse<AdminAddQuestions>>{

    var option1, option2, option3, option4;

    try{

      if(options[0]["OptionValue"] != ""){
        option1 = options[0]["OptionValue"];
        console.log("Option 1 = " + option1);
      }

    }catch{
      console.log("option 1 is null/empty");
    }

    try{

      if(options[1]["OptionValue"] != ""){
        option2 = options[1]["OptionValue"];
        console.log("Option 2 = " + option2);
      }

    }catch{
      console.log("option 2 is null/empty");
    }

    try{

      if(options[2]["OptionValue"] != ""){
        option3 = options[2]["OptionValue"];
        console.log("Option 3 = " + option3);
      }

    }catch{
      console.log("option 3 is null/empty");
    }

    try{

      if(options[3]["OptionValue"] != ""){
        option4 = options[3]["OptionValue"];
        console.log("Option 4 = " + option4);
      }

    }catch{
      console.log("option 4 is null/empty");
    }

    var parameters = {
      EvertzInterviewApp: {
        Subsystem: "Admin",
        Command: "AddQuestion",
        ParameterList: {
          "Question": question,
          "ModuleId": moduleId,
          "Answer": answerId,
          "QuestionOptions": [
            {
              "OptionId": "1",
              "OptionValue": option1
            },
            {
              "OptionId": "2",
              "OptionValue": option2
            },
            {
              "OptionId": "3",
              "OptionValue": option3
            },
            {
              "OptionId": "4",
              "OptionValue": option4
            }
          ]
        }
      }
    }
    console.log(parameters);
    return this.httpClientObject.post<AdminAddQuestions>(`${this.urlWithEndPoint}`, parameters,{
      headers: new HttpHeaders({
        "Content-Type": "application/json",
        "Authentication": "frontendui|frontendui"
     }),
      observe: 'response'
    });    

  }

  candidateTestStatus(registerNumber: CandidateTestStatus,questionType: CandidateTestStatus): Observable<HttpResponse<CandidateTestStatus>>{

    var parameters = {
      EvertzInterviewApp: {
        Subsystem: "Candidate",
        Command: "TestStatus",
        ParameterList: {
                "RegisterNumber": registerNumber,
                "QuestionType": questionType
            }
        }
    }
    console.log(parameters);
    return this.httpClientObject.post<CandidateTestStatus>(`${this.urlWithEndPoint}`, parameters,{
      headers: new HttpHeaders({
        "Content-Type": "application/json",
        "Authentication": "frontendui|frontendui"
     }),
      observe: 'response'
    });    

  }

  AddDropDownList(request: AddUIList,requestData: AddUIList): Observable<HttpResponse<AddUIList>> {
    
    var parameters = {
      EvertzInterviewApp: {
        Subsystem: "UI",
        Command: "Add",
        ParameterList: {
                "UIDropdownField": request,
                "Data": requestData
            }
        }
    }

    return this.httpClientObject.post<AddUIList>(`${this.urlWithEndPoint}`, parameters,{
      headers: new HttpHeaders({
        "Content-Type": "application/json",
        "Authentication": "frontendui|frontendui"
     }),
      observe: 'response'
    });

  }

  AdminGenerateResult(Request: GetUIList): Observable<HttpResponse<GetUIList>> {
    
    var parameters = {
      EvertzInterviewApp: {
        Subsystem: "Admin",
        Command: "GenerateResult",
        ParameterList: {
                "CollegeId": Request
            }
        }
    }
    console.log(parameters);
    return this.httpClientObject.post<GetUIList>(`${this.urlWithEndPoint}`, parameters,{
      headers: new HttpHeaders({
        "Content-Type": "application/json",
        "Authentication": "frontendui|frontendui"
     }),
      observe: 'response'
    });

  }

  AdminGetAll(Request: GetUIList): Observable<HttpResponse<GetUIList>> {
    
    var parameters = {
      EvertzInterviewApp: {
        Subsystem: "Admin",
        Command: "GetAll",
        ParameterList: {
                "UIDropdownField": Request
            }
        }
    }
    console.log(parameters);
    return this.httpClientObject.post<GetUIList>(`${this.urlWithEndPoint}`, parameters,{
      headers: new HttpHeaders({
        "Content-Type": "application/json",
        "Authentication": "frontendui|frontendui"
     }),
      observe: 'response'
    });
    
  }

  AdminGetMandatoryModules(Request: GetUIList): Observable<HttpResponse<GetUIList>> {
    
    var parameters = {
      EvertzInterviewApp: {
        Subsystem: "Admin",
        Command: "GetMandatoryModules",
        ParameterList: {
                "UIDropdownField": Request
            }
        }
    }
    console.log(parameters);
    return this.httpClientObject.post<GetUIList>(`${this.urlWithEndPoint}`, parameters,{
      headers: new HttpHeaders({
        "Content-Type": "application/json",
        "Authentication": "frontendui|frontendui"
     }),
      observe: 'response'
    });
    
  }

  AdminSetCollege(CollegeId: SetCollege): Observable<HttpResponse<SetCollege>> {
    
    var parameters = {
      EvertzInterviewApp: {
        Subsystem: "Admin",
        Command: "SetCollege",
        ParameterList: {
                "CollegeId": CollegeId
            }
        }
    }
    console.log(parameters);
    return this.httpClientObject.post<SetCollege>(`${this.urlWithEndPoint}`, parameters,{
      headers: new HttpHeaders({
        "Content-Type": "application/json",
        "Authentication": "frontendui|frontendui"
     }),
      observe: 'response'
    });
    
  }

}