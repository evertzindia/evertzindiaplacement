import { Component, OnInit, ViewChild ,HostListener} from '@angular/core';
import {ActivatedRoute, Router, NavigationExtras} from "@angular/router";
import { SystemConfig } from '../ws-calls/system-config';
import { PostToServerService } from '../post-to-server.service';
import { HttpResponse, HttpErrorResponse } from '@angular/common/http';
import { LoadingBarService } from '@ngx-loading-bar/core';
import { timer } from 'rxjs';
import { CountdownComponent } from 'ngx-countdown';
import { browserRefresh } from '../app.component';

@Component({
  selector: 'app-questions-page',
  templateUrl: './questions-page.component.html',
  styleUrls: ['./questions-page.component.css']
})
export class QuestionsPageComponent implements OnInit {

  public registerNumber: string;
  public studentName: string;
  public previousQid: any;
  public passButtonClass = "success";
  public defaultButtonClass = "outline-primary";
  public questionType = "MCQ";
  public serverResponse = "";
  public serverStatus_Error_Class = "";
  public serverStatus_Error_Box = "";
  public serverStatus_Error_Message = "";
  //public questionJson = this.systemConfig.questionsList["EvertzInterviewApp"]["Output"]["ParameterList"]["QuestionsList"]
  public questionJson: any;
  public moduleOnHTML = "";
  public questionOnHTML = "";
  public optionIdSelected = "";
  public optionIdSelectedList = [];
  public optionIdSelectdByCandidate = "";
  public buttonColor = [];
  public questionBlock = [];
  public moduleName = [];
  public questionToDisplay = [];
  public sortedModuleQuestions = [];
  public unsortedModuleQuestions = [];
  public totalQuestions: any;
  public isLastQuestion = "false";
  public isQuestionsLoaded = "false";
  public submitted = false;
  public windowActive = true;
  public resasonForSubmitting = "";
  public mcqTimer = this.systemConfig.timerForMCQ;
  isShow: boolean;
  topPosToStartShowing = 100;
  public resetMCQTime = this.systemConfig.errorResetTime;
  seconds: number = this.resetMCQTime;
  interval;


  @ViewChild('countdown') counter:
  CountdownComponent;

  
  finishTest(countdown){
    console.log(countdown["left"]);
    if(parseInt(countdown["left"]) == 0 ){
      console.log("finishing test...");
      this.candiateSubmitAnswer(this.registerNumber,this.questionToDisplay["QId"],this.optionIdSelected,this.questionType,true);
    }
  }

  @HostListener('window:scroll')
  checkScroll() {
      
    // window의 scroll top
    // Both window.pageYOffset and document.documentElement.scrollTop returns the same result in all the cases. window.pageYOffset is not supported below IE 9.

    const scrollPosition = window.pageYOffset || document.documentElement.scrollTop || document.body.scrollTop || 0;

    //console.log('[scroll]', scrollPosition);
    
    if (scrollPosition >= this.topPosToStartShowing) {
      this.isShow = true;
    } else {
      this.isShow = false;
    }
  }

  // TODO: Cross browsing
  gotoTop() {
    window.scroll({ 
      top: 0, 
      left: 0, 
      behavior: 'auto' 
    });
  }

  @HostListener('document:keydown', ['$event'])
  handleKeyboardEvent(event: KeyboardEvent) { 
    console.log(event.key);
    if(event.key == "Escape"){
      //submit the test
      //this.resasonForSubmitting = "Escape";
      //this.candiateSubmitAnswer(this.registerNumber,this.questionToDisplay["QId"],this.optionIdSelected,this.questionType,false);
    }else if(event.key == "F5"){
      //this.resasonForSubmitting = "F5";
      //this.candiateSubmitAnswer(this.registerNumber,this.questionToDisplay["QId"],this.optionIdSelected,this.questionType,false);
      //return false;
    }
  }
   
  //To Update the Registration Number and Language Selected from Registration Page
  public constructor(private postToServerObject: PostToServerService,private route: ActivatedRoute, private systemConfig: SystemConfig,private router: Router,private loadingBar: LoadingBarService) {
      this.route.queryParams.subscribe(params => {
          this.registerNumber = params["registerNumber"];
          this.studentName = params["studentName"];           
      });
    }

ngOnInit() {

    this.startLoading();
    this.validateStudentTestStatus(this.registerNumber,this.questionType);
    if(browserRefresh){
      this.onReload();
    }
    this.startTimer();
  
  window.onblur = () =>{
      //console.log("in-active window");
      //this.resasonForSubmitting = "Tab";
      //this.candiateSubmitAnswer(this.registerNumber,this.questionToDisplay["QId"],this.optionIdSelected,this.questionType,false);
   };

  }

 

  onReload(){
    //this.resasonForSubmitting = "Reload";
    //this.candiateSubmitAnswer(this.registerNumber,this.questionToDisplay["QId"],this.optionIdSelected,this.questionType,false);
    //console.log("Browser Reloaded");
  }

  startTimer() {
    this.interval = setInterval(() => {
      if(this.seconds > 0) {
        this.seconds--;
      } else {
        this.seconds = this.resetMCQTime;
        this.serverStatus_Error_Class = "";
        this.serverStatus_Error_Box = "";
        this.serverStatus_Error_Message = "";
      }
    },1000)
  }

  startLoading() {
    this.loadingBar.start();
  }

  stopLoading() {
    this.loadingBar.complete();
  }

  validateStudentTestStatus(registerNumber,questionType){

    this.postToServerObject.candidateTestStatus(registerNumber,questionType).subscribe((res: HttpResponse<any>)=> {
      console.log(JSON.parse(JSON.stringify(res.body)));
      var wsCallStatus = "";
      this.serverResponse = JSON.parse(JSON.stringify(res.body));
      wsCallStatus = this.serverResponse["EvertzInterviewApp"]["Success"];

      if(!wsCallStatus){

          let navigationExtras: NavigationExtras = {
          queryParams: {
              registerNumber : this.registerNumber,
              status : 1
          }
          
        };
        
      this.router.navigate(['/info'], navigationExtras)

      }else{
        this.candiateGetQuestoins(this.registerNumber,this.questionType);
      }      


    },
    (err: HttpErrorResponse) => {     
      if(err.status == 0) {

        //window.scrollTo(0, 0);
        this.gotoTop();
        this.serverStatus_Error_Class = "isa_error";
        this.serverStatus_Error_Box = "fa fa-times-circle";
        this.serverStatus_Error_Message = "Test Already Taken, Please contact your Supervisor.";

        }
        else {
        console.log("Questions Received sucessfully for : " + registerNumber);
      }
    }
  );

  }

  getQuestionsUpdateToUI(){

        //console.log(this.questionJson);
        //iterate to get QuestionsList

        for(let question in this.questionJson){
          this.questionBlock[question] = this.questionJson[question];
          if(parseInt(this.questionJson[question]["Questions"][0]["OptionSelected"]) != 0){
            this.buttonColor.push(this.passButtonClass);
          }else{
            this.buttonColor.push(this.defaultButtonClass);
          }
          //this.buttonColor.push(this.defaultButtonClass);
          this.optionIdSelectedList.push(this.questionJson[question]["Questions"][0]["OptionSelected"]);
        }

      //iterate to find how many Modules in questions list
      for(let module in this.questionBlock){
        var moduleExists = this.moduleName.includes(this.questionBlock[module]["ModuleName"]);
        if(!moduleExists){
          this.moduleName.push(this.questionBlock[module]["ModuleName"]);
        }
      }

      //to seperate out the questions based on ModuleName
      for(let module in this.moduleName){
        for(let moduleInjson in this.questionBlock){
            if(this.moduleName[module] == this.questionBlock[moduleInjson]["ModuleName"]){
                var bindModule = this.questionBlock[moduleInjson]["Questions"][0]["Question"];
                bindModule = this.questionBlock[moduleInjson]["ModuleName"] + "|" + bindModule;
                this.questionBlock[moduleInjson]["Questions"][0]["Question"] = bindModule;
                this.unsortedModuleQuestions.push(this.questionBlock[moduleInjson]["Questions"][0]);
            }
        }
        this.sortQuestions();
        this.unsortedModuleQuestions = [];

      }

      //to check the total questions
      console.log(this.moduleName);
      console.log(this.sortedModuleQuestions);
      //console.log(this.sortedModuleQuestions[0]);

      //setting Question1 on load
      this.questionToDisplay = [];
      this.questionToDisplay = this.sortedModuleQuestions[0];
      this.optionIdSelectdByCandidate = this.optionIdSelectedList[0];
      var moduleInSelectedQuestion = this.questionToDisplay["Question"].split("|")[0];
      this.questionOnHTML = this.questionToDisplay["Question"].substring(moduleInSelectedQuestion.length+1);
      this.previousQid = 0;

      this.stopLoading();
      this.isQuestionsLoaded = "true";
  }

  sortQuestions(){

    for(var i=0; i<this.unsortedModuleQuestions.length; i++){
      for(var j=i+1; j<this.unsortedModuleQuestions.length; j++){
          if( parseInt( this.unsortedModuleQuestions[i]["Id"]) > parseInt (this.unsortedModuleQuestions[j]["Id"]) ){
            var temp = this.unsortedModuleQuestions[i]["Id"];
            this.unsortedModuleQuestions[i]["Id"] = this.unsortedModuleQuestions[j]["Id"];
            this.unsortedModuleQuestions[j]["Id"] = temp;
          }
      }
    }

    for(let questions in this.unsortedModuleQuestions){
        this.sortedModuleQuestions.push(this.unsortedModuleQuestions[questions]);
    }
    
    this.totalQuestions = this.sortedModuleQuestions.length;
  }

  
  checkModuleQuestion(totalQuestions){
    if(totalQuestions["Question"].split("|")[0] == this.moduleOnHTML){
      return true;
    }else{
      return false;
    }
    
  }

  checkModule(moduleOnUI){
    this.moduleOnHTML = moduleOnUI.toString();
    return true;
  }

  displayQuestionOnUI(id){

    this.questionToDisplay = [];

    this.questionToDisplay = this.sortedModuleQuestions[parseInt(id)-1];
    var moduleInSelectedQuestion = this.questionToDisplay["Question"].split("|")[0];
    this.questionOnHTML = this.questionToDisplay["Question"].substring(moduleInSelectedQuestion.length+1);
    console.log("Option selected for "+ id + " is: " + this.optionIdSelectedList[parseInt(id)-1]);
    this.optionIdSelectdByCandidate = this.optionIdSelectedList[parseInt(id)-1];
    if(this.totalQuestions == parseInt(this.questionToDisplay["Id"])){
      this.isLastQuestion = "true";
    }else{
      this.isLastQuestion = "false";
    }
    //console.log(id-1);
    //console.log(this.previousQid);

    console.log("Processing QId: "+this.questionToDisplay["QId"]);
    console.log("Processing Id: "+this.questionToDisplay["Id"]);

    if(this.previousQid != id-1){
      if(this.buttonColor[this.previousQid] == this.passButtonClass){
      }else{
        this.buttonColor[this.previousQid] = this.defaultButtonClass;
      }      
    }

    this.previousQid = id-1;
    this.gotoTop();
  }

  radioChangeHandler(event: any){
    this.optionIdSelected = event.target.value;
  }

  /*saveQuestionClicked(){

    if(this.optionIdSelected!=""){
    this.buttonColor[parseInt(this.questionToDisplay["Id"])-1] = this.passButtonClass;
    this.optionIdSelectedList[parseInt(this.questionToDisplay["Id"])] = this.optionIdSelected;
    }
    this.previousQid = parseInt(this.questionToDisplay["Id"])-1;
    this.optionIdSelected = "";

  }*/

  nextQuestionClicked(){

    if(this.isLastQuestion !="true"){

      console.log("-----Start Next--------");
      console.log("Processed QID: "+this.questionToDisplay["QId"]);
      console.log("Processed ID: "+this.questionToDisplay["Id"]);
      //console.log(this.optionIdSelected);
      this.optionIdSelectdByCandidate = this.optionIdSelectedList[parseInt(this.questionToDisplay["Id"])];
      console.log("Option selected for "+ parseInt(this.questionToDisplay["Id"]) + " is: " + this.optionIdSelectedList[parseInt(this.questionToDisplay["Id"])]);

        //saves the option and moves to the next question
        if(this.optionIdSelected!=""){
          this.buttonColor[parseInt(this.questionToDisplay["Id"])-1] = this.passButtonClass;
          this.optionIdSelectedList[parseInt(this.questionToDisplay["Id"])-1] = this.optionIdSelected;
          this.candiateSaveAnswer(this.registerNumber,this.questionToDisplay["QId"],this.optionIdSelected,this.questionType);
        }

        this.optionIdSelected = "";
        
        this.previousQid = parseInt(this.questionToDisplay["Id"]);

        this.questionToDisplay = this.sortedModuleQuestions[parseInt(this.questionToDisplay["Id"])];
        this.optionIdSelectdByCandidate = this.optionIdSelectedList[parseInt(this.questionToDisplay["Id"])-1];

        var moduleInSelectedQuestion = this.questionToDisplay["Question"].split("|")[0];
        this.questionOnHTML = this.questionToDisplay["Question"].substring(moduleInSelectedQuestion.length+1);

        //for button changes
        if(this.buttonColor[parseInt(this.questionToDisplay["Id"])-2] != this.passButtonClass){
          this.buttonColor[parseInt(this.questionToDisplay["Id"])-2] = this.defaultButtonClass;
        }

        //checking if moduleId is the last question
        if((this.totalQuestions-1 == parseInt(this.questionToDisplay["Id"])-1) || (this.totalQuestions == parseInt(this.questionToDisplay["Id"]))){
          console.log("last question in module");
          this.isLastQuestion = "true";
        }

        
        console.log("-------------");
      console.log("Processing QID: "+this.questionToDisplay["QId"]);
      console.log("Processing ID: "+this.questionToDisplay["Id"]);
      //console.log(this.optionIdSelected);
      //this.optionIdSelectdByCandidate = this.optionIdSelectedList[parseInt(this.questionToDisplay["Id"])];
      console.log("Option selected for "+ parseInt(this.questionToDisplay["Id"]) + " is: " + this.optionIdSelectedList[parseInt(this.questionToDisplay["Id"])-1]);
        console.log("-----End Next--------");
    }else{

      if(this.optionIdSelected!=""){
        this.buttonColor[parseInt(this.questionToDisplay["Id"])-1] = this.passButtonClass;
        this.optionIdSelectedList[parseInt(this.questionToDisplay["Id"])] = this.optionIdSelected;
      }
      //saving the Answer to DB
      console.log("Processing ID: "+this.questionToDisplay["QId"]);

      console.log("calling submit Test function");
      this.candiateSubmitAnswer(this.registerNumber,this.questionToDisplay["QId"],this.optionIdSelected,this.questionType,true);
    }

  }

    onSubmit(event:any) {
      this.submitted = true;
      //console.log("Submitted Successfully");
      $('#submitPrompt').modal('hide');
      /*let navigationExtras: NavigationExtras = {
        queryParams: {
            registerNumber : this.registerNumber,
            studentName : this.studentName
        }
        
      };*/
      

      this.candiateSubmitAnswer(this.registerNumber,this.questionToDisplay["QId"],this.optionIdSelected,this.questionType,true);

      //Moves to the next page, and passing registerNumber and languageName as arguement.
      //this.router.navigate(['/coding'], navigationExtras);
  }

  public candiateGetQuestoins(registerNumber,questionType){

    this.postToServerObject.candiateGetQuestoins(registerNumber,questionType).subscribe((res: HttpResponse<any>)=> {
        console.log(JSON.parse(JSON.stringify(res.body)));
        var wsCallStatus = "";
        this.serverResponse = JSON.parse(JSON.stringify(res.body));
        wsCallStatus = this.serverResponse["EvertzInterviewApp"]["Success"];

        if(!wsCallStatus){

          this.serverStatus_Error_Class = "isa_error";
          this.serverStatus_Error_Box = "fa fa-times-circle";
          this.serverStatus_Error_Message = "Server Unable to Get Questions list, Please contact Your Supervisor.";
          //window.scrollTo(0, 0);
          this.gotoTop();
          window.stop();
        }else{
          this.questionJson = this.serverResponse["EvertzInterviewApp"]["ParameterList"]["QuestionsList"];
          //console.log(this.questionJson);
          //console.log("Questoins Added");
          this.getQuestionsUpdateToUI();
        }

      },
      (err: HttpErrorResponse) => {     
        if(err.status == 0) {

          //window.scrollTo(0, 0);
          this.gotoTop();
          this.serverStatus_Error_Class = "isa_error";
          this.serverStatus_Error_Box = "fa fa-times-circle";
          this.serverStatus_Error_Message = "Server failed to Get Questions List, Please contact your Supervisor.";
          let navigationExtras: NavigationExtras = {
            queryParams: {
                registerNumber : this.registerNumber,
                status : 5,
            }
            
          };
          
          this.router.navigate(['/info'], navigationExtras);

          }
          else {
          console.log("Questions Received sucessfully for : " + registerNumber);
        }
      }
    );
  };


  public candiateSaveAnswer(registerNumber,qId,answer,option){

    this.postToServerObject.candiateSaveAnswer(registerNumber,qId,answer,option).subscribe((res: HttpResponse<any>)=> {
        var wsCallStatus = "";
        this.serverResponse = JSON.parse(JSON.stringify(res.body));
        console.log(this.serverResponse);
        wsCallStatus = this.serverResponse["EvertzInterviewApp"]["Success"];

        if(!wsCallStatus){

          this.serverStatus_Error_Class = "isa_error";
          this.serverStatus_Error_Box = "fa fa-times-circle";
          this.serverStatus_Error_Message = "Server Unable to Save Answer, Please contact Your Supervisor.";
          //window.scrollTo(0, 0);
          this.gotoTop();

        }

      },
      (err: HttpErrorResponse) => {     
        if(err.status == 0) {

          //window.scrollTo(0, 0);
          this.gotoTop();
          this.serverStatus_Error_Class = "isa_error";
          this.serverStatus_Error_Box = "fa fa-times-circle";
          this.serverStatus_Error_Message = "Server Down, Please contact Your Supervisor.";
          let navigationExtras: NavigationExtras = {
            queryParams: {
                registerNumber : this.registerNumber,
                status : 5
            }
            
          };
          
          this.router.navigate(['/info'], navigationExtras);

          }
          else {
          console.log("Question answer saved to DB sucessfully : " + qId);
        }
      }
    );
  };

  public candiateSubmitAnswer(registerNumber,qId,answer,option,testProgress){

    this.postToServerObject.candiateSubmitAnswer(registerNumber,qId,answer,option).subscribe((res: HttpResponse<any>)=> {
        console.log(JSON.parse(JSON.stringify(res.body)));
        var wsCallStatus = "";
        this.serverResponse = JSON.parse(JSON.stringify(res.body));
        wsCallStatus = this.serverResponse["EvertzInterviewApp"]["Success"];

        if(!wsCallStatus){

          this.serverStatus_Error_Class = "isa_error";
          this.serverStatus_Error_Box = "fa fa-times-circle";
          this.serverStatus_Error_Message = "Failed to Submit Test, Please contact your Supervisor.";
          //window.scrollTo(0, 0);
          this.gotoTop();
          window.stop();
        }else{
            
          if(testProgress){

            //call generate questions for coding
            option = "Coding";
            this.candiateGenerateQuestoins(registerNumber,option);

          }else{

              
          let navigationExtras: NavigationExtras = {
            queryParams: {
                registerNumber : this.registerNumber,
                status : 4,
                event : this.resasonForSubmitting
            }
            
          };
          
          this.router.navigate(['/info'], navigationExtras)

          }          
        }

      },
      (err: HttpErrorResponse) => {     
        if(err.status == 0) {

          //window.scrollTo(0, 0);
          this.gotoTop();
          this.serverStatus_Error_Class = "isa_error";
          this.serverStatus_Error_Box = "fa fa-times-circle";
          this.serverStatus_Error_Message = "Please contact Your Supervisor.";
          let navigationExtras: NavigationExtras = {
            queryParams: {
                registerNumber : this.registerNumber,
                status : 5
            }
            
          };
          
          this.router.navigate(['/info'], navigationExtras);

          
          }
          else {
          console.log("Test submitted sucessfully for Student: " + registerNumber);
        }
      }
    );
  };

  public candiateGenerateQuestoins(registerNumber,questionType){

    this.postToServerObject.candiateGenerateQuestoins(registerNumber,questionType).subscribe((res: HttpResponse<any>)=> {
        console.log(JSON.parse(JSON.stringify(res.body)));
        var wsCallStatus = "";
        this.serverResponse = JSON.parse(JSON.stringify(res.body));
        wsCallStatus = this.serverResponse["EvertzInterviewApp"]["Success"];

        if(!wsCallStatus){

          this.serverStatus_Error_Class = "isa_error";
          this.serverStatus_Error_Box = "fa fa-times-circle";
          this.serverStatus_Error_Message = "Server Unable to Generate Coding Questions list, Please contact Your Supervisor.";
          //window.scrollTo(0, 0);
          this.gotoTop();
          window.stop();
        }
        let navigationExtras: NavigationExtras = {
          queryParams: {
              registerNumber : this.registerNumber,
              studentName : this.studentName
          }
          
        };

        console.log("Coding Questions Generated sucessfully for : " + registerNumber);
        
        //Moves to the next page, and passing registerNumber and languageName as arguement.
        this.router.navigate(['/coding'], navigationExtras);

      },
      (err: HttpErrorResponse) => {     
        if(err.status == 0) {
          //window.scrollTo(0, 0);
          this.gotoTop();
          this.serverStatus_Error_Class = "isa_error";
          this.serverStatus_Error_Box = "fa fa-times-circle";
          this.serverStatus_Error_Message = "Server failed to Generate Coding Questions, Please contact your Supervisor.";
          let navigationExtras: NavigationExtras = {
            queryParams: {
                registerNumber : this.registerNumber,
                status : 5
            }
            
          };
          
          this.router.navigate(['/info'], navigationExtras);

          }
          else {
          console.log("Coding Questions Generated sucessfully for : " + registerNumber);
        }
      }
    );
  };

}