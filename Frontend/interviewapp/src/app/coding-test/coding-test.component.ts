import { Component, OnInit, ViewChild ,HostListener ,ElementRef} from '@angular/core';
import { HttpErrorResponse,HttpResponse } from '@angular/common/http';
import { PostToServerService } from '../post-to-server.service';
import {ActivatedRoute, Router, NavigationExtras} from "@angular/router";
import { SystemConfig } from '../ws-calls/system-config';
import {NgForm} from '@angular/forms';
import { CountdownComponent } from 'ngx-countdown';
import { LoadingBarService } from '@ngx-loading-bar/core';
import { browserRefresh } from '../app.component';

@Component({
  selector: 'app-coding-test',
  templateUrl: './coding-test.component.html',
  styleUrls: ['./coding-test.component.css']
})
export class CodingTestComponent implements OnInit {

  codeoutput:any;
  
  public editorOptions: any;
  public registerNumber:any;
  public studentName:any;
  public codingLanguage:any;
  public questionsBlock: any;
  public questionToDisplay: any;
  public question: any;
  public isNextButtonEnaled = "false";
  public totalQuestoins: any;
  public nextQuestion = 0;
  public moduleName: any;
  public compilationStatus: any;
  public isTestCaseResultsAvailable = "false";
  public isProgramRunning = "false";
  public testCaseResults: any;
  public questionType = "Coding";
  public serverResponse = "";
  public resasonForSubmitting = "";
  public serverStatus_Error_Class = "";
  public serverStatus_Error_Box = "";
  public serverStatus_Error_Message = "";
  public codingTimer = this.systemConfig.timerForCoding;
  public submitted = false;
  public isQuestionsLoaded = "false";
  public baseCode:String;
  public code:String;
  public testCaseFailed = "false";
  public compilationResult = "false";
  public resetCodingTime = this.systemConfig.errorResetTime;
  seconds: number = this.resetCodingTime;
  interval;

  //editorOptions = {theme: 'vs-dark', language: String(this.moduleName.toLowerCase()),acceptSuggestionOnEnter:'off'};


  @ViewChild('countdown') counter:
  CountdownComponent;
  
  constructor(private postToServerObject: PostToServerService,private route: ActivatedRoute,private systemConfig: SystemConfig,private router: Router,private loadingBar: LoadingBarService) { 
      this.route.queryParams.subscribe(params => {
        this.registerNumber = params["registerNumber"];
        this.studentName = params["studentName"];           
    });
    
  }

 

  @HostListener('document:keydown', ['$event'])
  handleKeyboardEvent(event: KeyboardEvent) { 
    console.log(event.key);
    if(event.key == "Escape"){
      //submit the test
      //this.resasonForSubmitting = "Escape";
      //this.candiateSubmitAnswer(this.registerNumber,this.questionToDisplay["QId"],"",this.questionType,false);
    }else if(event.key == "F5"){
      //this.resasonForSubmitting = "F5";
      //this.candiateSubmitAnswer(this.registerNumber,this.questionToDisplay["QId"],"",this.questionType,false);
      //return false;
    }
  }

  ngOnInit() {
    this.validateStudentTestStatus(this.registerNumber,this.questionType);
    //this.questionsBlock = this.systemConfig.questionsList["EvertzInterviewApp"]["ParameterList"]["QuestionsList"]["Questions"];
    //this.getQuestions();
    this.startLoading();

    if(browserRefresh){
      this.onReload();
    }
    window.onblur = () =>{
      console.log("in-active window");
      //this.resasonForSubmitting = "Tab";
      //this.candiateSubmitAnswer(this.registerNumber,this.questionToDisplay["QId"],"",this.questionType,false);
   }
       
  }


  onReload(){
    //this.resasonForSubmitting = "Reload";
    //this.candiateSubmitAnswer(this.registerNumber,this.questionToDisplay["QId"],"",this.questionType,false);
    console.log("Browser Reloaded");
  }

  startLoading() {
    this.loadingBar.start();
  }

  stopLoading() {
    this.loadingBar.complete();
  }

  onSubmit(event:any,code) {
    this.submitted = true;
    $('#submitPrompt').modal('hide');
    this.candiateSubmitAnswer(this.registerNumber,this.questionToDisplay["QId"],code._value,this.questionType,true);
  }

  startTimer() {
    this.interval = setInterval(() => {
      if(this.seconds > 0) {
        this.seconds--;
      } else {
        this.seconds = this.resetCodingTime;
        this.serverStatus_Error_Class = "";
        this.serverStatus_Error_Box = "";
        this.serverStatus_Error_Message = "";
      }
    },1000)
  }


  finishTest(countdown,code){
    //console.log(String(code._value));
    //console.log(countdown);
    if(parseInt(countdown["left"]) == 0 ){
      console.log("finishing test...");
      this.submitProgram(code._value);
    }
 
  }

  getSavedProgram(){

    this.candiateGetSavedProgram(this.registerNumber,this.questionToDisplay["QId"]);

  }

  getQuestions(){
    this.totalQuestoins = this.questionsBlock.length;
    if(this.questionsBlock.length > 1){
      this.questionToDisplay = this.questionsBlock[0];
      this.question = this.questionToDisplay["Question"];
      this.moduleName = this.questionToDisplay["ModuleName"];
      this.baseCode = this.questionToDisplay["BaseCode"];
     
    }
  }

  saveProgram(code){
    //this.candidateCode = code;
    console.log(code._value);
    this.compilationStatus = "";
    this.isTestCaseResultsAvailable = "false";
    this.compilationResult = "false";
    this.testCaseFailed = "false";
    this.isProgramRunning = "true";
    this.candiateSaveAnswer(this.registerNumber,this.questionToDisplay["QId"],code._value,this.questionType);
  }

  compileProgram(code){

    this.isTestCaseResultsAvailable = "false";
    this.compilationStatus = "";
    this.compilationResult = "false";
    this.isProgramRunning = "true";
    this.startLoading();
    this.sequence_Compile_1(this.registerNumber,this.questionToDisplay["QId"],code._value,this.questionType);

  }

  runProgram(code){

    this.isTestCaseResultsAvailable = "false";
    this.compilationStatus = "";
    this.compilationResult = "false";
    this.testCaseFailed = "false";
    this.isProgramRunning = "true";
    this.sequence_Run_1(this.registerNumber,this.questionToDisplay["QId"],code._value,this.questionType);
    
  }

  submitProgram(code){

    this.isTestCaseResultsAvailable = "false";
    this.compilationStatus = "";
    this.compilationResult = "false";
    this.testCaseFailed = "false";
    this.isProgramRunning = "true";
    this.startLoading();
    this.candiateSubmitAnswer(this.registerNumber,this.questionToDisplay["QId"],code._value,this.questionType,true);

  }

  nextProgram(code){
    
    this.nextQuestion += 1;
    this.questionToDisplay = this.questionsBlock[this.nextQuestion];
    this.question = this.questionToDisplay["Question"];
    this.moduleName = this.questionToDisplay["ModuleName"];
    this.baseCode = this.questionToDisplay["BaseCode"];
    this.isTestCaseResultsAvailable = "false";
    this.compilationResult = "false";
    this.testCaseFailed = "false";
    this.isProgramRunning = "true";
    this.compilationStatus = "";
    if(this.totalQuestoins == this.nextQuestion){
      this.isNextButtonEnaled = "false";
    }

  }

  validateTestCases() {
    
    for(let testStatus of this.testCaseResults){
      if(testStatus["TestCaseResultStatus"] != 0){
        this.testCaseFailed = "true";
        break;
      }
    }
    this.isTestCaseResultsAvailable = "true";

  }

  validateStudentTestStatus(registerNumber,questionType){

    this.postToServerObject.candidateTestStatus(registerNumber,questionType).subscribe((res: HttpResponse<any>)=> {
      console.log(JSON.parse(JSON.stringify(res.body)));
      var wsCallStatus = "";
      this.serverResponse = JSON.parse(JSON.stringify(res.body));
      wsCallStatus = this.serverResponse["EvertzInterviewApp"]["Success"];

      if(!wsCallStatus){

          let navigationExtras: NavigationExtras = {
          queryParams: {
              registerNumber : this.registerNumber,
              status : 2
          }
          
        };
        
        this.router.navigate(['/info'], navigationExtras)

      }else{

      this.candiateGetQuestoins(this.registerNumber,this.questionType);
    }

    },
    (err: HttpErrorResponse) => {     
      if(err.status == 0) {

        window.scrollTo(0, 0);
        this.serverStatus_Error_Class = "isa_error";
        this.serverStatus_Error_Box = "fa fa-times-circle";
        this.serverStatus_Error_Message = "Test Already Taken, Please contact your Supervisor.";
        this.serverStatus_Error_Message = "Server failed to Get Questions List, Please contact your Supervisor.";
          let navigationExtras: NavigationExtras = {
            queryParams: {
                registerNumber : this.registerNumber,
                status : 5
            }
            
          };
          
          this.router.navigate(['/info'], navigationExtras);

        }
        else {
        console.log("Questions Received sucessfully for : " + registerNumber);
      }
    }
  );

  }

  public candiateGetQuestoins(registerNumber,questionType){

    this.postToServerObject.candiateGetQuestoins(registerNumber,questionType).subscribe((res: HttpResponse<any>)=> {
        console.log(JSON.parse(JSON.stringify(res.body)));
        var wsCallStatus = "";
        this.serverResponse = JSON.parse(JSON.stringify(res.body));
        wsCallStatus = this.serverResponse["EvertzInterviewApp"]["Success"];

        if(!wsCallStatus){

          this.serverStatus_Error_Class = "isa_error";
          this.serverStatus_Error_Box = "fa fa-times-circle";
          this.serverStatus_Error_Message = "Server Unable to Get Coding Questions list, Please contact Your Supervisor.";
          window.scrollTo(0, 0);
          window.stop();
        }else{

        this.questionsBlock = this.serverResponse["EvertzInterviewApp"]["ParameterList"]["QuestionsList"]["Questions"];
        console.log(this.questionsBlock);
        this.totalQuestoins = this.questionsBlock.length;

        if(this.questionsBlock.length > 1){
          this.questionToDisplay = this.questionsBlock[0];
          this.question = this.questionToDisplay["Question"];
          this.moduleName = this.questionToDisplay["ModuleName"];
          this.baseCode = this.questionToDisplay["BaseCode"];
          this.isNextButtonEnaled = "true";
          this.isQuestionsLoaded = "true";
          this.stopLoading();
          if(this.moduleName == "C++"){
              this.moduleName = "cpp";
          }
          this.editorOptions = {theme: 'vs-dark', language: String(this.moduleName.toLowerCase()),acceptSuggestionOnEnter:'on'};
          this.code = this.baseCode.replace(/&nbsp;/g, ' ').replace(/<br>/g, '\n');     
        }else{

          this.questionToDisplay = this.questionsBlock[0];
          this.question = this.questionToDisplay["Question"];
          this.moduleName = this.questionToDisplay["ModuleName"];
          this.baseCode = this.questionToDisplay["BaseCode"];
          this.isQuestionsLoaded = "true";
          this.stopLoading();
          if(this.moduleName == "C++"){
            this.moduleName = "cpp";
          }
          this.editorOptions = {theme: 'vs-dark', language: String(this.moduleName.toLowerCase()),acceptSuggestionOnEnter:'on'};
          this.code = this.baseCode.replace(/&nbsp;/g, ' ').replace(/<br>/g, '\n');          
        }
      }
      },
      (err: HttpErrorResponse) => {     
        if(err.status == 0) {

          window.scrollTo(0, 0);
          this.serverStatus_Error_Class = "isa_error";
          this.serverStatus_Error_Box = "fa fa-times-circle";
          this.serverStatus_Error_Message = "Server failed to Get Coding Questions List, Please contact your Supervisor.";
          this.serverStatus_Error_Message = "Server failed to Get Questions List, Please contact your Supervisor.";
          let navigationExtras: NavigationExtras = {
            queryParams: {
                registerNumber : this.registerNumber,
                status : 5
            }
            
          };
          
          this.router.navigate(['/info'], navigationExtras);

          }
          else {
          console.log("Questions Received sucessfully for : " + registerNumber);
        }
      }
    );
  };

  public candiateGetSavedProgram(registerNumber,qId){

    this.postToServerObject.candiateGetSavedProgram(registerNumber,qId).subscribe((res: HttpResponse<any>)=> {
        var wsCallStatus = "";
        this.serverResponse = JSON.parse(JSON.stringify(res.body));
        console.log(this.serverResponse);
        wsCallStatus = this.serverResponse["EvertzInterviewApp"]["Success"];

        if(!wsCallStatus){

          this.serverStatus_Error_Class = "isa_error";
          this.serverStatus_Error_Box = "fa fa-times-circle";
          this.serverStatus_Error_Message = this.serverResponse["EvertzInterviewApp"]["Reason"];
          
          window.scrollTo(0, 0);

        }else{
          console.log("Program Loaded Sucessfully.");
          this.compilationStatus = "Program Loaded Sucessfully.";  
          //console.log(this.serverResponse["EvertzInterviewApp"]["Reason"]);
        }
      },
      (err: HttpErrorResponse) => {     
        if(err.status == 0) {

          window.scrollTo(0, 0);
          this.serverStatus_Error_Class = "isa_error";
          this.serverStatus_Error_Box = "fa fa-times-circle";
          this.serverStatus_Error_Message = "Failed to Save Answer, please resubmit your answer.";
          this.serverStatus_Error_Message = "Server down, Please contact your Supervisor.";
          let navigationExtras: NavigationExtras = {
            queryParams: {
                registerNumber : this.registerNumber,
                status : 5
            }
            
          };
          
          this.router.navigate(['/info'], navigationExtras);

          }
          else {
          console.log("Program loaded sucessfully for: " + qId);
        }
      }
    );
  };

  public candiateSaveAnswer(registerNumber,qId,answer,option){

    this.postToServerObject.candiateSaveAnswer(registerNumber,qId,answer,option).subscribe((res: HttpResponse<any>)=> {
        var wsCallStatus = "";
        this.serverResponse = JSON.parse(JSON.stringify(res.body));
        console.log(this.serverResponse);
        wsCallStatus = this.serverResponse["EvertzInterviewApp"]["Success"];

        if(!wsCallStatus){

          this.serverStatus_Error_Class = "isa_error";
          this.serverStatus_Error_Box = "fa fa-times-circle";
          this.serverStatus_Error_Message = "Server Unable to Save Answer, Please contact Your Supervisor.";
          
          window.scrollTo(0, 0);

        }else{
        console.log("program saved to DB");
        this.compilationStatus = "Program Saved Sucessfully.";
        
        }
      },
      (err: HttpErrorResponse) => {     
        if(err.status == 0) {

          window.scrollTo(0, 0);
          this.serverStatus_Error_Class = "isa_error";
          this.serverStatus_Error_Box = "fa fa-times-circle";
          this.serverStatus_Error_Message = "Failed to Save Answer, please resubmit your answer.";
          this.serverStatus_Error_Message = "Server failed to Get Questions List, Please contact your Supervisor.";
          let navigationExtras: NavigationExtras = {
            queryParams: {
                registerNumber : this.registerNumber,
                status : 5
            }
            
          };
          
          this.router.navigate(['/info'], navigationExtras);

          }
          else {
          console.log("Question answer saved to DB sucessfully : " + qId);
        }
      }
    );
  };

  public candiateSubmitAnswer(registerNumber,qId,answer,option,testProgress){

    this.postToServerObject.candiateSubmitAnswer(registerNumber,qId,answer,option).subscribe((res: HttpResponse<any>)=> {
        console.log(JSON.parse(JSON.stringify(res.body)));
        var wsCallStatus = "";
        this.serverResponse = JSON.parse(JSON.stringify(res.body));
        wsCallStatus = this.serverResponse["EvertzInterviewApp"]["Success"];

        if(!wsCallStatus){

          this.serverStatus_Error_Class = "isa_error";
          this.serverStatus_Error_Box = "fa fa-times-circle";
          this.serverStatus_Error_Message = "Failed to Submit Test, Please contact your Supervisor.";
          window.scrollTo(0, 0);
          window.stop();
        }else{

          if(testProgress){

            console.log("Test Submitted Sucessfully...")
            let navigationExtras: NavigationExtras = {
            queryParams: {
                registerNumber : this.registerNumber,
                status : 3
            }
            
          };
          this.router.navigate(['/info'], navigationExtras);

          }else{

            let navigationExtras: NavigationExtras = {
              queryParams: {
                  registerNumber : this.registerNumber,
                  status : 4,
                  event : this.resasonForSubmitting 
              }
              
            };
            this.router.navigate(['/info'], navigationExtras);
          }
      }
      },
      (err: HttpErrorResponse) => {     
        if(err.status == 0) {

          window.scrollTo(0, 0);
          this.serverStatus_Error_Class = "isa_error";
          this.serverStatus_Error_Box = "fa fa-times-circle";
          this.serverStatus_Error_Message = "Failed to Submit Test, Please contact your Supervisor.";
          this.serverStatus_Error_Message = "Server failed to Get Questions List, Please contact your Supervisor.";
          let navigationExtras: NavigationExtras = {
            queryParams: {
                registerNumber : this.registerNumber,
                status : 5
            }
            
          };
          
          this.router.navigate(['/info'], navigationExtras);

          }
          else {
          console.log("Test submitted sucessfully for Student: " + registerNumber);
        }
      }
    );
  };

  public sequence_Compile_1(registerNumber,qId,answer,option){

    this.postToServerObject.candiateSaveAnswer(registerNumber,qId,answer,option).subscribe((res: HttpResponse<any>)=> {
        var wsCallStatus = "";
        this.serverResponse = JSON.parse(JSON.stringify(res.body));
        console.log(this.serverResponse);
        wsCallStatus = this.serverResponse["EvertzInterviewApp"]["Success"];

        if(!wsCallStatus){

          this.serverStatus_Error_Class = "isa_error";
          this.serverStatus_Error_Box = "fa fa-times-circle";
          this.serverStatus_Error_Message = "Server Unable to Save Answer, Please contact Your Supervisor.";
          window.scrollTo(0, 0);
          window.stop();

        }else{
        console.log("program saved to DB, compiling...");
        this.sequence_Compile_2("CompileProgram",registerNumber,qId,option);
        }
      },
      (err: HttpErrorResponse) => {     
        if(err.status == 0) {

          window.scrollTo(0, 0);
          this.serverStatus_Error_Class = "isa_error";
          this.serverStatus_Error_Box = "fa fa-times-circle";
          this.serverStatus_Error_Message = "Failed to Save Answer, please resubmit your answer.";
          this.serverStatus_Error_Message = "Server failed to Get Questions List, Please contact your Supervisor.";
          let navigationExtras: NavigationExtras = {
            queryParams: {
                registerNumber : this.registerNumber,
                status : 5
            }
            
          };
          
          this.router.navigate(['/info'], navigationExtras);

          }
          else {
          console.log("Question answer saved to DB sucessfully, Compiling... : " + qId);
        }
      }
    );
  };

  public sequence_Compile_2(command,registerNumber,questionId,questionType){

    this.postToServerObject.candiateCodingTask(command,registerNumber,questionId,questionType).subscribe((res: HttpResponse<any>)=> {
        console.log(JSON.parse(JSON.stringify(res.body)));
        var wsCallStatus = "";
        this.serverResponse = JSON.parse(JSON.stringify(res.body));
        wsCallStatus = this.serverResponse["EvertzInterviewApp"]["Success"];
        console.log(wsCallStatus);
        if(wsCallStatus){

          /*this.serverStatus_Error_Class = "isa_error";
          this.serverStatus_Error_Box = "fa fa-times-circle";
          this.serverStatus_Error_Message = "Unable to "+ command +", Please contact Your Supervisor.";*/
          console.log("Program Compiled sucessfully.");
          this.compilationStatus = "Program Compiled sucessfully.";
          this.stopLoading();

        }else{
          this.compilationStatus = this.serverResponse["EvertzInterviewApp"]["Reason"];
          this.stopLoading();
          if(String(this.compilationStatus).search("ERROR")){
            this.compilationResult = "true";
          }
          window.scrollTo(0, 0);
          window.stop();

        }
      },
      (err: HttpErrorResponse) => {     
        if(err.status == 0) {

          window.scrollTo(0, 0);
          this.serverStatus_Error_Class = "isa_error";
          this.serverStatus_Error_Box = "fa fa-times-circle";
          this.serverStatus_Error_Message = "Server failed to "+ command +", Please contact your Supervisor.";
          this.serverStatus_Error_Message = "Server failed to Get Questions List, Please contact your Supervisor.";
          let navigationExtras: NavigationExtras = {
            queryParams: {
                registerNumber : this.registerNumber,
                status : 5
            }
            
          };
          
          this.router.navigate(['/info'], navigationExtras);

          }
          else {
          console.log("Program Compiled sucessfully.: " + questionId);
        }
      }
    );
  };

  public sequence_Run_1(registerNumber,qId,answer,option){

    this.postToServerObject.candiateSaveAnswer(registerNumber,qId,answer,option).subscribe((res: HttpResponse<any>)=> {
        var wsCallStatus = "";
        this.serverResponse = JSON.parse(JSON.stringify(res.body));
        console.log(this.serverResponse);
        wsCallStatus = this.serverResponse["EvertzInterviewApp"]["Success"];

        if(!wsCallStatus){

          this.serverStatus_Error_Class = "isa_error";
          this.serverStatus_Error_Box = "fa fa-times-circle";
          this.serverStatus_Error_Message = "Server Unable to Save Answer, Please contact Your Supervisor.";
          window.scrollTo(0, 0);
          window.stop();
        }else{
        console.log("program saved to DB, compiling...");
        this.sequence_Run_2("CompileProgram",registerNumber,qId,option);
        }
      },
      (err: HttpErrorResponse) => {     
        if(err.status == 0) {

          window.scrollTo(0, 0);
          this.serverStatus_Error_Class = "isa_error";
          this.serverStatus_Error_Box = "fa fa-times-circle";
          this.serverStatus_Error_Message = "Failed to Save Answer, please resubmit your answer.";
          this.serverStatus_Error_Message = "Server failed to Get Questions List, Please contact your Supervisor.";
          let navigationExtras: NavigationExtras = {
            queryParams: {
                registerNumber : this.registerNumber,
                status : 5
            }
            
          };
          
          this.router.navigate(['/info'], navigationExtras);

          }
          else {
          console.log("Question answer saved to DB sucessfully, Compiling... : " + qId);
        }
      }
    );
  };

  public sequence_Run_2(command,registerNumber,questionId,questionType){

    this.postToServerObject.candiateCodingTask(command,registerNumber,questionId,questionType).subscribe((res: HttpResponse<any>)=> {
        console.log(JSON.parse(JSON.stringify(res.body)));
        var wsCallStatus = "";
        this.serverResponse = JSON.parse(JSON.stringify(res.body));
        wsCallStatus = this.serverResponse["EvertzInterviewApp"]["Success"];

        if(wsCallStatus){

          /*this.serverStatus_Error_Class = "isa_error";
          this.serverStatus_Error_Box = "fa fa-times-circle";
          this.serverStatus_Error_Message = "Unable to "+ command +", Please contact Your Supervisor.";*/
          console.log("Program Compiled Sucessfully, Running... ");
          this.compilationStatus = "Program Compiled Sucessfully.";
          this.sequence_Run_3("RunProgram",registerNumber,questionId,questionType);

        }else{
          this.compilationStatus = this.serverResponse["EvertzInterviewApp"]["Reason"];
          //this.compilationStatus = this.serverResponse["EvertzInterviewApp"]["Reason"];
          if(String(this.compilationStatus).search("ERROR")){
            this.compilationResult = "true";
          }
          window.scrollTo(0, 0);
          window.stop();

        }
      },
      (err: HttpErrorResponse) => {     
        if(err.status == 0) {

          window.scrollTo(0, 0);
          this.serverStatus_Error_Class = "isa_error";
          this.serverStatus_Error_Box = "fa fa-times-circle";
          this.serverStatus_Error_Message = "Server failed to "+ command +", Please contact your Supervisor.";
          this.serverStatus_Error_Message = "Server failed to Get Questions List, Please contact your Supervisor.";
          let navigationExtras: NavigationExtras = {
            queryParams: {
                registerNumber : this.registerNumber,
                status : 5
            }
            
          };
          
          this.router.navigate(['/info'], navigationExtras);

          }
          else {
            console.log("Program Compiled Sucessfully, Running... : " + questionId);
        }
      }
    );
  };

  public sequence_Run_3(command,registerNumber,questionId,questionType){

    this.postToServerObject.candiateCodingTask(command,registerNumber,questionId,questionType).subscribe((res: HttpResponse<any>)=> {
        console.log(JSON.parse(JSON.stringify(res.body)));
        var wsCallStatus = "";
        this.serverResponse = JSON.parse(JSON.stringify(res.body));
        wsCallStatus = this.serverResponse["EvertzInterviewApp"]["Success"];

        if(wsCallStatus){
          console.log("Program Ran Sucessfully...");
          this.testCaseResults = this.serverResponse["EvertzInterviewApp"]["ParameterList"]["TestCaseList"];
          console.log("Validating Results...");
          this.stopLoading();
          this.validateTestCases();
                    
        }else{
          this.serverStatus_Error_Class = "isa_error";
          this.serverStatus_Error_Box = "fa fa-times-circle";
          this.serverStatus_Error_Message = "Unable to "+ command +", Please contact Your Supervisor.";
          window.scrollTo(0, 0);
          this.stopLoading();
          window.stop();
        }
      },
      (err: HttpErrorResponse) => {     
        if(err.status == 0) {

          window.scrollTo(0, 0);
          this.serverStatus_Error_Class = "isa_error";
          this.serverStatus_Error_Box = "fa fa-times-circle";
          this.serverStatus_Error_Message = "Server failed to "+ command +", Please contact your Supervisor.";
          this.serverStatus_Error_Message = "Server failed to Get Questions List, Please contact your Supervisor.";
          let navigationExtras: NavigationExtras = {
            queryParams: {
                registerNumber : this.registerNumber,
                status : 5
            }
            
          };
          
          this.router.navigate(['/info'], navigationExtras);

          }
          else {
            console.log("Program Ran Sucessfully for : " + questionId);
        }
      }
    );
  };

}
