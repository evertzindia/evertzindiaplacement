import { Component } from '@angular/core';
import { BreakpointObserver, Breakpoints } from '@angular/cdk/layout';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { PostToServerService } from '../post-to-server.service';
import { HttpErrorResponse, HttpResponse } from '@angular/common/http';

@Component({
  selector: 'app-admin-reset-test',
  templateUrl: './admin-reset-test.component.html',
  styleUrls: ['./admin-reset-test.component.css']
})
export class AdminResetTestComponent {
  
  public serverResponse: any;
  public serverStatus_Error_Class = "";
  public serverStatus_Error_Box = "";
  public serverStatus_Error_Message = "";
  public questionTypeList = ['All','MCQ','Coding'];
  public questionType: any;
  public resetStatus = "";
  public questionStatus = "";
 public  selectedTestType: string = '';

  isHandset$: Observable<boolean> = this.breakpointObserver.observe(Breakpoints.Handset)
    .pipe(
      map(result => result.matches)
    );

  constructor(private breakpointObserver: BreakpointObserver,private postToServerObject: PostToServerService,) {}

  resetTest(regno){
    console.log(regno);
    this.serverStatus_Error_Class = "";
    this.serverStatus_Error_Box = "";
    this.serverStatus_Error_Message = "";
    this.resetStatus = "";
    this.questionStatus = "";
    if(this.selectedTestType != ""){
      this.adminResetTest(regno,this.selectedTestType);
    }
    
  }
 
  selectChangeHandler (event: any) {
    this.selectedTestType = event.target.value;
  }

  public adminResetTest(registerNumber,questionType){

    this.postToServerObject.adminResetTest(registerNumber,questionType).subscribe((res: HttpResponse<any>)=> {
        console.log(JSON.parse(JSON.stringify(res.body)));
        var wsCallStatus = "";
        this.serverResponse = JSON.parse(JSON.stringify(res.body));
        wsCallStatus = this.serverResponse["EvertzInterviewApp"]["Success"];

        if(!wsCallStatus){

          this.serverStatus_Error_Class = "isa_error";
          this.serverStatus_Error_Box = "fa fa-times-circle";
          this.serverStatus_Error_Message = "Failed to Rest, Please contact your System Admin.";
          window.scrollTo(0, 0);
          window.stop();
        }else{
          this.resetStatus = "Reset "+questionType+" Test - Success for: " + registerNumber;
          /*if(questionType != "All"){
            this.candiateGenerateQuestoins(registerNumber,questionType);
          }*/
        }       

      },
      (err: HttpErrorResponse) => {     
        if(err.status == 0) {

          window.scrollTo(0, 0);
          this.serverStatus_Error_Class = "isa_error";
          this.serverStatus_Error_Box = "fa fa-times-circle";
          this.serverStatus_Error_Message = "Server failed to Reset, Please contact your System Admin.";

          }
          else {
          console.log("Questions Received sucessfully for : " + registerNumber);
        }
      }
    );
  };

  public candiateGenerateQuestoins(registerNumber,questionType){

    this.postToServerObject.candiateGenerateQuestoins(registerNumber,questionType).subscribe((res: HttpResponse<any>)=> {
        console.log(JSON.parse(JSON.stringify(res.body)));
        var wsCallStatus = "";
        this.serverResponse = JSON.parse(JSON.stringify(res.body));
        wsCallStatus = this.serverResponse["EvertzInterviewApp"]["Success"];

        if(wsCallStatus){

          this.questionStatus = questionType + " Question generated sucessfully for: "+ registerNumber;

        }else{

          this.serverStatus_Error_Class = "isa_error";
          this.serverStatus_Error_Box = "fa fa-times-circle";
          this.serverStatus_Error_Message = "Server Unable to Generate "+questionType+" Questions list, Please contact Your Supervisor.";
          window.scrollTo(0, 0);
          window.stop();
        }

        

      },
      (err: HttpErrorResponse) => {     
        if(err.status == 0) {
          window.scrollTo(0, 0);
          this.serverStatus_Error_Class = "isa_error";
          this.serverStatus_Error_Box = "fa fa-times-circle";
          this.serverStatus_Error_Message = "Server failed to Generate "+questionType+" Questions, Please contact your Supervisor.";

          }
          else {
          console.log(questionType+ " Questions Generated sucessfully for : " + registerNumber);
        }
      }
    );
  };  

}
