export class RegisterStudentDetails {
    RegisterNumber: any;
    StudentName: any;
    DOB: any;
    Gender: any;
    College: any;
    Degree: any;
    Branch: any;
    YearOfPassing: any;
    CGPA: any;
    Email: any;
    Mobile: any;
    LanguageSelected: any;
}