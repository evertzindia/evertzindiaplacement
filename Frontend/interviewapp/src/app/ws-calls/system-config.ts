import { Injectable } from '@angular/core';

@Injectable()
export class SystemConfig {

    constructor() {}

    public serverIp = "10.42.10.21";
    public mcqTestStatus = "MCQ Test already taken. Please contact your Supervisor.";
    public codingTestStatus = "Coding Test already taken. Please contact your Supervisor.";
    public testStatus = "Data Saved to Server. You may leave the hall.";
    public malpractise = " key pressed. Test saved. You may leave the hall.";
    public serverTimedOut = "Connection Timed Out - Please contact your Supervisor";
    public errorResetTime = 10;
    public timerForMCQ = 3600;
    public timerForCoding = 1800;

    public questionsList = {
        "EvertzInterviewApp": {
            "Command": "GenerateResult",
            "Subsystem": "Admin",
            "ParameterList": [
                {
                    "RegisterNumber": "1mj15cs422",
                    "Score": {
                        "Score": 0,
                        "Networking": 1,
                        "Linux": 2,
                        "Coding": 3,
                        "Broadcast": 4,
                        "Analytical": 5,
                        "Total": 28,
                        "SQL": 6,
                        "OptionalModule": "Java"
                    },
                    "Name": "Noor"
                },
                {
                    "RegisterNumber": "13422HG",
                    "Score": {
                        "Score": 0,
                        "Networking": 2,
                        "Linux": 1,
                        "Coding": 3,
                        "Broadcast": 4,
                        "Analytical": 5,
                        "Total": 25,
                        "SQL": 6,
                        "OptionalModule": "Java"
                    },
                    "Name": "Ramya"
                },
                {
                    "RegisterNumber": "13412HG",
                    "Score": {
                        "Score": 0,
                        "Networking": 3,
                        "Linux": 2,
                        "Coding": 1,
                        "Broadcast": 4,
                        "Analytical": 5,
                        "Total": 20,
                        "SQL": 6,
                        "OptionalModule": "Java"
                    },
                    "Name": "Pranab"
                },
                {
                    "RegisterNumber": "13462HG",
                    "Score": {
                        "Score": 0,
                        "Networking": 4,
                        "Linux": 3,
                        "Coding": 2,
                        "Broadcast": 1,
                        "Analytical": 5,
                        "Total": 22,
                        "SQL": 6,
                        "OptionalModule": "Java"
                    },
                    "Name": "Joshua"
                },
                {
                    "RegisterNumber": "PR123",
                    "Score": {
                        "Score": 0,
                        "Networking": 5,
                        "Linux": 4,
                        "Coding": 3,
                        "Broadcast": 2,
                        "Analytical": 1,
                        "Total": 22,
                        "SQL": 6,
                        "OptionalModule": "Java"
                    },
                    "Name": "Sita"
                },
                {
                    "RegisterNumber": "HR123",
                    "Score": {
                        "Score": 0,
                        "Networking": 6,
                        "Linux": 5,
                        "Coding": 4,
                        "Broadcast": 3,
                        "Analytical": 2,
                        "Total": 24,
                        "SQL": 1,
                        "OptionalModule": "Java"
                    },
                    "Name": "Simran"
                }
            ],
            "Success": true,
            "Result": "Result generated"
        }
    }
}